Attribute VB_Name = "m_Parking"
Option Explicit
Public pkHost$, pkName$, pkUser$, pkPass$
Public pkSystem As Boolean
Public Sub initialParking(set_dbHost$, set_dbName$, set_dbUser$, set_dbPass$)
     '<EhHeader>
     On Error GoTo initialParking_Err
     '</EhHeader>
100  pkHost = set_dbHost
105  pkName = set_dbName
110  pkUser = set_dbUser
115  pkPass = set_dbPass
     '<EhFooter>
     Exit Sub

initialParking_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Parking.initialParking " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub getParking()
     '<EhHeader>
     On Error GoTo getParking_Err
     '</EhHeader>
        Dim pDB As cDB
        Dim rs  As Recordset
        Dim tmpSql$, i%, sql$, tmpDateTime$
100     Set pDB = New cDB
105     pDB.dType = d_MySQL

110     sql = "SELECT Id,ParkingTime,LicenseDigit,Flag,ReferenceId FROM ParkingLog WHERE ParkingTime > '" & LastTime & "'"

115     Set rs = pDB.Query(sql)
120     If rs.RecordCount = 0 Then
125         Set rs = Nothing
130         Set pDB = Nothing

            Exit Sub

        End If
135     tmpSql = "INSERT INTO tblParking (CID,Flag,ParkingTime,LicenseDigit,ReferenceId) VALUES "
 
140     i = 1

145     Do While Not rs.EOF
150         If Val(Format$(Now, "yyyy")) > 2100 Then ' �.�.
155             tmpDateTime = Format$(rs!ParkingTime, "yyyy") - 543 & Format$(rs!ParkingTime, "-MM-dd hh:mm:ss")
            Else
160             tmpDateTime = Format$(rs!ParkingTime, "yyyy") & Format$(rs!ParkingTime, "-MM-dd hh:mm:ss")
            End If

165         tmpSql = tmpSql & "(" & rs!ID & "," & rs!Flag & ",'" & tmpDateTime & "','" & rs!LicenseDigit & "'," & rs!ReferenceId & ")"

170         If rs.RecordCount - i > 0 Then
175             tmpSql = tmpSql & ","
            End If
180         rs.MoveNext
185         i = i + 1
        Loop

190     tmpSql = tmpSql & ";"
195     Call InsertParking(tmpSql)
200     Set rs = Nothing
205     Set pDB = Nothing
     '<EhFooter>
     Exit Sub

getParking_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Parking.getParking " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub InsertParking(sql$)
     '<EhHeader>
     On Error GoTo InsertParking_Err
     '</EhHeader>
        Dim rs1 As ADODB.Recordset
100     Set rs1 = New ADODB.Recordset
105     rs1.CursorLocation = adUseClient
110     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
115     Set rs1 = Nothing
     '<EhFooter>
     Exit Sub

InsertParking_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Parking.InsertParking " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub
Private Function LastTime() As String
     '<EhHeader>
     On Error GoTo LastTime_Err
     '</EhHeader>
        Dim sql As String
        Dim rs1 As ADODB.Recordset
        Dim curDate$
100     curDate = m_year.gYear & Format$(Now, "-MM-dd") & " 00:00:00"

105         sql = "SELECT * FROM tblParking WHERE ParkingTime>='" & curDate & "' ORDER BY ID DESC"

110     Set rs1 = New ADODB.Recordset
115     rs1.CursorLocation = adUseClient
120     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
125     If Val(Format$(Now, "yyyy")) > 2100 Then ' �.�.
130         If rs1.RecordCount > 0 Then
135             LastTime = Format$(rs1!ParkingTime, "yyyy") - 543 & Format$(rs1!ParkingTime, "-MM-dd hh:mm:ss")
            Else
140             LastTime = curDate
            End If
        Else
145          If rs1.RecordCount > 0 Then
150             LastTime = Format$(rs1!ParkingTime, "yyyy") & Format$(rs1!ParkingTime, "-MM-dd hh:mm:ss")
            Else
155             LastTime = curDate
            End If
        End If
    
160     Set rs1 = Nothing
     '<EhFooter>
     Exit Function

LastTime_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Parking.LastTime " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function
Public Function Connection() As Boolean
     '<EhHeader>
     On Error GoTo Connection_Err
     '</EhHeader>
    Dim pDB As cDB
100 Set pDB = New cDB
105 pDB.dType = d_MySQL
110 Connection = pDB.Connect
115 Set pDB = Nothing
     '<EhFooter>
     Exit Function

Connection_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Parking.Connection " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function
