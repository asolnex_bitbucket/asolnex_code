Attribute VB_Name = "m_LED"
Option Explicit

Public DSP_IP     As String

Public DSP_PORT   As String

Public DSP_Bing   As String

Public DSP_Color  As String

Public DSP_BingOn As Boolean

Public Sub DSP_TEST()
     '<EhHeader>
     On Error GoTo DSP_TEST_Err
     '</EhHeader>


        Dim sDat() As Byte
        Dim STX    As Byte
        Dim ETX    As Byte
        Dim chkSum As Byte
        Dim tmpDat As String, tmpCarNo$, tmpQNo$, tmpLane$
        Dim Bing   As String, dColor As String
        Dim i%

100     STX = &H2
105     ETX = &H3
110     Bing = "5"
115     dColor = DSP_Color
120     tmpDat = Chr$(STX) & "<0>0</0>"
125     tmpDat = tmpDat & "<1>"

130     For i = 0 To 3
135         tmpCarNo = "8AB"
140         tmpQNo = "888"
145         tmpLane = "88"
150         If i = 0 Then
155             Bing = DSP_Bing
            Else
160             Bing = "0"
            End If
165         tmpDat = tmpDat & "<r" & i & "><0>" & tmpCarNo & tmpQNo & tmpLane & "</0><1>" & Bing & "</1><2>" & dColor & "</2></r" & i & ">"
170     Next i

175     tmpDat = tmpDat & "</1>"
180     tmpDat = tmpDat & Chr$(ETX)

185     For i = 1 To Len(tmpDat)
190         chkSum = chkSum Xor Asc(Mid(tmpDat, i, 1))
195     Next i

200     tmpDat = tmpDat & Chr$(chkSum)

205     With frmMain.sck
210         .RemoteHost = m_LED.DSP_IP
215         .RemotePort = m_LED.DSP_PORT
220         .SendData tmpDat
225         DoEvents: Sleep 30
230         .SendData tmpDat
        End With

     '<EhFooter>
     Exit Sub

DSP_TEST_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_LED.DSP_TEST " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub DSP(BingOn As Boolean)
     '<EhHeader>
     On Error GoTo DSP_Err
     '</EhHeader>


        Dim sDat() As Byte
        Dim STX    As Byte
        Dim ETX    As Byte
        Dim chkSum As Byte
        Dim tmpDat As String, tmpCarNo$, tmpQNo$, tmpLane$
        Dim Bing   As String, dColor As String
        Dim i%

100     STX = &H2
105     ETX = &H3
110     Bing = "5"
115     dColor = DSP_Color
120     tmpDat = Chr$(STX) & "<0>0</0>"
125     tmpDat = tmpDat & "<1>"

130     For i = 0 To 3
135         tmpCarNo = FillStr(Right$(frmDSP32.lblCarNo(i).Caption, 3), 3)
140         tmpQNo = FillStr(frmDSP32.lblQ(i).Caption, 3)
145         tmpLane = FillStr(frmDSP32.lblLane(i).Caption, 2)
150         If Val(tmpQNo) <= 0 Then
155             tmpCarNo = "###"
            End If
160         If i = 0 And BingOn Then
165             Bing = DSP_Bing
            Else
170             Bing = "0"
            End If
175         tmpDat = tmpDat & "<r" & i & "><0>" & tmpCarNo & tmpQNo & tmpLane & "</0><1>" & Bing & "</1><2>" & dColor & "</2></r" & i & ">"
180     Next i

185     tmpDat = tmpDat & "</1>"
190     tmpDat = tmpDat & Chr$(ETX)

195     For i = 1 To Len(tmpDat)
200         chkSum = chkSum Xor Asc(Mid(tmpDat, i, 1))
205     Next i

210     tmpDat = tmpDat & Chr$(chkSum)

        'If Not frmMain.MSComm1.PortOpen Then OpenComPort (9)
        'frmMain.MSComm1.Output = tmpDat

215     With frmMain.sck
220         .RemoteHost = m_LED.DSP_IP
225         .RemotePort = m_LED.DSP_PORT
230         .SendData tmpDat
235         DoEvents: Sleep 30
240         .SendData tmpDat
        End With
    'Debug.Print tmpDat
     '<EhFooter>
     Exit Sub

DSP_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_LED.DSP " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Function FillStr(refStr As String, fLen As Byte)
     '<EhHeader>
     On Error GoTo FillStr_Err
     '</EhHeader>


100     If Len(refStr) < fLen Then
105         FillStr = Space$(fLen - Len(refStr)) & refStr
        Else
110         FillStr = refStr
        End If

     '<EhFooter>
     Exit Function

FillStr_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_LED.FillStr " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function

Private Sub OpenComPort(Com As Integer)
     '<EhHeader>
     On Error GoTo OpenComPort_Err
     '</EhHeader>

        'On Error Resume Next
        On Error GoTo Err1

100     If frmMain.MSComm1.PortOpen = True Then
105         frmMain.MSComm1.PortOpen = False
        End If

110     frmMain.MSComm1.CommPort = Com
115     frmMain.MSComm1.settings = "9600,n,8,1"
120     frmMain.MSComm1.Handshaking = comNone
125     frmMain.MSComm1.InBufferSize = 1024
130     frmMain.MSComm1.OutBufferSize = 512
135     frmMain.MSComm1.RThreshold = 1 ' ��ͧ�� 1 �֧���Ѻ��ҷ��ͺ��Ѻ��
140     frmMain.MSComm1.SThreshold = 0
    
145     frmMain.MSComm1.InputLen = 1
    
150     frmMain.MSComm1.EOFEnable = False
155     frmMain.MSComm1.ParityReplace = "?"
160     frmMain.MSComm1.NullDiscard = False
165     frmMain.MSComm1.RTSEnable = False
170     frmMain.MSComm1.DTREnable = True
175     frmMain.MSComm1.PortOpen = True ' **

        Exit Sub
    
Err1:

     '<EhFooter>
     Exit Sub

OpenComPort_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_LED.OpenComPort " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub
