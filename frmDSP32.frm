VERSION 5.00
Object = "{C0A63B80-4B21-11D3-BD95-D426EF2C7949}#1.0#0"; "vsflex7l.ocx"
Begin VB.Form frmDSP32 
   BorderStyle     =   0  'None
   ClientHeight    =   10800
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   19200
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   Picture         =   "frmDSP32.frx":0000
   ScaleHeight     =   10800
   ScaleWidth      =   19200
   ShowInTaskbar   =   0   'False
   Begin VSFlex7LCtl.VSFlexGrid fgQWait 
      Height          =   10800
      Left            =   12015
      TabIndex        =   1
      Top             =   0
      Width           =   7200
      _cx             =   12700
      _cy             =   19050
      _ConvInfo       =   1
      Appearance      =   0
      BorderStyle     =   1
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "TH SarabunPSK"
         Size            =   20.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MousePointer    =   0
      BackColor       =   0
      ForeColor       =   16777215
      BackColorFixed  =   192
      ForeColorFixed  =   16777215
      BackColorSel    =   0
      ForeColorSel    =   -2147483634
      BackColorBkg    =   0
      BackColorAlternate=   0
      GridColor       =   16777215
      GridColorFixed  =   16777215
      TreeColor       =   -2147483632
      FloodColor      =   192
      SheetBorder     =   -2147483642
      FocusRect       =   1
      HighLight       =   0
      AllowSelection  =   -1  'True
      AllowBigSelection=   -1  'True
      AllowUserResizing=   0
      SelectionMode   =   0
      GridLines       =   1
      GridLinesFixed  =   2
      GridLineWidth   =   1
      Rows            =   50
      Cols            =   4
      FixedRows       =   2
      FixedCols       =   0
      RowHeightMin    =   0
      RowHeightMax    =   0
      ColWidthMin     =   0
      ColWidthMax     =   0
      ExtendLastCol   =   -1  'True
      FormatString    =   $"frmDSP32.frx":384044
      ScrollTrack     =   0   'False
      ScrollBars      =   0
      ScrollTips      =   0   'False
      MergeCells      =   5
      MergeCompare    =   0
      AutoResize      =   -1  'True
      AutoSizeMode    =   0
      AutoSearch      =   0
      AutoSearchDelay =   2
      MultiTotals     =   -1  'True
      SubtotalPosition=   1
      OutlineBar      =   0
      OutlineCol      =   0
      Ellipsis        =   0
      ExplorerBar     =   0
      PicturesOver    =   0   'False
      FillStyle       =   0
      RightToLeft     =   0   'False
      PictureType     =   0
      TabBehavior     =   0
      OwnerDraw       =   0
      Editable        =   0
      ShowComboButton =   -1  'True
      WordWrap        =   0   'False
      TextStyle       =   0
      TextStyleFixed  =   0
      OleDragMode     =   0
      OleDropMode     =   0
      ComboSearch     =   3
      AutoSizeMouse   =   -1  'True
      FrozenRows      =   0
      FrozenCols      =   0
      AllowUserFreezing=   0
      BackColorFrozen =   0
      ForeColorFrozen =   16777215
      WallPaperAlignment=   9
   End
   Begin VB.TextBox txtQSkip 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "TH SarabunPSK"
         Size            =   26.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   1335
      Left            =   0
      MultiLine       =   -1  'True
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   9480
      Width           =   12000
   End
   Begin VB.Label lblLane 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "5"
      BeginProperty Font 
         Name            =   "TH SarabunPSK"
         Size            =   110.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1890
      Index           =   0
      Left            =   9570
      TabIndex        =   13
      Top             =   1110
      Width           =   2595
   End
   Begin VB.Label lblLane 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "2"
      BeginProperty Font 
         Name            =   "TH SarabunPSK"
         Size            =   110.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1890
      Index           =   1
      Left            =   9570
      TabIndex        =   12
      Top             =   2760
      Width           =   2595
   End
   Begin VB.Label lblLane 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "10"
      BeginProperty Font 
         Name            =   "TH SarabunPSK"
         Size            =   110.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1890
      Index           =   2
      Left            =   9570
      TabIndex        =   11
      Top             =   4410
      Width           =   2595
   End
   Begin VB.Label lblLane 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "7"
      BeginProperty Font 
         Name            =   "TH SarabunPSK"
         Size            =   110.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1890
      Index           =   3
      Left            =   9570
      TabIndex        =   10
      Top             =   6060
      Width           =   2595
   End
   Begin VB.Label lblQ 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "001"
      BeginProperty Font 
         Name            =   "TH SarabunPSK"
         Size            =   110.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   1890
      Index           =   0
      Left            =   4980
      TabIndex        =   9
      Top             =   1110
      Width           =   4755
   End
   Begin VB.Label lblQ 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "003"
      BeginProperty Font 
         Name            =   "TH SarabunPSK"
         Size            =   110.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   1890
      Index           =   1
      Left            =   4980
      TabIndex        =   8
      Top             =   2760
      Width           =   4755
   End
   Begin VB.Label lblQ 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "002"
      BeginProperty Font 
         Name            =   "TH SarabunPSK"
         Size            =   110.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   1890
      Index           =   2
      Left            =   4980
      TabIndex        =   7
      Top             =   4410
      Width           =   4755
   End
   Begin VB.Label lblQ 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "001"
      BeginProperty Font 
         Name            =   "TH SarabunPSK"
         Size            =   110.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   1890
      Index           =   3
      Left            =   4980
      TabIndex        =   6
      Top             =   6060
      Width           =   4755
   End
   Begin VB.Label lblCarNo 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "1EA"
      BeginProperty Font 
         Name            =   "TH SarabunPSK"
         Size            =   110.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1890
      Index           =   0
      Left            =   0
      TabIndex        =   5
      Top             =   1110
      Width           =   4965
   End
   Begin VB.Label lblCarNo 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "2EA"
      BeginProperty Font 
         Name            =   "TH SarabunPSK"
         Size            =   110.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1890
      Index           =   1
      Left            =   0
      TabIndex        =   4
      Top             =   2760
      Width           =   4965
   End
   Begin VB.Label lblCarNo 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "5EA"
      BeginProperty Font 
         Name            =   "TH SarabunPSK"
         Size            =   110.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1890
      Index           =   2
      Left            =   0
      TabIndex        =   3
      Top             =   4410
      Width           =   4965
   End
   Begin VB.Label lblCarNo 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "1EC"
      BeginProperty Font 
         Name            =   "TH SarabunPSK"
         Size            =   110.25
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1890
      Index           =   3
      Left            =   0
      TabIndex        =   2
      Top             =   6060
      Width           =   4965
   End
End
Attribute VB_Name = "frmDSP32"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Sub ClearQ()
     '<EhHeader>
     On Error GoTo ClearQ_Err
     '</EhHeader>


        Dim i%

100     For i = 0 To 3
105         lblCarNo(i).Caption = ""
110         lblLane(i).Caption = ""
115         lblQ(i).Caption = ""
120     Next i

     '<EhFooter>
     Exit Sub

ClearQ_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmDSP32.ClearQ " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
     '<EhHeader>
     On Error GoTo Form_KeyPress_Err
     '</EhHeader>


100     If KeyAscii = 27 Then Unload Me

     '<EhFooter>
     Exit Sub

Form_KeyPress_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmDSP32.Form_KeyPress " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub Form_Load()
     '<EhHeader>
     On Error GoTo Form_Load_Err
     '</EhHeader>


100     fgQWait.MergeRow(0) = True

     '<EhFooter>
     Exit Sub

Form_Load_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmDSP32.Form_Load " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

