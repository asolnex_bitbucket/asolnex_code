Attribute VB_Name = "m_OpenFile"
Option Explicit

Private Declare Function ShellExecute _
                Lib "shell32.dll" _
                Alias "ShellExecuteA" (ByVal hWnd As Long, _
                                       ByVal lpszOp As String, _
                                       ByVal lpszFile As String, _
                                       ByVal lpszParams As String, _
                                       ByVal lpszDir As String, _
                                       ByVal FsShowCmd As Long) As Long

Private Declare Function GetDesktopWindow Lib "USER32" () As Long

Const SW_SHOWNORMAL = 1

Const SE_ERR_FNF = 2&

Const SE_ERR_PNF = 3&

Const SE_ERR_ACCESSDENIED = 5&

Const SE_ERR_OOM = 8&

Const SE_ERR_DLLNOTFOUND = 32&

Const SE_ERR_SHARE = 26&

Const SE_ERR_ASSOCINCOMPLETE = 27&

Const SE_ERR_DDETIMEOUT = 28&

Const SE_ERR_DDEFAIL = 29&

Const SE_ERR_DDEBUSY = 30&

Const SE_ERR_NOASSOC = 31&

Const ERROR_BAD_FORMAT = 11&

Public Function Start(DocName As String) As Long
     '<EhHeader>
     On Error GoTo Start_Err
     '</EhHeader>


        Dim Scr_hDC As Long
100     Scr_hDC = GetDesktopWindow()
105     Start = ShellExecute(Scr_hDC, "Open", DocName, "", "C:\", SW_SHOWNORMAL)

     '<EhFooter>
     Exit Function

Start_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_OpenFile.Start " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function

