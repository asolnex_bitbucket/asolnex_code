Attribute VB_Name = "m_sck"
Option Explicit

Public Sub processData(qStr As String)
     '<EhHeader>
     On Error GoTo processData_Err
     '</EhHeader>

If Len(qStr) < 1 Then Exit Sub
'Call LogNTEvent(vbCrLf & vbCrLf & "TEST : " & qStr, EVENTLOG_ERROR_TYPE, 0) ' For debug

        Dim exp() As String
100     exp = Split(Trim$(qStr), "|")

        'exp(0) => Function KP PRT
105     If exp(0) = "KP" Then
            'exp(1) => command
            'exp(2) => Index
            'exp(3) => QType
110         frmKeyPad.cmbQType(exp(2)).ListIndex = Val(exp(3))
115         Call processKeyPad(exp(1), Val(exp(2)))
120     ElseIf exp(0) = "PRT" Then
            'exp(1) => Index
            'exp(2) => CarNo
            'exp(3) => CarLicense
            'exp(4) => QBottle
            'exp(5) => CID Car ID
125         Call m_Q.getQ(Val(exp(1)) + 1, Val(exp(4)), exp(2), exp(3), CLng(exp(5)))
130         Call frmMain.ReadData 'g_ForceRead = True
135     ElseIf exp(0) = "QPass" Then
            '(1) => QBottle
            '(2)=>QID
            '(3) => Lane
140         Call m_Q.QPassCall(Val(exp(2)), Val(exp(1)), Val(exp(3)))
145         g_ForceRead = True
150     ElseIf exp(0) = "DQ" Then
            '(1) => index
            '(2)=> QNUM
155         Call m_Q.DirectQ(Val(exp(1)) + 1, Val(exp(2))) 'Call Direct Q
160         g_ForceRead = True
        End If

     '<EhFooter>
     Exit Sub

processData_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_sck.processData " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub processKeyPad(cmd As String, index As Integer)
     '<EhHeader>
     On Error GoTo processKeyPad_Err
     '</EhHeader>


100     Select Case cmd
    
            Case "C": Call frmKeyPad.cmdCall_Click(index)
        
105         Case "S": Call frmKeyPad.cmdStart_Click(index)
        
110         Case "F": Call frmKeyPad.cmdFinish_Click(index)
    
        End Select

     '<EhFooter>
     Exit Sub

processKeyPad_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_sck.processKeyPad " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub processPrinter(cmd As String, index As Integer)

End Sub

