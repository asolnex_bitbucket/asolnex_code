VERSION 5.00
Object = "{C0A63B80-4B21-11D3-BD95-D426EF2C7949}#1.0#0"; "vsflex7l.ocx"
Begin VB.Form frmQPass 
   Caption         =   "Queue Pass"
   ClientHeight    =   7080
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6405
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   222
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmQPass.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   7080
   ScaleWidth      =   6405
   StartUpPosition =   2  'CenterScreen
   Begin VSFlex7LCtl.VSFlexGrid fgQPass 
      Height          =   7080
      Left            =   15
      TabIndex        =   0
      Top             =   15
      Width           =   6420
      _cx             =   11324
      _cy             =   12488
      _ConvInfo       =   1
      Appearance      =   1
      BorderStyle     =   1
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MousePointer    =   0
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      BackColorFixed  =   -2147483633
      ForeColorFixed  =   -2147483630
      BackColorSel    =   -2147483635
      ForeColorSel    =   -2147483634
      BackColorBkg    =   -2147483636
      BackColorAlternate=   -2147483643
      GridColor       =   -2147483633
      GridColorFixed  =   -2147483632
      TreeColor       =   -2147483632
      FloodColor      =   192
      SheetBorder     =   -2147483642
      FocusRect       =   1
      HighLight       =   0
      AllowSelection  =   -1  'True
      AllowBigSelection=   -1  'True
      AllowUserResizing=   0
      SelectionMode   =   0
      GridLines       =   1
      GridLinesFixed  =   2
      GridLineWidth   =   1
      Rows            =   50
      Cols            =   6
      FixedRows       =   1
      FixedCols       =   0
      RowHeightMin    =   0
      RowHeightMax    =   0
      ColWidthMin     =   0
      ColWidthMax     =   0
      ExtendLastCol   =   -1  'True
      FormatString    =   $"frmQPass.frx":058A
      ScrollTrack     =   0   'False
      ScrollBars      =   2
      ScrollTips      =   0   'False
      MergeCells      =   0
      MergeCompare    =   0
      AutoResize      =   -1  'True
      AutoSizeMode    =   0
      AutoSearch      =   0
      AutoSearchDelay =   2
      MultiTotals     =   -1  'True
      SubtotalPosition=   1
      OutlineBar      =   0
      OutlineCol      =   0
      Ellipsis        =   0
      ExplorerBar     =   0
      PicturesOver    =   0   'False
      FillStyle       =   0
      RightToLeft     =   0   'False
      PictureType     =   0
      TabBehavior     =   0
      OwnerDraw       =   0
      Editable        =   2
      ShowComboButton =   -1  'True
      WordWrap        =   0   'False
      TextStyle       =   0
      TextStyleFixed  =   0
      OleDragMode     =   0
      OleDropMode     =   0
      ComboSearch     =   3
      AutoSizeMouse   =   -1  'True
      FrozenRows      =   0
      FrozenCols      =   0
      AllowUserFreezing=   0
      BackColorFrozen =   0
      ForeColorFrozen =   0
      WallPaperAlignment=   9
   End
End
Attribute VB_Name = "frmQPass"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub fgQPass_DblClick()
     '<EhHeader>
     On Error GoTo fgQPass_DblClick_Err
     '</EhHeader>

        '���¡��Ƿ���ҹ����ǵ���Ţ�ʹդ��

100     If Len(fgQPass.TextMatrix(fgQPass.row, 4)) <= 0 Then
105         MsgBox "Please select lane", vbCritical

            Exit Sub

        End If
110     If InStr(1, fgQPass.TextMatrix(fgQPass.row, 3), "Bottle") > 0 Then
115         Call m_Q.QPassCall(Val(fgQPass.TextMatrix(fgQPass.row, 0)), True, Val(Left(fgQPass.TextMatrix(fgQPass.row, 4), 2))) ' QID,Bottle,Lane
        Else
120         Call m_Q.QPassCall(Val(fgQPass.TextMatrix(fgQPass.row, 0)), False, Val(Left(fgQPass.TextMatrix(fgQPass.row, 4), 2)))
        End If
125     Call m_Q.QPass(fgQPass)
130     g_ForceRead = True

     '<EhFooter>
     Exit Sub

fgQPass_DblClick_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmQPass.fgQPass_DblClick " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub cQPass()
     '<EhHeader>
     On Error GoTo cQPass_Err
     '</EhHeader>


100     Call m_Q.QPass(fgQPass)

     '<EhFooter>
     Exit Sub

cQPass_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmQPass.cQPass " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub fgQPass_EnterCell()
     '<EhHeader>
     On Error GoTo fgQPass_EnterCell_Err
     '</EhHeader>


100     If fgQPass.Col = 4 Then
105         fgQPass.Editable = flexEDKbdMouse
        Else
110         fgQPass.Editable = flexEDNone
        End If

     '<EhFooter>
     Exit Sub

fgQPass_EnterCell_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmQPass.fgQPass_EnterCell " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub Form_Load()
     '<EhHeader>
     On Error GoTo Form_Load_Err
     '</EhHeader>


100     Call cQPass

     '<EhFooter>
     Exit Sub

Form_Load_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmQPass.Form_Load " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub
