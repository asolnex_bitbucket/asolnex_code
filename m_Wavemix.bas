Attribute VB_Name = "WAVEMIX"
Option Explicit

Global hWaveMix     As Long

Global lpWaveMix()  As Long

Global WaveHandle   As Long

Global WAVMIX_Quiet As Integer

Global Const WAVEMIX_MAXCHANNELS = 8

Type tChannelInfo

    Loops As Long

    WaveFile As String

End Type

Type WAVEMIXINFO

    wSize As Integer
    bVersionMajor As String * 1
    bVersionMinor As String * 1
    szDate(12) As String
    dwFormats As Long

End Type

Type MIXCONFIG

    wSize As Integer
    dwFlagsLo As Integer
    dwFlagsHi As Integer
    wChannels As Integer
    wSamplingRate As Integer

End Type

Private Type MIXPLAYPARAMS

    wSize         As Integer
    hMixSessionLo As Integer
    hMixSessionHi As Integer
    iChannelLo    As Integer
    iChannelHi    As Integer
    lpMixWaveLo   As Integer
    lpMixWaveHi   As Integer
    hWndNotifyLo  As Integer
    hWndNotifyHi  As Integer
    dwFlagsLo     As Integer
    dwFlagsHi     As Integer
    wLoops        As Integer

End Type

Declare Function WaveMixInit Lib "WAVMIX32.DLL" () As Long
Declare Function WaveMixConfigureInit Lib "WAVMIX32.DLL" (lpConfig As MIXCONFIG) As Long
Declare Function WaveMixActivate _
        Lib "WAVMIX32.DLL" (ByVal hMixSession As Long, _
                            ByVal fActivate As Integer) As Long
Declare Function WaveMixOpenWave _
        Lib "WAVMIX32.DLL" (ByVal hMixSession As Long, _
                            szWaveFilename As Any, _
                            ByVal hInst As Long, _
                            ByVal dwFlags As Long) As Long
Declare Function WaveMixOpenChannel _
        Lib "WAVMIX32.DLL" (ByVal hMixSession As Long, _
                            ByVal iChannel As Long, _
                            ByVal dwFlags As Long) As Long
Declare Function WaveMixPlay Lib "WAVMIX32.DLL" (lpMixPlayParams As Any) As Integer
Declare Function WaveMixFlushChannel _
        Lib "WAVMIX32.DLL" (ByVal hMixSession As Long, _
                            ByVal iChannel As Integer, _
                            ByVal dwFlags As Long) As Integer
Declare Function WaveMixCloseChannel _
        Lib "WAVMIX32.DLL" (ByVal hMixSession As Long, _
                            ByVal iChannel As Integer, _
                            ByVal dwFlags As Long) As Integer
Declare Function WaveMixFreeWave _
        Lib "WAVMIX32.DLL" (ByVal hMixSession As Long, _
                            ByVal lpMixWave As Long) As Integer
Declare Function WaveMixCloseSession _
        Lib "WAVMIX32.DLL" (ByVal hMixSession As Long) As Integer
Declare Sub WaveMixPump Lib "WAVMIX32.DLL" ()
Declare Function WaveMixGetInfo _
        Lib "WAVMIX32.DLL" (lpWaveMixInfo As WAVEMIXINFO) As Integer

Private Function HiWord(ByVal l As Long) As Integer
     '<EhHeader>
     On Error GoTo HiWord_Err
     '</EhHeader>


100     l = l \ &H10000
    
105     HiWord = Val("&H" & Hex$(l))

     '<EhFooter>
     Exit Function

HiWord_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.WAVEMIX.HiWord " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function

Private Function LoWord(ByVal l As Long) As Integer
     '<EhHeader>
     On Error GoTo LoWord_Err
     '</EhHeader>


100     l = l And &HFFFF&
    
105     LoWord = Val("&H" & Hex$(l))

     '<EhFooter>
     Exit Function

LoWord_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.WAVEMIX.LoWord " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function

Function WAVMIX_AddFile(filename As String) As Integer
     '<EhHeader>
     On Error GoTo WAVMIX_AddFile_Err
     '</EhHeader>

        '------------------------------------------------------------
        ' Open a wave file and assign it to the next available
        ' channel.
        '------------------------------------------------------------

        Dim wRtn As Long

100     WAVMIX_AddFile = False
105     If WAVMIX_Quiet Then Exit Function
110     If WaveHandle + 1 = WAVEMIX_MAXCHANNELS Then Exit Function

115     ReDim Preserve lpWaveMix(WaveHandle)
120     lpWaveMix(WaveHandle) = WaveMixOpenWave(hWaveMix, ByVal filename, 0, 0)
125     wRtn = WaveMixOpenChannel(hWaveMix, WaveHandle, 0)
130     WAVMIX_AddFile = WaveHandle
135     WaveHandle = WaveHandle + 1

     '<EhFooter>
     Exit Function

WAVMIX_AddFile_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.WAVEMIX.WAVMIX_AddFile " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function

Sub WAVMIX_SetFile(filename As String, AChannel As Long)
     '<EhHeader>
     On Error GoTo WAVMIX_SetFile_Err
     '</EhHeader>

        '------------------------------------------------------------
        ' Assign a new wave file, FileName, to the specified channel,
        ' AChannel.  If this channel is currently assigned another
        ' wave file, stop playing the channel and free the active
        ' wave file.
        '------------------------------------------------------------

        Dim wRtn As Long
    
100     If WAVMIX_Quiet Then Exit Sub
    
105     If AChannel > UBound(lpWaveMix) Then
110         ReDim Preserve lpWaveMix(AChannel)
115         WaveHandle = AChannel
        End If
    
        ' If another wave is currently assigned to this
        ' channel, free it.
        '    If lpWaveMix(AChannel) <> 0 Then
        '        WAVMIX_StopChannel AChannel
        '        wRtn = WaveMixFreeWave(hWaveMix, lpWaveMix(AChannel))
        '    End If
    
        ' Open the new wave and assign it to this channel.
        'lpWaveMix(AChannel) = WaveMixOpenWave(hWaveMix, ByVal FileName, 1, 0)
120     lpWaveMix(AChannel) = WaveMixOpenWave(hWaveMix, ByVal filename, 0, 0) ' Org
125     wRtn = WaveMixOpenChannel(hWaveMix, AChannel, 0)

     '<EhFooter>
     Exit Sub

WAVMIX_SetFile_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.WAVEMIX.WAVMIX_SetFile " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Sub WAVMIX_Close()
     '<EhHeader>
     On Error GoTo WAVMIX_Close_Err
     '</EhHeader>

        '------------------------------------------------------------
        ' Stop playing all channels and free all wave files, then
        ' close down this WaveMix session.
        '------------------------------------------------------------

        Dim wRtn As Long
        Dim i    As Integer, rc As Integer
    
100     If WAVMIX_Quiet Then Exit Sub

105     If (hWaveMix <> 0) Then

110         For i = 0 To UBound(lpWaveMix)
115             If lpWaveMix(i) <> 0 Then
120                 WAVMIX_StopChannel CLng(i)
125                 rc = WaveMixFreeWave(hWaveMix, lpWaveMix(i))
                End If
            Next

130         wRtn = WaveMixCloseSession(hWaveMix)
135         hWaveMix = 0
        End If

     '<EhFooter>
     Exit Sub

WAVMIX_Close_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.WAVEMIX.WAVMIX_Close " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Function WAVMIX_InitMixer() As Integer
     '<EhHeader>
     On Error GoTo WAVMIX_InitMixer_Err
     '</EhHeader>

        '------------------------------------------------------------
        ' Initialize and activate the WaveMix DLL.
        '------------------------------------------------------------

        Dim wRtn   As Long
        Dim config As MIXCONFIG

100     If WAVMIX_Quiet Then Exit Function

105     WaveHandle = 0
110     ReDim lpWaveMix(0)
115     ChDir App.path
    
120     config.wSize = Len(config)
125     config.dwFlagsHi = 1
130     config.dwFlagsLo = 0
        'Allow stereo sound
135     config.wChannels = 2
140     hWaveMix = WaveMixConfigureInit(config)
145     wRtn = WaveMixActivate(hWaveMix, True)

150     If (wRtn <> 0) Then
155         WAVMIX_InitMixer = False
160         Call WaveMixCloseSession(hWaveMix)
165         hWaveMix = 0
        Else
170         WAVMIX_InitMixer = True
        End If

     '<EhFooter>
     Exit Function

WAVMIX_InitMixer_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.WAVEMIX.WAVMIX_InitMixer " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function

Sub WAVMIX_StopChannel(ByVal ChannelNum As Long)
     '<EhHeader>
     On Error GoTo WAVMIX_StopChannel_Err
     '</EhHeader>

        '------------------------------------------------------------
        ' Stop playing the specified channel.
        '------------------------------------------------------------

        Dim rc As Integer

100     If WAVMIX_Quiet Then Exit Sub
105     If (hWaveMix = 0) Then Exit Sub
    
110     rc = WaveMixFlushChannel(hWaveMix, ChannelNum, 0)

     '<EhFooter>
     Exit Sub

WAVMIX_StopChannel_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.WAVEMIX.WAVMIX_StopChannel " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Sub WAVMIX_Activate(Activate As Long)
     '<EhHeader>
     On Error GoTo WAVMIX_Activate_Err
     '</EhHeader>

        '------------------------------------------------------------
        ' Activate the WaveMix DLL.
        '------------------------------------------------------------

        Dim rc As Integer

100     If WAVMIX_Quiet Then Exit Sub
105     If (hWaveMix = 0) Then Exit Sub

110     rc = WaveMixActivate(hWaveMix, Activate)

     '<EhFooter>
     Exit Sub

WAVMIX_Activate_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.WAVEMIX.WAVMIX_Activate " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Sub WAVMIX_PlayChannel(ChannelNum As Long, LoopWave As Long)
     '<EhHeader>
     On Error GoTo WAVMIX_PlayChannel_Err
     '</EhHeader>

        '------------------------------------------------------------
        ' Play a specified channel, and indicate whether the sound
        ' should be looped.
        '------------------------------------------------------------

        Dim params As MIXPLAYPARAMS
        Dim wRtn   As Long

100     If WAVMIX_Quiet Then Exit Sub
105     If ChannelNum > UBound(lpWaveMix) Then Exit Sub
110     If (hWaveMix = 0) Then Exit Sub

115     params.wSize = Len(params)
120     params.hMixSessionLo = LoWord(hWaveMix)
125     params.hMixSessionHi = HiWord(hWaveMix)
130     params.iChannelLo = LoWord(ChannelNum)
135     params.iChannelHi = HiWord(ChannelNum)
140     params.lpMixWaveLo = LoWord(lpWaveMix(ChannelNum))
145     params.lpMixWaveHi = HiWord(lpWaveMix(ChannelNum))
150     params.hWndNotifyLo = 0
155     params.hWndNotifyHi = 0
160     params.dwFlagsHi = 5
165     params.dwFlagsLo = 0
170     params.wLoops = LoopWave
175     wRtn = WaveMixPlay(params)

     '<EhFooter>
     Exit Sub

WAVMIX_PlayChannel_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.WAVEMIX.WAVMIX_PlayChannel " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

