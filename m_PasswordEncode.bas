Attribute VB_Name = "m_Encode"
Option Explicit

' A Base64 Encoder/Decoder.
'
' This module is used to encode and decode data in Base64 format as described in RFC 1521.
'
' Home page: www.source-code.biz.
' License: GNU/LGPL (www.gnu.org/licenses/lgpl.html).
' Copyright 2007: Christian d'Heureuse, Inventec Informatik AG, Switzerland.
' This module is provided "as is" without warranty of any kind.
Private InitDone       As Boolean

Private Map1(0 To 63)  As Byte

Private Map2(0 To 127) As Byte

' Encodes a string into Base64 format.
' No blanks or line breaks are inserted.
' Parameters:
'   S         a String to be encoded.
' Returns:    a String with the Base64 encoded data.
Public Function Base64EncodeString(ByVal S As String) As String
     '<EhHeader>
     On Error GoTo Base64EncodeString_Err
     '</EhHeader>


100     Base64EncodeString = Base64Encode(ConvertStringToBytes(S))

     '<EhFooter>
     Exit Function

Base64EncodeString_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Encode.Base64EncodeString " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function

' Encodes a byte array into Base64 format.
' No blanks or line breaks are inserted.
' Parameters:
'   InData    an array containing the data bytes to be encoded.
' Returns:    a string with the Base64 encoded data.
Public Function Base64Encode(InData() As Byte)
     '<EhHeader>
     On Error GoTo Base64Encode_Err
     '</EhHeader>


100     Base64Encode = Base64Encode2(InData, UBound(InData) - LBound(InData) + 1)

     '<EhFooter>
     Exit Function

Base64Encode_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Encode.Base64Encode " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function

' Encodes a byte array into Base64 format.
' No blanks or line breaks are inserted.
' Parameters:
'   InData    an array containing the data bytes to be encoded.
'   InLen     number of bytes to process in InData.
' Returns:    a string with the Base64 encoded data.
Public Function Base64Encode2(InData() As Byte, ByVal InLen As Long) As String
     '<EhHeader>
     On Error GoTo Base64Encode2_Err
     '</EhHeader>


100     If Not InitDone Then Init
105     If InLen = 0 Then Base64Encode2 = "": Exit Function
110     Dim ODataLen As Long: ODataLen = (InLen * 4 + 2) \ 3        ' output length without padding
115     Dim OLen     As Long: OLen = ((InLen + 2) \ 3) * 4        ' output length including padding
        Dim Out()    As Byte
120     ReDim Out(0 To OLen - 1) As Byte
125     Dim ip0 As Long: ip0 = LBound(InData)
        Dim ip  As Long
        Dim OP  As Long

130     Do While ip < InLen
135         Dim i0 As Byte: i0 = InData(ip0 + ip): ip = ip + 1
140         Dim i1 As Byte: If ip < InLen Then i1 = InData(ip0 + ip): ip = ip + 1 Else i1 = 0
145         Dim i2 As Byte: If ip < InLen Then i2 = InData(ip0 + ip): ip = ip + 1 Else i2 = 0
150         Dim o0 As Byte: o0 = i0 \ 4
155         Dim o1 As Byte: o1 = ((i0 And 3) * &H10) Or (i1 \ &H10)
160         Dim o2 As Byte: o2 = ((i1 And &HF) * 4) Or (i2 \ &H40)
165         Dim o3 As Byte: o3 = i2 And &H3F
170         Out(OP) = Map1(o0): OP = OP + 1
175         Out(OP) = Map1(o1): OP = OP + 1
180         Out(OP) = IIf(OP < ODataLen, Map1(o2), Asc("=")): OP = OP + 1
185         Out(OP) = IIf(OP < ODataLen, Map1(o3), Asc("=")): OP = OP + 1
        Loop

190     Base64Encode2 = ConvertBytesToString(Out)

     '<EhFooter>
     Exit Function

Base64Encode2_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Encode.Base64Encode2 " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function

' Decodes a string from Base64 format.
' Parameters:
'    s        a Base64 String to be decoded.
' Returns     a String containing the decoded data.
Public Function Base64DecodeString(ByVal S As String) As String
     '<EhHeader>
     On Error GoTo Base64DecodeString_Err
     '</EhHeader>


100     If S = "" Then Base64DecodeString = "": Exit Function
105     Base64DecodeString = ConvertBytesToString(Base64Decode(S))

     '<EhFooter>
     Exit Function

Base64DecodeString_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Encode.Base64DecodeString " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function

' Decodes a byte array from Base64 format.
' Parameters
'   s         a Base64 String to be decoded.
' Returns:    an array containing the decoded data bytes.
Public Function Base64Decode(ByVal S As String) As Byte()
     '<EhHeader>
     On Error GoTo Base64Decode_Err
     '</EhHeader>


100     If Not InitDone Then Init
105     Dim IBuf() As Byte: IBuf = ConvertStringToBytes(S)
110     Dim ILen   As Long: ILen = UBound(IBuf) + 1
115     If ILen Mod 4 <> 0 Then Err.Raise vbObjectError, , "Length of Base64 encoded input string is not a multiple of 4."

120     Do While ILen > 0
125         If IBuf(ILen - 1) <> Asc("=") Then Exit Do
130         ILen = ILen - 1
        Loop

135     Dim OLen  As Long: OLen = (ILen * 3) \ 4
        Dim Out() As Byte
140     ReDim Out(0 To OLen - 1) As Byte
        Dim ip As Long
        Dim OP As Long

145     Do While ip < ILen
150         Dim i0 As Byte: i0 = IBuf(ip): ip = ip + 1
155         Dim i1 As Byte: i1 = IBuf(ip): ip = ip + 1
160         Dim i2 As Byte: If ip < ILen Then i2 = IBuf(ip): ip = ip + 1 Else i2 = Asc("A")
165         Dim i3 As Byte: If ip < ILen Then i3 = IBuf(ip): ip = ip + 1 Else i3 = Asc("A")
170         If i0 > 127 Or i1 > 127 Or i2 > 127 Or i3 > 127 Then Err.Raise vbObjectError, , "Illegal character in Base64 encoded data."
175         Dim b0 As Byte: b0 = Map2(i0)
180         Dim b1 As Byte: b1 = Map2(i1)
185         Dim b2 As Byte: b2 = Map2(i2)
190         Dim b3 As Byte: b3 = Map2(i3)
195         If b0 > 63 Or b1 > 63 Or b2 > 63 Or b3 > 63 Then Err.Raise vbObjectError, , "Illegal character in Base64 encoded data."
200         Dim o0 As Byte: o0 = (b0 * 4) Or (b1 \ &H10)
205         Dim o1 As Byte: o1 = ((b1 And &HF) * &H10) Or (b2 \ 4)
210         Dim o2 As Byte: o2 = ((b2 And 3) * &H40) Or b3
215         Out(OP) = o0: OP = OP + 1
220         If OP < OLen Then Out(OP) = o1: OP = OP + 1
225         If OP < OLen Then Out(OP) = o2: OP = OP + 1
        Loop

230     Base64Decode = Out

     '<EhFooter>
     Exit Function

Base64Decode_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Encode.Base64Decode " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function

Private Sub Init()
     '<EhHeader>
     On Error GoTo Init_Err
     '</EhHeader>


        Dim c As Integer, i As Integer
        ' set Map1
100     i = 0
        Dim startFor As Integer
105     startFor = Asc("A")

110     endFor = Asc("Z")

115     For c = startFor To endFor: Map1(i) = c: i = i + 1: Next
120     startFor = Asc("a")

125     endFor = Asc("z")

130     For c = startFor To endFor: Map1(i) = c: i = i + 1: Next
135     startFor = Asc("0")

140     endFor = Asc("9")

145     For c = startFor To endFor: Map1(i) = c: i = i + 1: Next
150     Map1(i) = Asc("+"): i = i + 1
155     Map1(i) = Asc("/"): i = i + 1

        ' set Map2
160     For i = 0 To 127: Map2(i) = 255: Next
165     For i = 0 To 63: Map2(Map1(i)) = i: Next
170     InitDone = True

     '<EhFooter>
     Exit Sub

Init_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Encode.Init " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Function ConvertStringToBytes(ByVal S As String) As Byte()
     '<EhHeader>
     On Error GoTo ConvertStringToBytes_Err
     '</EhHeader>


100     Dim b1() As Byte: b1 = S
105     Dim l    As Long: l = (UBound(b1) + 1) \ 2
110     If l = 0 Then ConvertStringToBytes = b1: Exit Function
        Dim b2() As Byte
115     ReDim b2(0 To l - 1) As Byte
        Dim P As Long

120     endFor = l - 1

125     For P = 0 To endFor
130         Dim c As Long: c = b1(2 * P) + 256 * CLng(b1(2 * P + 1))
135         If c >= 256 Then c = Asc("?")
140         b2(P) = c
        Next

145     ConvertStringToBytes = b2

     '<EhFooter>
     Exit Function

ConvertStringToBytes_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Encode.ConvertStringToBytes " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function

Private Function ConvertBytesToString(B() As Byte) As String
     '<EhHeader>
     On Error GoTo ConvertBytesToString_Err
     '</EhHeader>


100     Dim l    As Long: l = UBound(B) - LBound(B) + 1
        Dim b2() As Byte
105     ReDim b2(0 To (2 * l) - 1) As Byte
110     Dim p0 As Long: p0 = LBound(B)
        Dim P  As Long

115     endFor = l - 1

120     For P = 0 To endFor: b2(2 * P) = B(p0 + P): Next
125     Dim S As String: S = b2
130     ConvertBytesToString = S

     '<EhFooter>
     Exit Function

ConvertBytesToString_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Encode.ConvertBytesToString " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function
