VERSION 5.00
Object = "{6BF52A50-394A-11D3-B153-00C04F79FAA6}#1.0#0"; "wmp.dll"
Begin VB.Form frmSound 
   Caption         =   "Sound"
   ClientHeight    =   5100
   ClientLeft      =   60
   ClientTop       =   405
   ClientWidth     =   10005
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   222
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   5100
   ScaleWidth      =   10005
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstPlay 
      Height          =   3960
      Left            =   0
      TabIndex        =   2
      Top             =   1125
      Width           =   9960
   End
   Begin VB.CommandButton cmdPlay 
      Caption         =   "Play"
      Height          =   360
      Left            =   6225
      TabIndex        =   1
      Top             =   -30
      Width           =   990
   End
   Begin WMPLibCtl.WindowsMediaPlayer wmp 
      Height          =   1170
      Left            =   -15
      TabIndex        =   0
      Top             =   -30
      Width           =   6240
      URL             =   ""
      rate            =   1
      balance         =   0
      currentPosition =   0
      defaultFrame    =   ""
      playCount       =   1
      autoStart       =   -1  'True
      currentMarker   =   0
      invokeURLs      =   -1  'True
      baseURL         =   ""
      volume          =   50
      mute            =   0   'False
      uiMode          =   "full"
      stretchToFit    =   0   'False
      windowlessVideo =   0   'False
      enabled         =   -1  'True
      enableContextMenu=   -1  'True
      fullScreen      =   0   'False
      SAMIStyle       =   ""
      SAMILang        =   ""
      SAMIFilename    =   ""
      captioningID    =   ""
      enableErrorDialogs=   0   'False
      _cx             =   11007
      _cy             =   2064
   End
End
Attribute VB_Name = "frmSound"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim pBusy As Boolean

Public Sub cmdPlay_Click()
     '<EhHeader>
     On Error GoTo cmdPlay_Click_Err
     '</EhHeader>

        On Error Resume Next

        Dim i%
        'pBusy = False
100     g_Sound = True

105     For i = 0 To lstPlay.ListCount - 1
110         wmp.URL = lstPlay.List(i)
115         wmp.Controls.Play
120         pBusy = True ' Set flag after play sound

125         Do While pBusy
130             DoEvents: DoEvents
135             Sleep 50

140             DoEvents: DoEvents
            Loop

145     Next i

        '=================== Clear ====================================================
150     lstPlay.Clear
        '=================== Clear ====================================================
     '<EhFooter>
     Exit Sub

cmdPlay_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmSound.cmdPlay_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub Form_Load()
     '<EhHeader>
     On Error GoTo Form_Load_Err
     '</EhHeader>


100     wmp.settings.volume = 100
105     g_Sound = True
     '<EhFooter>
     Exit Sub

Form_Load_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmSound.Form_Load " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub Form_Unload(Cancel As Integer)
     '<EhHeader>
     On Error GoTo Form_Unload_Err
     '</EhHeader>
100  g_Sound = False
     '<EhFooter>
     Exit Sub

Form_Unload_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmSound.Form_Unload " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub wmp_PlayStateChange(ByVal NewState As Long)
     '<EhHeader>
     On Error GoTo wmp_PlayStateChange_Err
     '</EhHeader>

100  g_Sound = True
105     If NewState = 3 Then
110         pBusy = True
        Else
115         pBusy = False
        End If

     '<EhFooter>
     Exit Sub

wmp_PlayStateChange_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmSound.wmp_PlayStateChange " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub
