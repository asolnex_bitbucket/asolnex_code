VERSION 5.00
Object = "{C0A63B80-4B21-11D3-BD95-D426EF2C7949}#1.0#0"; "vsflex7l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSCOMM32.OCX"
Begin VB.Form frmMain 
   Caption         =   "Queue System Time Stamp"
   ClientHeight    =   9990
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   18960
   Icon            =   "frmMain.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   9990
   ScaleWidth      =   18960
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin MSCommLib.MSComm MSComm1 
      Left            =   18105
      Top             =   4890
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DTREnable       =   -1  'True
   End
   Begin VB.ListBox lstDSP 
      Height          =   3570
      Left            =   13095
      TabIndex        =   11
      Top             =   -135
      Visible         =   0   'False
      Width           =   2895
   End
   Begin MSWinsockLib.Winsock sck 
      Left            =   14235
      Top             =   405
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
      Protocol        =   1
   End
   Begin VB.Frame fMain 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   9615
      Left            =   0
      TabIndex        =   1
      Top             =   30
      Width           =   19035
      Begin VB.Frame fChart 
         Caption         =   "�ӹǹö�����Ŵ�Թ���������ª������ (ᴧ=ö��� ����=��Ŵ����)"
         Height          =   3345
         Left            =   90
         TabIndex        =   12
         Top             =   5760
         Width           =   18945
         Begin MSChart20Lib.MSChart chart 
            Height          =   2070
            Left            =   210
            OleObjectBlob   =   "frmMain.frx":08CA
            TabIndex        =   13
            Top             =   270
            Width           =   18570
         End
         Begin VB.Label lblSiteName 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "SiteName"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   222
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   525
            Left            =   75
            TabIndex        =   14
            Top             =   2580
            Width           =   18360
         End
      End
      Begin VB.ListBox lsSpeak 
         Height          =   2985
         ItemData        =   "frmMain.frx":2B3D
         Left            =   10530
         List            =   "frmMain.frx":2B3F
         TabIndex        =   10
         Top             =   960
         Visible         =   0   'False
         Width           =   1740
      End
      Begin VB.Frame fCarWait 
         Caption         =   "ö�ѧ������Ѻ���"
         Height          =   2175
         Left            =   13455
         TabIndex        =   8
         Top             =   1320
         Width           =   5535
         Begin VSFlex7LCtl.VSFlexGrid fgCarIN 
            Height          =   1815
            Left            =   135
            TabIndex        =   9
            Top             =   255
            Width           =   5250
            _cx             =   9260
            _cy             =   3201
            _ConvInfo       =   1
            Appearance      =   1
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   -2147483635
            ForeColorSel    =   -2147483634
            BackColorBkg    =   -2147483636
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   1
            HighLight       =   0
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   0
            SelectionMode   =   0
            GridLines       =   1
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   1
            Cols            =   4
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"frmMain.frx":2B41
            ScrollTrack     =   0   'False
            ScrollBars      =   2
            ScrollTips      =   0   'False
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   0
            ShowComboButton =   -1  'True
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            Begin VB.Timer tmrSpeak 
               Interval        =   1200
               Left            =   0
               Top             =   0
            End
         End
      End
      Begin VB.Frame fCarOut 
         Caption         =   "ö�͡�ҡ�ç�ҹ"
         Height          =   2280
         Left            =   13455
         TabIndex        =   6
         Top             =   3510
         Width           =   5535
         Begin VSFlex7LCtl.VSFlexGrid fgCarOut 
            Height          =   1875
            Left            =   90
            TabIndex        =   7
            Top             =   285
            Width           =   5250
            _cx             =   9260
            _cy             =   3307
            _ConvInfo       =   1
            Appearance      =   1
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   -2147483635
            ForeColorSel    =   -2147483634
            BackColorBkg    =   -2147483636
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   1
            HighLight       =   0
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   0
            SelectionMode   =   0
            GridLines       =   1
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   1
            Cols            =   4
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"frmMain.frx":2BC9
            ScrollTrack     =   0   'False
            ScrollBars      =   2
            ScrollTips      =   0   'False
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   0
            ShowComboButton =   -1  'True
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
         End
      End
      Begin VB.Frame FQWait 
         Caption         =   "ö�����¡"
         Height          =   4485
         Left            =   7425
         TabIndex        =   4
         Top             =   1320
         Width           =   6015
         Begin VB.Timer tmrQ 
            Enabled         =   0   'False
            Interval        =   1000
            Left            =   0
            Top             =   0
         End
         Begin VSFlex7LCtl.VSFlexGrid fgQWait 
            Height          =   4110
            Left            =   165
            TabIndex        =   5
            Top             =   240
            Width           =   5715
            _cx             =   10081
            _cy             =   7250
            _ConvInfo       =   1
            Appearance      =   1
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   -2147483635
            ForeColorSel    =   -2147483634
            BackColorBkg    =   -2147483636
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   0
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   0
            SelectionMode   =   1
            GridLines       =   1
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   38
            Cols            =   8
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"frmMain.frx":2C50
            ScrollTrack     =   0   'False
            ScrollBars      =   2
            ScrollTips      =   0   'False
            MergeCells      =   1
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   0
            ShowComboButton =   -1  'True
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
         End
      End
      Begin VB.Frame FCur 
         Caption         =   "Current Loading"
         Height          =   4500
         Left            =   90
         TabIndex        =   2
         Top             =   1320
         Width           =   7320
         Begin VSFlex7LCtl.VSFlexGrid fgCurQ 
            Height          =   4125
            Left            =   150
            TabIndex        =   3
            Top             =   240
            Width           =   7020
            _cx             =   12382
            _cy             =   7276
            _ConvInfo       =   1
            Appearance      =   1
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   222
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   -2147483635
            ForeColorSel    =   -2147483634
            BackColorBkg    =   8421504
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   0
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   0
            SelectionMode   =   1
            GridLines       =   1
            GridLinesFixed  =   1
            GridLineWidth   =   1
            Rows            =   17
            Cols            =   8
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"frmMain.frx":2D4A
            ScrollTrack     =   0   'False
            ScrollBars      =   0
            ScrollTips      =   0   'False
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   0
            ShowComboButton =   -1  'True
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   16777215
            ForeColorFrozen =   0
            WallPaperAlignment=   9
         End
      End
   End
   Begin MSComctlLib.StatusBar stb 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   0
      Top             =   9615
      Width           =   18960
      _ExtentX        =   33443
      _ExtentY        =   661
      SimpleText      =   "Test"
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   27781
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            TextSave        =   "28/3/2559"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            TextSave        =   "9:13"
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuReport 
         Caption         =   "&Report"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "&Exit"
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuTicket 
         Caption         =   "&Ticket Printer"
      End
      Begin VB.Menu mnuKeyPad 
         Caption         =   "&KeyPad"
      End
      Begin VB.Menu mnuQPass 
         Caption         =   "&Queue Pass"
      End
   End
   Begin VB.Menu mnuSetting 
      Caption         =   "&Setting"
      Begin VB.Menu mnuDB 
         Caption         =   "DB Config"
      End
      Begin VB.Menu mnuDSP_TEST 
         Caption         =   "DSP TEST"
      End
      Begin VB.Menu mnuClear 
         Caption         =   "Clear Queue"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuAbout 
         Caption         =   "About"
      End
   End
   Begin VB.Menu mnuQWD 
      Caption         =   "QDel"
      Visible         =   0   'False
      Begin VB.Menu mnuQWaitDel 
         Caption         =   "Delete"
      End
   End
   Begin VB.Menu mnuQCD 
      Caption         =   "QCurDel"
      Visible         =   0   'False
      Begin VB.Menu mnuQCurDel 
         Caption         =   "Delete"
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private cCount%



Private Sub fgCurQ_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
     '<EhHeader>
     On Error GoTo fgCurQ_MouseUp_Err
     '</EhHeader>


100     If Button = MouseButtonConstants.vbRightButton And fgCurQ.MouseRow > 0 Then
105         If Len(fgCurQ.TextMatrix(fgCurQ.MouseRow, 2)) > 0 Then
110             PopupMenu mnuQCD
            End If
        End If

     '<EhFooter>
     Exit Sub

fgCurQ_MouseUp_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.fgCurQ_MouseUp " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub fgQWait_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
     '<EhHeader>
     On Error GoTo fgQWait_MouseUp_Err
     '</EhHeader>


100     If Button = MouseButtonConstants.vbRightButton And fgQWait.MouseRow > 0 And fgQWait.MouseRow < fgQWait.Rows - 1 Then
105         If Len(fgQWait.TextMatrix(fgQWait.MouseRow, 0)) > 0 Then
110             PopupMenu mnuQWD
            End If
        End If

     '<EhFooter>
     Exit Sub

fgQWait_MouseUp_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.fgQWait_MouseUp " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub Form_Load()
     '<EhHeader>
     On Error GoTo Form_Load_Err
     '</EhHeader>

100     If App.PrevInstance = True Then
105         MsgBox "������� Queue ��ҧ����� Task Manager" & vbCrLf & "��س� End Task ��͹�Դ�����"
110         Unload Me ' ������ End ����������зӧҹ���������Ẻ�������ó�

            Exit Sub

        End If
115     Call LogNTEvent("Information from " & App.EXEName & " Start : " & Now, EVENTLOG_INFORMATION_TYPE, 1001) ' Event Log Start Program
120     IniFile = App.Path & "\config.ini"
        '<Load config parameter MySql>
125     dbHost = m_Ini.ReadINI("Config", "dbHost", IniFile)
130     dbUser = m_Ini.ReadINI("Config", "dbUser", IniFile)
135     dbDBName = m_Ini.ReadINI("Config", "dbDBName", IniFile)
140     dbPass = m_Encode.Base64DecodeString(m_Ini.ReadINI("Config", "dbPass", IniFile))
        'Debug.Print m_Encode.Base64EncodeString("351286")
        'Debug.Print m_Encode.Base64DecodeString("MzUxMjg2")
        '</Load config parameter MySql>
    
        '<Connect Mysql>
145     m_DB.Connect
        '</Connect Mysql>
    
        '<Connect Database>
Connect:
150     If Not g_Connected Then
155         frmDBConfig.Show 1
160         If Not g_forceClose Then
165             GoTo Connect
            End If
170         Unload Me

            Exit Sub

        End If
        '</Connect Database>

175     lblSiteName.Caption = m_Ini.ReadINI("Config", "SiteName", IniFile)  '�����Ң�

180     maxLane = Val(m_Ini.ReadINI("Config", "maxLane", IniFile)) ' �ӹǹ�Ź
185     b1 = Val(m_Ini.ReadINI("Config", "B1", IniFile))
190     b2 = Val(m_Ini.ReadINI("Config", "B2", IniFile))
        '==============PRINTER IP PORT==============
195     m_printer.PRT_IP = m_Ini.ReadINI("Config", "PRT_IP", IniFile)
200     m_printer.PRT_PORT = Val(m_Ini.ReadINI("Config", "PRT_PORT", IniFile))
        '========================================
    
        '==============LED DISPLAY========================
205     m_LED.DSP_IP = m_Ini.ReadINI("Config", "DSP_IP", IniFile)
210     m_LED.DSP_PORT = Val(m_Ini.ReadINI("Config", "DSP_PORT", IniFile))
215     m_LED.DSP_Bing = m_Ini.ReadINI("Config", "DSP_Bing", IniFile)
220     m_LED.DSP_Color = m_Ini.ReadINI("Config", "DSP_Color", IniFile)
        '===============================================
    
        '=============LCD 32"=======================
225     frmDSP32.Left = Val(m_Ini.ReadINI("Config", "xLeft", IniFile))
230     frmDSP32.Top = 0
235     frmDSP32.Show
        '==========================================
    
        '==============Initial Queue Data===============
240     Call m_Q.QTypeLoad 'Initial Q
245     Call m_Q.LaneTypeLoad '��Ŵ�������Ź�˹���¡��ǻ����������
        '=========================================
        '==============Load KeyPad=================
250     frmKeyPad.Show
255     frmKeyPad.Hide
        '=========================================
    
        '=============== Parking Log====================
260     m_Parking.pkSystem = m_Ini.ReadINI("Config", "pkSystem", IniFile)
265     If m_Parking.pkSystem Then
270         Call m_Parking.initialParking(m_Ini.ReadINI("Config", "pkHost", IniFile), m_Ini.ReadINI("Config", "pkDB", IniFile), m_Ini.ReadINI("Config", "pkUser", IniFile), m_Encode.Base64DecodeString(m_Ini.ReadINI("Config", "pkPass", IniFile)))
        End If
        '============================================
    
275     mPark = m_Ini.ReadINI("Config", "mPark", IniFile) '������д�������� False = ����� ��������
    
        '==============First Time Read Database=========
280     Call ReadData
285     Call m_Q.initialCall
290     Call m_Q.curCall
        '========================================
    
        '==============Timer Read Database===========
295     g_dbRead = Val(m_Ini.ReadINI("Config", "dbRead", IniFile))
300     tmrQ.Enabled = True
        '========================================
305     LocalPort = Val(m_Ini.ReadINI("Config", "LocalPort", IniFile))
310     Call initialSocket 'Create Socket
315     frmSound.Show
320     frmSound.Visible = False
    
        '==============Speaker IP============
325     SP_IP = m_Ini.ReadINI("Config", "SP_IP", IniFile)
330     SP_PORT = m_Ini.ReadINI("Config", "SP_PORT", IniFile)
        '=================================

     '<EhFooter>
     Exit Sub

Form_Load_Err:

    Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.Form_Load " & _
               "at line " & Erl, EVENTLOG_ERROR_TYPE, Err.Number)
     Resume Next
     '</EhFooter>
End Sub

Private Sub initialSocket()
     '<EhHeader>
     On Error GoTo initialSocket_Err
     '</EhHeader>


100     sck.Close
105     sck.Protocol = sckUDPProtocol
110     sck.Bind LocalPort

     '<EhFooter>
     Exit Sub

initialSocket_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.initialSocket " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub Form_Resize()

    On Error Resume Next

    Dim margin%, fWidth%, fheight%, marginFG(2) As Integer, row%, rowH%
    fMain.Move 0, 0, Me.ScaleWidth, Me.ScaleHeight - stb.Height
    margin = 50
    marginFG(0) = 100 'margin left
    marginFG(1) = 250 'margin top
    fWidth = fMain.Width / 3

    '============Frame===============================================================

    FCur.Move margin, margin, fWidth - margin, fMain.Height / 2
    FQWait.Move (margin * 2) + FCur.Width, margin, fWidth - (margin * 2), fMain.Height / 2
    fCarWait.Move (margin * 3) + (FCur.Width + FQWait.Width), margin, fWidth, (fMain.Height / 4) - margin
    fCarOut.Move (margin * 3) + (FCur.Width + FQWait.Width), (fMain.Height / 4) + margin, fCarWait.Width, fCarWait.Height
    fChart.Move margin, FCur.Height + margin, fMain.Width - margin, fMain.Height / 2 - stb.Height / 2
    '================================================================================

    '=======================FlexGrid====================================================

    With fgCurQ
        .Move marginFG(0), marginFG(1), FCur.Width - (marginFG(0) * 2), FCur.Height - (marginFG(1) * 1.5)
        .ColWidth(0) = .Width * 0.1
        .ColWidth(1) = .Width * 0.2
        .ColWidth(2) = .Width * 0.1
        .ColWidth(3) = .Width * 0.1
        .ColWidth(4) = .Width * 0.1
        .ColWidth(5) = .Width * 0.2
        .ColWidth(6) = .Width * 0.2
    End With

    With fgQWait
        .Move marginFG(0), marginFG(1), FQWait.Width - (marginFG(0) * 2), FQWait.Height - (marginFG(1) * 1.5)
        .ColWidth(0) = .Width * 0.1
        .ColWidth(1) = .Width * 0.1
        .ColWidth(2) = .Width * 0.15
        .ColWidth(3) = .Width * 0.2
        .ColWidth(4) = .Width * 0.15
        .ColWidth(5) = .Width * 0.2
        .ColWidth(6) = .Width * 0.1
    End With

    With fgCarIN
        .Move marginFG(0), marginFG(1), fCarWait.Width - (marginFG(0) * 2), fCarWait.Height - (marginFG(1) * 1.5)
        .ColWidth(0) = .Width * 0.2
        .ColWidth(1) = .Width * 0.2
        .ColWidth(2) = .Width * 0.2
        .ColWidth(3) = .Width * 0.4
    End With

    With fgCarOut
        .Move marginFG(0), marginFG(1), fCarOut.Width - (marginFG(0) * 2), fCarOut.Height - (marginFG(1) * 1.5)
        .ColWidth(0) = .Width * 0.2
        .ColWidth(1) = .Width * 0.2
        .ColWidth(2) = .Width * 0.2
        .ColWidth(3) = .Width * 0.4
    End With

    '================================================================================

    '=========================Chart & Sitename================================================
    chart.Move 100, 200, fChart.Width - 200, fChart.Height - 100 - lblSiteName.Height
    lblSiteName.Move 0, chart.Height + 80, Me.Width
    '==============================================================================

End Sub

Private Sub Form_Unload(Cancel As Integer)
     '<EhHeader>
     On Error GoTo Form_Unload_Err
     '</EhHeader>


        Dim i%

100     For i = Forms.Count - 1 To 1 Step -1
105         Unload Forms(i)
        Next

110     Call LogNTEvent("Information from " & App.EXEName & " Close : " & Now, EVENTLOG_INFORMATION_TYPE, 1001) ' Event Log Start Program

     '<EhFooter>
     Exit Sub

Form_Unload_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.Form_Unload " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub mnuAbout_Click()
     '<EhHeader>
     On Error GoTo mnuAbout_Click_Err
     '</EhHeader>


        Dim tmp$
100     tmp = "QUEUE TIME STAMP "
105     tmp = tmp & "Version 1.0.0 [2015-12-15]" & vbCrLf & vbCrLf
110     tmp = tmp & "Author: Digital Technology Consultants Co.,Ltd." & vbCrLf & vbCrLf
115     tmp = tmp & "TEL: (02) 679-8511" & vbCrLf & vbCrLf
120     tmp = tmp & "Email: info@dtc9.com" & vbCrLf & vbCrLf
125     tmp = tmp & "Website: http://www.dtc9.com" & vbCrLf & vbCrLf
130     tmp = tmp & "Facebook: http://fb.me/dtcled" & vbCrLf & vbCrLf
135     MsgBox tmp, vbInformation, "About..."

     '<EhFooter>
     Exit Sub

mnuAbout_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.mnuAbout_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub mnuClear_Click()
     '<EhHeader>
     On Error GoTo mnuClear_Click_Err
     '</EhHeader>


100     If Not MsgBox("��ͧ���ź�����Ť�Ƿ����� ��������� ?", vbCritical + vbYesNo, "Confirm") = vbYes Then

            Exit Sub

        End If
105     Call m_Q.ClearQ
110     lstDSP.Clear
115     Call frmDSP32.ClearQ
120     g_ForceRead = True

     '<EhFooter>
     Exit Sub

mnuClear_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.mnuClear_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub mnuDB_Click()
     '<EhHeader>
     On Error GoTo mnuDB_Click_Err
     '</EhHeader>


100     frmDBConfig.Show 1

     '<EhFooter>
     Exit Sub

mnuDB_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.mnuDB_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub mnuDSP_TEST_Click()
     '<EhHeader>
     On Error GoTo mnuDSP_TEST_Click_Err
     '</EhHeader>


100     Call m_LED.DSP_TEST

     '<EhFooter>
     Exit Sub

mnuDSP_TEST_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.mnuDSP_TEST_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub mnuExit_Click()
     '<EhHeader>
     On Error GoTo mnuExit_Click_Err
     '</EhHeader>


100     Unload Me

     '<EhFooter>
     Exit Sub

mnuExit_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.mnuExit_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub mnuKeyPad_Click()
     '<EhHeader>
     On Error GoTo mnuKeyPad_Click_Err
     '</EhHeader>


100     frmKeyPad.Show

     '<EhFooter>
     Exit Sub

mnuKeyPad_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.mnuKeyPad_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub mnuQCurDel_Click()
     '<EhHeader>
     On Error GoTo mnuQCurDel_Click_Err
     '</EhHeader>


        Dim QID As Double
100     QID = Val(fgCurQ.TextMatrix(fgCurQ.row, fgCurQ.Cols - 1))
105     If QID > 0 Then
110         If Not MsgBox("��ͧ���ź�����Ť�� " & fgCurQ.TextMatrix(fgCurQ.row, 2) & " ��������� ?", vbCritical + vbYesNo, "Confirm") = vbYes Then

                Exit Sub

            End If
115         Call m_Q.QDel(QID)
120         Call m_Q.RemoveQ2Lane(QID) ' ��Ҥ���͡�ҡ �Ź
125         g_ForceRead = True
        End If

     '<EhFooter>
     Exit Sub

mnuQCurDel_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.mnuQCurDel_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub mnuQPass_Click()
     '<EhHeader>
     On Error GoTo mnuQPass_Click_Err
     '</EhHeader>


100     frmQPass.Show

     '<EhFooter>
     Exit Sub

mnuQPass_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.mnuQPass_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub mnuQWaitDel_Click()
     '<EhHeader>
     On Error GoTo mnuQWaitDel_Click_Err
     '</EhHeader>


        Dim QID As Double
100     QID = Val(fgQWait.TextMatrix(fgQWait.row, fgQWait.Cols - 1))
105     If QID > 0 Then
110         If Not MsgBox("��ͧ���ź�����Ť�� " & fgQWait.TextMatrix(fgQWait.row, 0) & " ��������� ?", vbCritical + vbYesNo, "Confirm") = vbYes Then

                Exit Sub

            End If
115         Call m_Q.QDel(QID)
120         g_ForceRead = True
        End If

     '<EhFooter>
     Exit Sub

mnuQWaitDel_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.mnuQWaitDel_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub mnuReport_Click()
     '<EhHeader>
     On Error GoTo mnuReport_Click_Err
     '</EhHeader>

100     frmReport.Show

     '<EhFooter>
     Exit Sub

mnuReport_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.mnuReport_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub mnuTicket_Click()
     '<EhHeader>
     On Error GoTo mnuTicket_Click_Err
     '</EhHeader>

100     Call m_Q.setCarNo
105     frmTicket.Show

     '<EhFooter>
     Exit Sub

mnuTicket_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.mnuTicket_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub ReadData()
     '<EhHeader>
     On Error GoTo ReadData_Err
     '</EhHeader>

100     If Not dbBusy Then
105         Call m_DB.Disconnect
110         Call m_DB.Connect
115         fgCarIN.Rows = fgCarIN.FixedRows
120         fgCarOut.Rows = fgCarOut.Rows
125         If m_Parking.pkSystem Then
130             Call m_Parking.getParking
135             Call m_Q.QCarInOut(fgCarIN, True)
140             Call m_Q.QCarInOut(fgCarOut, False)
145             Call m_Q.UpdateCarTimeOUT
            End If
150         Call m_Q.CurQ(fgCurQ)
155         Call m_Q.QWait(fgQWait)
160         Call m_Q.CurQonLane
165         Call m_Q.QPass(frmQPass.fgQPass)
170         Call m_Q.CompleteInHr ' Chart
        
        
        End If
     '<EhFooter>
     Exit Sub

ReadData_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.ReadData " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub sck_DataArrival(ByVal bytesTotal As Long)
     '<EhHeader>
     On Error GoTo sck_DataArrival_Err
     '</EhHeader>

        On Error Resume Next

        Dim recStr$
100     sck.GetData recStr
        'Debug.Print recStr
105     If recStr = "$$$" Then Exit Sub
        'Debug.Print recStr
110     m_sck.processData (recStr)

     '<EhFooter>
     Exit Sub

sck_DataArrival_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.sck_DataArrival " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub tmrQ_Timer()
    '<EhHeader>
    On Error Resume Next
    '</EhHeader>


    If cCount > g_dbRead Or g_ForceRead Then
        g_ForceRead = False
        cCount = 0
        Call ReadData
    End If

    cCount = cCount + 1
End Sub

Private Sub m_Speak3_2(QNum As String, LaneNum As String)    ' ��˹�ҷ��ٴ ˹�� 3 ��ѧ 2
     '<EhHeader>
     On Error GoTo m_Speak3_2_Err
     '</EhHeader>


        Dim i     As Integer
        Dim sPath As String
    
        ' 2009.12.15
100     If SkipSound = False Then

105         Call Speech(App.Path + "\Prompt\BLIP")
110         Annouce = "Prompt5_"
            
115         sPath = App.Path & "\Prompt\Prompt5\"
120         Call Speech(sPath & Annouce & "Invite")  ' �ѹ�ԭ
            
125         Call Speech(sPath & Annouce & "Queue")  ' �����Ţ
130         Call SpeechNumber(QNum, sPath & Annouce)
135         Call Speech(sPath & Annouce & "Lane") ' ����Ź
140         Call SpeechOrdinal(CurrencyString(Val(LaneNum)), sPath)
            'Call Speech(sPath & Annouce & "Sir") '��Ѻ/���
                
145         If g_Sound Then Call frmSound.cmdPlay_Click 'frmSoundDx.Timer1.Enabled = True ' �ٴ��������

        End If ' If SkipSound = True Then

     '<EhFooter>
     Exit Sub

m_Speak3_2_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.m_Speak3_2 " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub Speech(filename As String)   ' �ӷ�����ҹ
     '<EhHeader>
     On Error GoTo Speech_Err
     '</EhHeader>
100  If Len(filename) = 0 And g_Sound Then Exit Sub
105  frmSound.lstPlay.AddItem filename & ".wav" 'frmSoundDx.List1.AddItem filename & ".wav"

    '    If Len(filename) = 0 Then Exit Sub
    '    '    WAVMIX_SetFile filename & ".wav", 1
    '    '    WAVMIX_PlayChannel 1, 0
    '    frmSoundDx.List1.AddItem filename & ".wav"
    '
    '    'frmSoundDx.Timer1.Enabled = True
     '<EhFooter>
     Exit Sub

Speech_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.Speech " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub SpeechNumber(InNum As String, sPath As String)
     '<EhHeader>
     On Error GoTo SpeechNumber_Err
     '</EhHeader>


        Dim Prompt   As String
        'Dim OneDigit As String
        Dim i        As Byte
        Dim j        As Byte
        Dim Qsplit() As String

100     If Len(InNum) > 3 Then ' �ó� ������� 001
105         Qsplit = Split(InNum, "-")

110         For j = 0 To 1
115             For i = 1 To Len(Qsplit(j))
120                 Prompt = Mid(Qsplit(j), i, 1)
125                 Call Speech(sPath & Prompt)       ' **
130             Next i

135             If j = 0 Then
140                 Call Speech(App.Path + "\Prompt\PromptUSA\PromptUSA_to")
                End If
145         Next j

        Else

150         For i = 1 To Len(InNum)
155             Prompt = Mid(InNum, i, 1)
160             Call Speech(sPath & Prompt)       ' **
165         Next i

        End If

     '<EhFooter>
     Exit Sub

SpeechNumber_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.SpeechNumber " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub SpeechOrdinal(ByVal Str1 As String, sPath As String)
     '<EhHeader>
     On Error GoTo SpeechOrdinal_Err
     '</EhHeader>

        On Error Resume Next

        Dim i       As Byte
        Dim NameWav As String ' name of Wav file
        Dim Scpt    As Byte ' Sub Script of Array
        Dim Prompt  As Variant
100     If Len(Str1) = 0 Then Exit Sub
105     Scpt = 0
110     Prompt = Split(Str1, Annouce)

115     For i = LBound(Prompt) + 1 To UBound(Prompt)
120         Scpt = Scpt + 1
125         NameWav = Annouce & Prompt(Scpt)
130         Call Speech(sPath & NameWav)
        Next

     '<EhFooter>
     Exit Sub

SpeechOrdinal_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.SpeechOrdinal " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Function CurrencyString(Number As Variant) As String
     '<EhHeader>
     On Error GoTo CurrencyString_Err
     '</EhHeader>


        Dim cType(16) As String
        Dim Num(9)    As String
        Dim l, i As Integer
        Dim str        As String
        Dim VarAnnouce As String
100     If Len(Number) > 3 Then  ' Add 29 April 2003
105         Number = Right(Number, 3)
        End If
110     VarAnnouce = Annouce ' �������ͧ���§���оٴ
115     Num(0) = "":        Num(1) = VarAnnouce & "1"
120     Num(2) = VarAnnouce & "2":  Num(3) = VarAnnouce & "3"
125     Num(4) = VarAnnouce & "4":      Num(5) = VarAnnouce & "5"
130     Num(6) = VarAnnouce & "6":    Num(7) = VarAnnouce & "7"
135     Num(8) = VarAnnouce & "8":  Num(9) = VarAnnouce & "9"
140     cType(1) = "":        cType(2) = ""
145     cType(3) = "":        cType(4) = ""
150     cType(5) = VarAnnouce & "10":    cType(6) = VarAnnouce & "100"
155     cType(7) = VarAnnouce & "1000":   cType(8) = VarAnnouce & "10000"
160     cType(9) = VarAnnouce & "100000":  cType(10) = VarAnnouce & "1000000"
165     cType(11) = VarAnnouce & "10":  cType(12) = VarAnnouce & "100"
170     cType(13) = VarAnnouce & "1000":  cType(14) = VarAnnouce & "10000"
175     cType(15) = VarAnnouce & "100000": cType(16) = VarAnnouce & "1000000"
180     If Not (IsNumeric(Number)) Then
185         CurrencyString = "������͡�����������繢����ŷ����������Ţ"

            Exit Function

        End If
190     str = Format(Number, "###0.00")
195     l = Len(str)
  
200     If l > 16 Then
205         CurrencyString = "�ѧ��ѹ�������ö�Ҥ�ҷ���ҡ������ҹ��ҹ��"

            Exit Function

210     ElseIf l = 0 Then
215         CurrencyString = ""

            Exit Function

        End If
220     ReDim LStr(l + 1) As String
225     LStr(5) = 0

230     For i = 1 To l
235         LStr(l + 1 - i) = Mid(str, i, 1)
240     Next i

245     str = ""

250     For i = l To 4 Step -1
255         If ((i = 5) Or (i = 11)) And (LStr(i) = 2) Then
260             Num(2) = VarAnnouce & "20" ' ���
265         ElseIf ((i = 5) Or (i = 11)) And (LStr(i) = 1) Then
270             Num(1) = ""
275         ElseIf (i = 4) And (LStr(i) = 1) And (l > 4) And (LStr(5) <> 0) Then
280             Num(1) = VarAnnouce & "11" ' ���
            End If
285         If l > 10 Then
290             If (i = 10) And (LStr(i) = 1) And (l > 10) And (LStr(11) <> 0) Then
295                 Num(1) = VarAnnouce & "11"
                End If
            End If
300         If (LStr(i) <> 0) Or (i = 10) Then
305             str = str & Num(LStr(i)) & cType(i)
            End If
310         Num(2) = VarAnnouce & "2"
315         Num(1) = VarAnnouce & "1"
320     Next i
  
        'If Not ((L = 4) And (LStr(4) = 0)) Then
        '  Str = Str & VarAnnouce & "�ҷ"
        'End If
  
325     If LStr(2) = 2 Then
330         Num(2) = VarAnnouce & "20"
        End If
335     If (LStr(2) <> 0) Then
340         If LStr(2) <> 1 Then
345             str = str & Num(LStr(2))
            End If
350         str = str & VarAnnouce & "10"
        End If
355     Num(2) = VarAnnouce & "2"
360     If (LStr(1) = 1) And (LStr(2) <> 0) Then
365         Num(1) = VarAnnouce & "11"
        End If
370     str = str & Num(LStr(1))
375     If (LStr(1) <> 0) Or (LStr(2) <> 0) Then
380         str = str & VarAnnouce & "ʵҧ��"
        End If
385     CurrencyString = str + ""

     '<EhFooter>
     Exit Function

CurrencyString_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.CurrencyString " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function

Private Sub tmrSpeak_Timer()
    '<EhHeader>
    On Error Resume Next
    '</EhHeader>


    Dim tmp   As String ' "_999_99"
    Dim exp() As String
    Dim i%

    On Error Resume Next

    If g_Sound Then
        If lsSpeak.ListCount > 0 And frmSound.lstPlay.ListCount = 0 Then ' If lsSpeak.ListCount > 0 Then
    
            tmp = lsSpeak.List(0)
            lsSpeak.RemoveItem (0)
            exp = Split(tmp, Space$(1)) '0=> QNum 1=>CarNo 2=> Lane
        
            tmp = exp(1) & "|" & exp(0) & "|" & Val(exp(2))
        
            For i = 0 To frmMain.lstDSP.ListCount
                If InStr(1, frmMain.lstDSP.List(i), exp(0)) > 0 Then
                    frmMain.lstDSP.RemoveItem (i) 'ź��ҫ�ӡ�͹

                    Exit For

                End If
            Next i
        
            frmMain.lstDSP.AddItem tmp, 0
            m_LED.DSP_BingOn = True
            Call m_Q.curCall
            Call sendToSpeaker(exp(0), exp(1), Val(exp(2)))
            Call m_Speak3_2(exp(0), exp(2))
        End If
    End If
End Sub

Sub sendToSpeaker(QNum As String, QCarNo As String, Lane As Integer)
     '<EhHeader>
     On Error GoTo sendToSpeaker_Err
     '</EhHeader>

100     With sck
105         .RemoteHost = SP_IP
110         .RemotePort = SP_PORT
115         .SendData QNum & "|" & QCarNo & "|" & Lane
        End With

120     'Call initialSocket
     '<EhFooter>
     Exit Sub

sendToSpeaker_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmMain.sendToSpeaker " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

