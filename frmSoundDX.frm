VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmSoundDx 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DirectSound Streaming"
   ClientHeight    =   5205
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   12885
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSoundDX.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5205
   ScaleWidth      =   12885
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   50
      Left            =   5760
      Top             =   300
   End
   Begin VB.ListBox List1 
      Height          =   4350
      Left            =   3960
      TabIndex        =   16
      Top             =   720
      Width           =   8835
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Add Prompt"
      Height          =   495
      Left            =   3960
      TabIndex        =   15
      Top             =   120
      Width           =   1215
   End
   Begin VB.Frame frmTags 
      Caption         =   "Tags"
      Height          =   1665
      Left            =   300
      TabIndex        =   9
      Top             =   3375
      Width           =   3390
      Begin VB.ListBox lstTags 
         Height          =   1320
         IntegralHeight  =   0   'False
         Left            =   75
         TabIndex        =   10
         Top             =   225
         Width           =   3240
      End
   End
   Begin VB.Timer tmrPos 
      Enabled         =   0   'False
      Interval        =   50
      Left            =   2775
      Top             =   825
   End
   Begin MSComctlLib.Slider sldPos 
      Height          =   280
      Left            =   300
      TabIndex        =   7
      Top             =   1950
      Width           =   3390
      _ExtentX        =   5980
      _ExtentY        =   503
      _Version        =   393216
      TickStyle       =   3
   End
   Begin MSComDlg.CommonDialog dlgOpen 
      Left            =   3210
      Top             =   825
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      Filter          =   "*.wav|*.wav"
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Enabled         =   0   'False
      Height          =   390
      Left            =   1425
      TabIndex        =   6
      Top             =   825
      Width           =   1065
   End
   Begin VB.ComboBox cboDevice 
      Height          =   315
      Left            =   900
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   180
      Width           =   2790
   End
   Begin VB.CommandButton cmdStop 
      Caption         =   "Stop"
      Enabled         =   0   'False
      Height          =   390
      Left            =   2625
      TabIndex        =   3
      Top             =   1350
      Width           =   1065
   End
   Begin VB.CommandButton cmdPause 
      Caption         =   "Pause"
      Enabled         =   0   'False
      Height          =   390
      Left            =   1425
      TabIndex        =   2
      Top             =   1350
      Width           =   1065
   End
   Begin VB.CommandButton cmdPlay 
      Caption         =   "Play"
      Enabled         =   0   'False
      Height          =   390
      Left            =   225
      TabIndex        =   1
      Top             =   1350
      Width           =   1065
   End
   Begin VB.CommandButton cmdOpen 
      Caption         =   "Open"
      Height          =   390
      Left            =   225
      TabIndex        =   0
      Top             =   825
      Width           =   1065
   End
   Begin MSComctlLib.Slider sldVolume 
      Height          =   285
      Left            =   1050
      TabIndex        =   12
      Top             =   2550
      Width           =   2565
      _ExtentX        =   4524
      _ExtentY        =   503
      _Version        =   393216
      Min             =   -10000
      Max             =   0
      TickStyle       =   3
   End
   Begin MSComctlLib.Slider sldBalance 
      Height          =   285
      Left            =   1050
      TabIndex        =   14
      Top             =   2925
      Width           =   2565
      _ExtentX        =   4524
      _ExtentY        =   503
      _Version        =   393216
      Min             =   -10000
      Max             =   10000
      TickStyle       =   3
   End
   Begin VB.Label lblBalance 
      AutoSize        =   -1  'True
      Caption         =   "Balance:"
      Height          =   195
      Left            =   315
      TabIndex        =   13
      Top             =   2925
      Width           =   615
   End
   Begin VB.Label lblVolume 
      AutoSize        =   -1  'True
      Caption         =   "Volume:"
      Height          =   195
      Left            =   375
      TabIndex        =   11
      Top             =   2550
      Width           =   570
   End
   Begin VB.Label lblPos 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "0:00/0:00"
      Height          =   195
      Left            =   2910
      TabIndex        =   8
      Top             =   2250
      Width           =   720
   End
   Begin VB.Label lblDevice 
      AutoSize        =   -1  'True
      Caption         =   "Device:"
      Height          =   195
      Left            =   225
      TabIndex        =   4
      Top             =   225
      Width           =   540
   End
End
Attribute VB_Name = "frmSoundDx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Buffer Length < 1000 can only be even hundreds
' (200, 400, 600, 800). I can't explain why,
' but for odd hundreds smaller streams can be
' played back wrong (missing or swapped buffers).
' The bigger the buffer, the better ;)
Private Const StreamBufferLength As Long = 2000

Private WithEvents m_clsStream   As DirectSoundStream
Attribute m_clsStream.VB_VarHelpID = -1

Private m_clsDirectSound         As DirectSound

Private m_clsWaveFile            As ISoundStream

Private m_blnDoNotMove           As Boolean

Public SpkBusy                   As Boolean

Private Sub ShowDevices()
     '<EhHeader>
     On Error GoTo ShowDevices_Err
     '</EhHeader>


        Dim i As Long
    
100     cboDevice.Clear
    
105     With m_clsDirectSound

110         For i = 1 To .DeviceCount
115             cboDevice.AddItem .DeviceDescription(i)
            Next

        End With
    
120     If cboDevice.ListCount > 0 Then
125         cboDevice.ListIndex = 0
        End If

     '<EhFooter>
     Exit Sub

ShowDevices_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmSoundDx.ShowDevices " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub cmdClose_Click()
     '<EhHeader>
     On Error GoTo cmdClose_Click_Err
     '</EhHeader>


100     If Not m_clsStream Is Nothing Then
105         m_clsStream.PlaybackStop
110         Set m_clsStream = Nothing
        End If
    
115     If Not m_clsWaveFile Is Nothing Then
120         m_clsWaveFile.StreamClose
        End If
    
125     If Not m_clsDirectSound Is Nothing Then
130         m_clsDirectSound.Deinitialize
        End If
    
135     cmdPlay.Enabled = False
140     cmdPause.Enabled = False
145     cmdStop.Enabled = False
150     cmdClose.Enabled = False
155     cmdOpen.Enabled = True
160     cboDevice.Enabled = True

     '<EhFooter>
     Exit Sub

cmdClose_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmSoundDx.cmdClose_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub cmdOpen_Click()
     '<EhHeader>
     On Error GoTo cmdOpen_Click_Err
     '</EhHeader>


        Dim clsStream As DirectSoundStream
        Dim i         As Long

        On Error GoTo ErrorHandler

        '        dlgOpen.Filter = "wav, mp3, wma|*.wav;*.mp3;*.wma"
        '        dlgOpen.FileName = vbNullString
        '        dlgOpen.ShowOpen
        '        If dlgOpen.FileName = vbNullString Then Exit Sub
        On Error GoTo cmdOpen_Click_Err

        '
100     If frmSoundDx.List1.ListCount > 0 Then
105         dlgOpen.filename = frmSoundDx.List1.List(0)
110         frmSoundDx.List1.RemoveItem 0
        End If

115     cmdClose_Click
    
120     Select Case LCase$(Right$(dlgOpen.filename, 3))

            Case "wav": Set m_clsWaveFile = New StreamWAV

125         Case Else:  Set m_clsWaveFile = New StreamWMA
        End Select

130     If Not m_clsWaveFile.StreamOpen(dlgOpen.filename) = SND_ERR_SUCCESS Then
            'MsgBox "Couldn't open the file!", vbExclamation

            Exit Sub

        End If

135     With m_clsWaveFile.StreamInfo
            ' create the DirectSound Primary Buffer with the output format
            ' of the Wave Stream so there is no unnecessary resampling
            ' which makes playback faster
140         If Not m_clsDirectSound.Initialize(cboDevice.ListIndex + 1, .SamplesPerSecond, .Channels, .BitsPerSample) Then
                'MsgBox "Couldn't initialize DirectSound!", vbExclamation
145             m_clsWaveFile.StreamClose

                Exit Sub

            End If

            ' create a DirectSound Secondary Buffer for playback
150         If Not m_clsDirectSound.CreateStream(.SamplesPerSecond, .Channels, .BitsPerSample, StreamBufferLength, clsStream) Then
                'MsgBox "Couldn't create DirectSound stream!", vbExclamation
            
155             m_clsWaveFile.StreamClose
160             m_clsDirectSound.Deinitialize
            
                Exit Sub

            Else
165             Set m_clsStream = clsStream
            
170             If m_clsWaveFile.StreamInfo.Duration = 0 Then
175                 sldPos.Max = 1
                Else
180                 sldPos.Max = m_clsWaveFile.StreamInfo.Duration
                End If
            
185             sldPos.Min = 0
190             sldPos.value = 0
            
195             lstTags.Clear

200             For i = 1 To m_clsWaveFile.StreamInfo.Tags.TagCount

205                 With m_clsWaveFile.StreamInfo.Tags.TagItem(i)
210                     lstTags.AddItem .TagName & ": " & .TagValue
                    End With

                Next
            
215             m_clsStream.Volume = sldVolume.value
220             m_clsStream.Balance = sldBalance.value
                        
225             cmdClose.Enabled = True
230             cmdOpen.Enabled = False
235             cboDevice.Enabled = False
240             cmdPlay.Enabled = True
245             cmdPause.Enabled = False
250             cmdStop.Enabled = False
            End If
        End With
    
ErrorHandler:

     '<EhFooter>
     Exit Sub

cmdOpen_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmSoundDx.cmdOpen_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub cmdPause_Click()
     '<EhHeader>
     On Error GoTo cmdPause_Click_Err
     '</EhHeader>


100     If Not m_clsStream.PlaybackPause() Then
            'MsgBox "Could not pause!", vbExclamation
        End If

     '<EhFooter>
     Exit Sub

cmdPause_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmSoundDx.cmdPause_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub cmdPlay_Click()
     '<EhHeader>
     On Error GoTo cmdPlay_Click_Err
     '</EhHeader>


        Dim i           As Long
        Dim intData()   As Integer
        Dim lngDataSize As Long
        Dim lngRead     As Long

        On Error Resume Next

100     If m_clsStream.PlaybackStatus = PlaybackStopped Then
            ' buffer 2 seconds of audio data
105         lngDataSize = m_clsStream.BytesFromMs(200)
110         ReDim intData(lngDataSize \ 2 - 1) As Integer
        
115         For i = 1 To 10
120             m_clsWaveFile.StreamRead VarPtr(intData(0)), lngDataSize, lngRead

125             DoEvents
            
130             If lngRead > 0 Then
135                 m_clsStream.AudioBufferAdd VarPtr(intData(0)), lngRead
                Else

                    Exit For

                End If
            Next

        End If
    
140     If Not m_clsStream.PlaybackStart() Then
            'MsgBox "Could not start playback!", vbExclamation
        End If

     '<EhFooter>
     Exit Sub

cmdPlay_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmSoundDx.cmdPlay_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub cmdStop_Click()
     '<EhHeader>
     On Error GoTo cmdStop_Click_Err
     '</EhHeader>


100     If Not m_clsStream.PlaybackStop() Then
            'MsgBox "Could not stop playback!", vbExclamation
        End If
    
105     m_clsWaveFile.StreamSeek 0, SND_SEEK_PERCENT

     '<EhFooter>
     Exit Sub

cmdStop_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmSoundDx.cmdStop_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub Command1_Click()
     '<EhHeader>
     On Error GoTo Command1_Click_Err
     '</EhHeader>


        Dim Path1  As String
        Dim Sound1 As String
    
100     Path1 = App.path & "\Prompt\PromptUSA\"
105     Sound1 = "PromptUSA_"
    
110     List1.Clear
115     List1.AddItem Path1 & Sound1 & "Number.wav"    ' หมายเลข
120     List1.AddItem Path1 & Sound1 & "1.wav"    ' หมายเลข
125     List1.AddItem Path1 & Sound1 & "0.wav"    ' หมายเลข
130     List1.AddItem Path1 & Sound1 & "2.wav"    ' หมายเลข
135     List1.AddItem Path1 & Sound1 & "Service.wav"    ' หมายเลข
140     List1.AddItem Path1 & Sound1 & "0.wav"    ' หมายเลข
145     List1.AddItem Path1 & Sound1 & "1.wav"    ' หมายเลข
        'List1.AddItem Path1 & Sound1 & "Sir.wav"    ' หมายเลข
    
150     frmSoundDx.Timer1.Enabled = True
    
     '<EhFooter>
     Exit Sub

Command1_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmSoundDx.Command1_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub SpeakDX(pFileName As String)
     '<EhHeader>
     On Error GoTo SpeakDX_Err
     '</EhHeader>


100     frmSoundDx.List1.AddItem pFileName
105     frmSoundDx.Timer1.Enabled = True

     '<EhFooter>
     Exit Sub

SpeakDX_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmSoundDx.SpeakDX " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub Form_Load()
     '<EhHeader>
     On Error GoTo Form_Load_Err
     '</EhHeader>


100     Set m_clsDirectSound = New DirectSound
    
105     SpkBusy = False
    
110     ProcessPrioritySet Priority:=ppHigh
    
115     ShowDevices

     '<EhFooter>
     Exit Sub

Form_Load_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmSoundDx.Form_Load " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub Form_Unload(Cancel As Integer)
     '<EhHeader>
     On Error GoTo Form_Unload_Err
     '</EhHeader>


100     If Not m_clsStream Is Nothing Then
105         m_clsStream.PlaybackStop
110         Set m_clsStream = Nothing
        End If
    
115     If Not m_clsWaveFile Is Nothing Then
120         m_clsWaveFile.StreamClose
        End If
    
125     If Not m_clsDirectSound Is Nothing Then
130         m_clsDirectSound.Deinitialize
135         Set m_clsDirectSound = Nothing
        End If

     '<EhFooter>
     Exit Sub

Form_Unload_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmSoundDx.Form_Unload " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub m_clsStream_BufferDone()
     '<EhHeader>
     On Error GoTo m_clsStream_BufferDone_Err
     '</EhHeader>


        Dim intData()   As Integer
        Dim lngDataSize As Long
        Dim lngRead     As Long
    
100     If Not m_clsWaveFile.EndOfStream Then
105         lngDataSize = m_clsStream.BytesFromMs(200)
110         ReDim intData(lngDataSize \ 2 - 1) As Integer
    
115         m_clsWaveFile.StreamRead VarPtr(intData(0)), lngDataSize, lngRead
        
120         If lngRead > 0 Then
125             m_clsStream.AudioBufferAdd VarPtr(intData(0)), lngRead
            End If
        End If

     '<EhFooter>
     Exit Sub

m_clsStream_BufferDone_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmSoundDx.m_clsStream_BufferDone " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub m_clsStream_NoDataLeft()
     '<EhHeader>
     On Error GoTo m_clsStream_NoDataLeft_Err
     '</EhHeader>


        Dim intData()   As Integer
        Dim lngDataSize As Long
        Dim lngRead     As Long
        Dim i           As Long
    
100     If m_clsWaveFile.EndOfStream Then
        
            '        Debug.Print "End Of Stream!"
105         SpkBusy = False
        
110         m_clsStream.PlaybackStop
115         m_clsWaveFile.StreamSeek 0, SND_SEEK_PERCENT
        Else
            ' buffer underrun, buffer 2 seconds of audio data
120         lngDataSize = m_clsStream.BytesFromMs(200)
125         ReDim intData(lngDataSize \ 2 - 1) As Integer
        
130         For i = 1 To 10
135             m_clsWaveFile.StreamRead VarPtr(intData(0)), lngDataSize, lngRead

140             DoEvents
            
145             If lngRead > 0 Then
150                 m_clsStream.AudioBufferAdd VarPtr(intData(0)), lngRead
                Else

                    Exit For

                End If
            Next

        End If

     '<EhFooter>
     Exit Sub

m_clsStream_NoDataLeft_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmSoundDx.m_clsStream_NoDataLeft " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub m_clsStream_StatusChanged(ByVal status As PlaybackStatus)
     '<EhHeader>
     On Error GoTo m_clsStream_StatusChanged_Err
     '</EhHeader>

100     Select Case True

            Case status = PlaybackPlaying
105             cmdPlay.Enabled = False
110             cmdPause.Enabled = True
115             cmdStop.Enabled = True
120             tmrPos.Enabled = True

125         Case status = PlaybackPausing
130             cmdPlay.Enabled = True
135             cmdPause.Enabled = False
140             cmdStop.Enabled = True
145             tmrPos.Enabled = False

150         Case status = PlaybackStopped
155             cmdPlay.Enabled = True
160             cmdPause.Enabled = False
165             cmdStop.Enabled = False
170             tmrPos.Enabled = False
        End Select

     '<EhFooter>
     Exit Sub

m_clsStream_StatusChanged_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmSoundDx.m_clsStream_StatusChanged " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub sldBalance_MouseDown(Button As Integer, _
                                 Shift As Integer, _
                                 X As Single, _
                                 Y As Single)
     '<EhHeader>
     On Error GoTo sldBalance_MouseDown_Err
     '</EhHeader>


100     If Button = vbRightButton Then
105         sldBalance.value = 0
        
110         If Not m_clsStream Is Nothing Then
115             m_clsStream.Balance = 0
            End If
        End If

     '<EhFooter>
     Exit Sub

sldBalance_MouseDown_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmSoundDx.sldBalance_MouseDown " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub sldBalance_Scroll()
     '<EhHeader>
     On Error GoTo sldBalance_Scroll_Err
     '</EhHeader>


100     If Not m_clsStream Is Nothing Then
105         m_clsStream.Balance = sldBalance.value
        End If

     '<EhFooter>
     Exit Sub

sldBalance_Scroll_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmSoundDx.sldBalance_Scroll " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub sldPos_MouseDown(Button As Integer, _
                             Shift As Integer, _
                             X As Single, _
                             Y As Single)
     '<EhHeader>
     On Error GoTo sldPos_MouseDown_Err
     '</EhHeader>


100     m_blnDoNotMove = True

     '<EhFooter>
     Exit Sub

sldPos_MouseDown_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmSoundDx.sldPos_MouseDown " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub sldPos_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
     '<EhHeader>
     On Error GoTo sldPos_MouseUp_Err
     '</EhHeader>


        Dim lngStreamPosition As Long

100     m_blnDoNotMove = False
    
105     m_clsWaveFile.StreamSeek sldPos.value \ 1000, SND_SEEK_SECONDS
110     lngStreamPosition = m_clsWaveFile.StreamInfo.Position
    
115     m_clsStream.PlaybackStop
120     cmdPlay_Click
125     m_clsStream.Elapsed = lngStreamPosition

     '<EhFooter>
     Exit Sub

sldPos_MouseUp_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmSoundDx.sldPos_MouseUp " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub sldVolume_Scroll()
     '<EhHeader>
     On Error GoTo sldVolume_Scroll_Err
     '</EhHeader>


100     If Not m_clsStream Is Nothing Then
105         m_clsStream.Volume = sldVolume.value
        End If

     '<EhFooter>
     Exit Sub

sldVolume_Scroll_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmSoundDx.sldVolume_Scroll " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub Timer1_Timer()
    '<EhHeader>
    On Error Resume Next
    '</EhHeader>

    DoEvents: Sleep 1
    If SpkBusy = True Then Exit Sub
    
    If List1.ListCount > 0 Then
        cmdOpen_Click
        cmdPlay_Click
        SpkBusy = True
        '        frmMain.cmdAnn1.Enabled = False
        '        frmMain.cmdAnn2.Enabled = False
        '        frmMain.cmdAnn3.Enabled = False
        '        frmMain.cmdAnn4.Enabled = False
    Else
        Timer1.Enabled = False
        SpkBufBusy = False ' ว่าง
        '        frmMain.cmdAnn1.Enabled = True
        '        frmMain.cmdAnn2.Enabled = True
        '        frmMain.cmdAnn3.Enabled = True
        '        frmMain.cmdAnn4.Enabled = True
    End If
End Sub

Private Sub tmrPos_Timer()
    '<EhHeader>
    On Error Resume Next
    '</EhHeader>


    lblPos.Caption = MsToString(m_clsStream.Elapsed) & "/" & MsToString(m_clsWaveFile.StreamInfo.Duration)

    If Not m_blnDoNotMove Then
        sldPos.value = m_clsStream.Elapsed
        '        Debug.Print Now
    End If
End Sub

Private Function MsToString(ByVal ms As Long) As String
     '<EhHeader>
     On Error GoTo MsToString_Err
     '</EhHeader>


        Dim secs As Long
        Dim mins As Long
    
100     secs = ms \ 1000
105     mins = secs \ 60
110     secs = secs Mod 60
    
115     MsToString = mins & ":" & Format(secs, "00")

     '<EhFooter>
     Exit Function

MsToString_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmSoundDx.MsToString " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function
