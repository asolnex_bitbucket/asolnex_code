VERSION 5.00
Begin VB.Form frmKeyPad 
   Caption         =   "KeyPad"
   ClientHeight    =   8205
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   8070
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   222
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmKeyPad.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   8205
   ScaleWidth      =   8070
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtQID 
      Height          =   360
      Index           =   15
      Left            =   7905
      TabIndex        =   133
      Top             =   7635
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQID 
      Height          =   360
      Index           =   14
      Left            =   7905
      TabIndex        =   132
      Top             =   7155
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQID 
      Height          =   360
      Index           =   13
      Left            =   7905
      TabIndex        =   131
      Top             =   6675
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQID 
      Height          =   360
      Index           =   12
      Left            =   7905
      TabIndex        =   130
      Top             =   6195
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQID 
      Height          =   360
      Index           =   11
      Left            =   7905
      TabIndex        =   129
      Top             =   5715
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQID 
      Height          =   360
      Index           =   10
      Left            =   7905
      TabIndex        =   128
      Top             =   5235
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQID 
      Height          =   360
      Index           =   9
      Left            =   7905
      TabIndex        =   127
      Top             =   4755
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQID 
      Height          =   360
      Index           =   8
      Left            =   7905
      TabIndex        =   126
      Top             =   4275
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQID 
      Height          =   360
      Index           =   7
      Left            =   7905
      TabIndex        =   125
      Top             =   3795
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQID 
      Height          =   360
      Index           =   6
      Left            =   7905
      TabIndex        =   124
      Top             =   3315
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQID 
      Height          =   360
      Index           =   5
      Left            =   7905
      TabIndex        =   123
      Top             =   2835
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQID 
      Height          =   360
      Index           =   4
      Left            =   7905
      TabIndex        =   122
      Top             =   2355
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQID 
      Height          =   360
      Index           =   3
      Left            =   7905
      TabIndex        =   121
      Top             =   1875
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQID 
      Height          =   360
      Index           =   2
      Left            =   7905
      TabIndex        =   120
      Top             =   1395
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQID 
      Height          =   360
      Index           =   1
      Left            =   7905
      TabIndex        =   119
      Top             =   915
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQID 
      Height          =   360
      Index           =   0
      Left            =   7905
      TabIndex        =   118
      Top             =   435
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtStatus 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   15
      Left            =   3150
      Locked          =   -1  'True
      TabIndex        =   112
      Text            =   "CALL"
      Top             =   7635
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox txtStatus 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   14
      Left            =   3165
      Locked          =   -1  'True
      TabIndex        =   111
      Text            =   "CALL"
      Top             =   7155
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox txtStatus 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   13
      Left            =   3165
      Locked          =   -1  'True
      TabIndex        =   110
      Text            =   "CALL"
      Top             =   6675
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox txtStatus 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   12
      Left            =   3165
      Locked          =   -1  'True
      TabIndex        =   109
      Text            =   "CALL"
      Top             =   6195
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox txtStatus 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   11
      Left            =   3165
      Locked          =   -1  'True
      TabIndex        =   108
      Text            =   "CALL"
      Top             =   5715
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox txtStatus 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   10
      Left            =   3165
      Locked          =   -1  'True
      TabIndex        =   107
      Text            =   "CALL"
      Top             =   5235
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox txtStatus 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   9
      Left            =   3165
      Locked          =   -1  'True
      TabIndex        =   106
      Text            =   "CALL"
      Top             =   4755
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox txtStatus 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   8
      Left            =   3165
      Locked          =   -1  'True
      TabIndex        =   105
      Text            =   "CALL"
      Top             =   4275
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox txtStatus 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   7
      Left            =   3165
      Locked          =   -1  'True
      TabIndex        =   104
      Text            =   "CALL"
      Top             =   3795
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox txtStatus 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   6
      Left            =   3165
      Locked          =   -1  'True
      TabIndex        =   103
      Text            =   "CALL"
      Top             =   3315
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox txtStatus 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   5
      Left            =   3165
      Locked          =   -1  'True
      TabIndex        =   102
      Text            =   "CALL"
      Top             =   2835
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox txtStatus 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   4
      Left            =   3165
      Locked          =   -1  'True
      TabIndex        =   101
      Text            =   "CALL"
      Top             =   2355
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox txtStatus 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   3
      Left            =   3165
      Locked          =   -1  'True
      TabIndex        =   100
      Text            =   "CALL"
      Top             =   1875
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox txtStatus 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   2
      Left            =   3165
      Locked          =   -1  'True
      TabIndex        =   99
      Text            =   "CALL"
      Top             =   1395
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox txtStatus 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   1
      Left            =   3165
      Locked          =   -1  'True
      TabIndex        =   98
      Text            =   "Loading(8888)"
      Top             =   915
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox txtStatus 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   0
      Left            =   3165
      Locked          =   -1  'True
      TabIndex        =   97
      Text            =   "Call"
      Top             =   435
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.TextBox txtQNum 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   15
      Left            =   2640
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   96
      Top             =   7635
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQNum 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   14
      Left            =   2640
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   95
      Top             =   7155
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQNum 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   13
      Left            =   2640
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   94
      Top             =   6675
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQNum 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   12
      Left            =   2640
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   93
      Top             =   6195
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQNum 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   11
      Left            =   2640
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   92
      Top             =   5715
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQNum 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   10
      Left            =   2640
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   91
      Top             =   5235
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQNum 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   9
      Left            =   2640
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   90
      Top             =   4755
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQNum 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   8
      Left            =   2640
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   89
      Top             =   4275
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQNum 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   7
      Left            =   2640
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   88
      Top             =   3795
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQNum 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   6
      Left            =   2640
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   87
      Top             =   3315
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQNum 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   5
      Left            =   2640
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   86
      Top             =   2835
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQNum 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   4
      Left            =   2640
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   85
      Top             =   2355
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQNum 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   3
      Left            =   2640
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   84
      Top             =   1875
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQNum 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   2
      Left            =   2640
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   83
      Top             =   1395
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQNum 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   1
      Left            =   2640
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   82
      Top             =   915
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.TextBox txtQNum 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   0
      Left            =   2640
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   81
      Text            =   "888"
      Top             =   435
      Visible         =   0   'False
      Width           =   500
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "Finish"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   15
      Left            =   6885
      TabIndex        =   64
      Top             =   7635
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "Finish"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   14
      Left            =   6885
      TabIndex        =   63
      Top             =   7155
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "Finish"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   13
      Left            =   6885
      TabIndex        =   62
      Top             =   6675
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "Finish"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   12
      Left            =   6885
      TabIndex        =   61
      Top             =   6195
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "Finish"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   11
      Left            =   6885
      TabIndex        =   60
      Top             =   5715
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "Finish"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   10
      Left            =   6885
      TabIndex        =   59
      Top             =   5235
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "Finish"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   9
      Left            =   6885
      TabIndex        =   58
      Top             =   4755
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "Finish"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   8
      Left            =   6885
      TabIndex        =   57
      Top             =   4275
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "Finish"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   7
      Left            =   6885
      TabIndex        =   56
      Top             =   3795
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "Finish"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   6
      Left            =   6885
      TabIndex        =   55
      Top             =   3315
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "Finish"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   5
      Left            =   6885
      TabIndex        =   54
      Top             =   2835
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "Finish"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   4
      Left            =   6885
      TabIndex        =   53
      Top             =   2355
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "Finish"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   3
      Left            =   6885
      TabIndex        =   52
      Top             =   1875
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "Finish"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   2
      Left            =   6885
      TabIndex        =   51
      Top             =   1395
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "Finish"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   1
      Left            =   6885
      TabIndex        =   50
      Top             =   915
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "Finish"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   0
      Left            =   6885
      TabIndex        =   49
      Top             =   435
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "Start"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   15
      Left            =   5805
      TabIndex        =   48
      Top             =   7635
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "Start"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   14
      Left            =   5805
      TabIndex        =   47
      Top             =   7155
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "Start"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   13
      Left            =   5805
      TabIndex        =   46
      Top             =   6675
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "Start"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   12
      Left            =   5805
      TabIndex        =   45
      Top             =   6195
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "Start"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   11
      Left            =   5805
      TabIndex        =   44
      Top             =   5715
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "Start"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   10
      Left            =   5805
      TabIndex        =   43
      Top             =   5235
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "Start"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   9
      Left            =   5805
      TabIndex        =   42
      Top             =   4755
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "Start"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   8
      Left            =   5805
      TabIndex        =   41
      Top             =   4275
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "Start"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   7
      Left            =   5805
      TabIndex        =   40
      Top             =   3795
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "Start"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   6
      Left            =   5805
      TabIndex        =   39
      Top             =   3315
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "Start"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   5
      Left            =   5805
      TabIndex        =   38
      Top             =   2835
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "Start"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   4
      Left            =   5805
      TabIndex        =   37
      Top             =   2355
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "Start"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   3
      Left            =   5805
      TabIndex        =   36
      Top             =   1875
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "Start"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   2
      Left            =   5805
      TabIndex        =   35
      Top             =   1395
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "Start"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   1
      Left            =   5805
      TabIndex        =   34
      Top             =   915
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "Start"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   0
      Left            =   5805
      TabIndex        =   33
      Top             =   435
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdCall 
      Caption         =   "Call"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   15
      Left            =   4695
      TabIndex        =   32
      Top             =   7635
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdCall 
      Caption         =   "Call"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   14
      Left            =   4695
      TabIndex        =   31
      Top             =   7155
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdCall 
      Caption         =   "Call"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   13
      Left            =   4695
      TabIndex        =   30
      Top             =   6675
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdCall 
      Caption         =   "Call"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   12
      Left            =   4695
      TabIndex        =   29
      Top             =   6195
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdCall 
      Caption         =   "Call"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   11
      Left            =   4695
      TabIndex        =   28
      Top             =   5715
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdCall 
      Caption         =   "Call"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   10
      Left            =   4695
      TabIndex        =   27
      Top             =   5235
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdCall 
      Caption         =   "Call"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   9
      Left            =   4695
      TabIndex        =   26
      Top             =   4755
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdCall 
      Caption         =   "Call"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   8
      Left            =   4695
      TabIndex        =   25
      Top             =   4275
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdCall 
      Caption         =   "Call"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   7
      Left            =   4695
      TabIndex        =   24
      Top             =   3795
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdCall 
      Caption         =   "Call"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   6
      Left            =   4695
      TabIndex        =   23
      Top             =   3315
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdCall 
      Caption         =   "Call"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   5
      Left            =   4695
      TabIndex        =   22
      Top             =   2835
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdCall 
      Caption         =   "Call"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   4
      Left            =   4695
      TabIndex        =   21
      Top             =   2355
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdCall 
      Caption         =   "Call"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   3
      Left            =   4695
      TabIndex        =   20
      Top             =   1875
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdCall 
      Caption         =   "Call"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   2
      Left            =   4695
      TabIndex        =   19
      Top             =   1395
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdCall 
      Caption         =   "Call"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   1
      Left            =   4695
      TabIndex        =   18
      Top             =   915
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CommandButton cmdCall 
      Caption         =   "Call"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   0
      Left            =   4695
      TabIndex        =   17
      Top             =   435
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.CheckBox chkOnTop 
      Caption         =   "On Top"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   180
      Style           =   1  'Graphical
      TabIndex        =   16
      Top             =   8295
      Width           =   7680
   End
   Begin VB.ComboBox cmbQType 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   15
      Left            =   1005
      Style           =   2  'Dropdown List
      TabIndex        =   15
      Top             =   7635
      Visible         =   0   'False
      Width           =   1560
   End
   Begin VB.ComboBox cmbQType 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   14
      Left            =   1005
      Style           =   2  'Dropdown List
      TabIndex        =   14
      Top             =   7155
      Visible         =   0   'False
      Width           =   1560
   End
   Begin VB.ComboBox cmbQType 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   13
      Left            =   1005
      Style           =   2  'Dropdown List
      TabIndex        =   13
      Top             =   6675
      Visible         =   0   'False
      Width           =   1560
   End
   Begin VB.ComboBox cmbQType 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   12
      Left            =   1005
      Style           =   2  'Dropdown List
      TabIndex        =   12
      Top             =   6195
      Visible         =   0   'False
      Width           =   1560
   End
   Begin VB.ComboBox cmbQType 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   11
      Left            =   1005
      Style           =   2  'Dropdown List
      TabIndex        =   11
      Top             =   5715
      Visible         =   0   'False
      Width           =   1560
   End
   Begin VB.ComboBox cmbQType 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   10
      Left            =   1005
      Style           =   2  'Dropdown List
      TabIndex        =   10
      Top             =   5235
      Visible         =   0   'False
      Width           =   1560
   End
   Begin VB.ComboBox cmbQType 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   9
      Left            =   1005
      Style           =   2  'Dropdown List
      TabIndex        =   9
      Top             =   4755
      Visible         =   0   'False
      Width           =   1560
   End
   Begin VB.ComboBox cmbQType 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   8
      Left            =   1005
      Style           =   2  'Dropdown List
      TabIndex        =   8
      Top             =   4275
      Visible         =   0   'False
      Width           =   1560
   End
   Begin VB.ComboBox cmbQType 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   7
      Left            =   1005
      Style           =   2  'Dropdown List
      TabIndex        =   7
      Top             =   3795
      Visible         =   0   'False
      Width           =   1560
   End
   Begin VB.ComboBox cmbQType 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   6
      Left            =   1005
      Style           =   2  'Dropdown List
      TabIndex        =   6
      Top             =   3315
      Visible         =   0   'False
      Width           =   1560
   End
   Begin VB.ComboBox cmbQType 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   5
      Left            =   1005
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   2835
      Visible         =   0   'False
      Width           =   1560
   End
   Begin VB.ComboBox cmbQType 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   4
      Left            =   1005
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   2355
      Visible         =   0   'False
      Width           =   1560
   End
   Begin VB.ComboBox cmbQType 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   3
      Left            =   1005
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   1875
      Visible         =   0   'False
      Width           =   1560
   End
   Begin VB.ComboBox cmbQType 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   2
      Left            =   1005
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   1395
      Visible         =   0   'False
      Width           =   1560
   End
   Begin VB.ComboBox cmbQType 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   1
      Left            =   1005
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   915
      Visible         =   0   'False
      Width           =   1560
   End
   Begin VB.ComboBox cmbQType 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   0
      Left            =   1005
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   435
      Visible         =   0   'False
      Width           =   1560
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Action"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   4
      Left            =   5805
      TabIndex        =   117
      Top             =   120
      Width           =   990
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Index           =   3
      Left            =   3165
      TabIndex        =   116
      Top             =   135
      Width           =   1455
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "QNo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Index           =   2
      Left            =   2640
      TabIndex        =   115
      Top             =   120
      Width           =   495
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "QType"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Index           =   1
      Left            =   1005
      TabIndex        =   114
      Top             =   120
      Width           =   1560
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Lane"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   0
      Left            =   270
      TabIndex        =   113
      Top             =   120
      Width           =   600
   End
   Begin VB.Label lblLane 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Lane "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   15
      Left            =   270
      TabIndex        =   80
      Top             =   7695
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.Label lblLane 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Lane "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   14
      Left            =   270
      TabIndex        =   79
      Top             =   7215
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.Label lblLane 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Lane "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   13
      Left            =   270
      TabIndex        =   78
      Top             =   6735
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.Label lblLane 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Lane "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   12
      Left            =   270
      TabIndex        =   77
      Top             =   6255
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.Label lblLane 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Lane "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   11
      Left            =   270
      TabIndex        =   76
      Top             =   5775
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.Label lblLane 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Lane "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   10
      Left            =   270
      TabIndex        =   75
      Top             =   5295
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.Label lblLane 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Lane "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   9
      Left            =   270
      TabIndex        =   74
      Top             =   4815
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.Label lblLane 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Lane "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   8
      Left            =   270
      TabIndex        =   73
      Top             =   4335
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.Label lblLane 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Lane "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   7
      Left            =   270
      TabIndex        =   72
      Top             =   3855
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.Label lblLane 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Lane "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   6
      Left            =   270
      TabIndex        =   71
      Top             =   3375
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.Label lblLane 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Lane "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   5
      Left            =   270
      TabIndex        =   70
      Top             =   2895
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.Label lblLane 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Lane "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   4
      Left            =   270
      TabIndex        =   69
      Top             =   2415
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.Label lblLane 
      BackStyle       =   0  'Transparent
      Caption         =   "Lane "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   3
      Left            =   270
      TabIndex        =   68
      Top             =   1935
      Visible         =   0   'False
      Width           =   600
   End
   Begin VB.Label lblLane 
      BackStyle       =   0  'Transparent
      Caption         =   "Lane "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   2
      Left            =   270
      TabIndex        =   67
      Top             =   1455
      Visible         =   0   'False
      Width           =   600
   End
   Begin VB.Label lblLane 
      BackStyle       =   0  'Transparent
      Caption         =   "Lane "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   270
      TabIndex        =   66
      Top             =   975
      Visible         =   0   'False
      Width           =   600
   End
   Begin VB.Label lblLane 
      BackStyle       =   0  'Transparent
      Caption         =   "Lane 1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   0
      Left            =   270
      TabIndex        =   65
      Top             =   495
      Visible         =   0   'False
      Width           =   600
   End
End
Attribute VB_Name = "frmKeyPad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub chkOnTop_Click()
     '<EhHeader>
     On Error GoTo chkOnTop_Click_Err
     '</EhHeader>


100     If chkOnTop.Value Then
105         Call m_Ontop.FormOnTop(Me)
        End If

     '<EhFooter>
     Exit Sub

chkOnTop_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmKeyPad.chkOnTop_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub cmdCall_Click(index As Integer)
     '<EhHeader>
     On Error GoTo cmdCall_Click_Err
     '</EhHeader>


100     If Len(txtQID(index).Text) > 0 Then
105         Call m_Q.QReCall(Val(txtQID(index).Text))
        Else
110         If Len(txtQNum(index).Text) = 3 Then
115             Call m_Q.DirectQ(index + 1, txtQNum(index).Text) 'Call Direct Q
            Else
120             Call m_Q.QCall(cmbQType(index).ListIndex + 1, index + 1) 'SEND QType,Lane
            End If
        End If

     '<EhFooter>
     Exit Sub

cmdCall_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmKeyPad.cmdCall_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub cmdFinish_Click(index As Integer)
     '<EhHeader>
     On Error GoTo cmdFinish_Click_Err
     '</EhHeader>


        Dim QID As Double, Lane%, i%
100     QID = Val(txtQID(index).Text)
105     Call m_Q.QFinish(QID)
110     Call m_Q.RemoveQ2Lane(QID) ' ��Ҥ���͡�ҡ �Ź

115     For i = 0 To frmMain.lstDSP.ListCount
120         If InStr(1, frmMain.lstDSP.List(i), txtQNum(index).Text) > 0 Then
125             frmMain.lstDSP.RemoveItem (i) '��Ҥ�Ƿ�������͡

                Exit For

            End If
130     Next i
        
135     m_LED.DSP_BingOn = False
140     Call m_Q.curCall ' �Ѵ Display ����
145     g_ForceRead = True

     '<EhFooter>
     Exit Sub

cmdFinish_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmKeyPad.cmdFinish_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub cmdStart_Click(index As Integer)
     '<EhHeader>
     On Error GoTo cmdStart_Click_Err
     '</EhHeader>


100     Call m_Q.QStartLoad(Val(txtQID(index).Text))
105     g_ForceRead = True

     '<EhFooter>
     Exit Sub

cmdStart_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmKeyPad.cmdStart_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub Form_Load()
     '<EhHeader>
     On Error GoTo Form_Load_Err
     '</EhHeader>

100     Call m_Ontop.FormOnTop(Me)
105     Call initialKeyPad
110     Call m_Q.CurQonLane

     '<EhFooter>
     Exit Sub

Form_Load_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmKeyPad.Form_Load " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub initialKeyPad()
     '<EhHeader>
     On Error GoTo initialKeyPad_Err
     '</EhHeader>


        Dim i%, j%

100     For j = 0 To 15
105         For i = 1 To UBound(m_Q.QTypeName)
110             cmbQType(j).AddItem m_Q.QTypeName(i)
115         Next i
120     Next j

125     For i = 1 To UBound(m_Q.QLaneType)
130         lblLane(i - 1).Caption = "Lane " & i
135         cmbQType(i - 1).ListIndex = m_Q.QLaneType(i) - 1
140     Next i
    
145     For i = 0 To m_global.maxLane - 1
150         lblLane(i).Visible = True
155         cmbQType(i).Visible = True
160         txtQNum(i).Visible = True
165         txtStatus(i).Visible = True
170         cmdCall(i).Visible = True
175         cmdStart(i).Visible = True
180         cmdFinish(i).Visible = True
185     Next i
    
        'If m_global.maxLane = 16 Then
190         cmbQType(b1 - 1).Enabled = False
195         cmbQType(b2 - 1).Enabled = False
        'End If

     '<EhFooter>
     Exit Sub

initialKeyPad_Err:

    Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmKeyPad.initialKeyPad " & _
               "at line " & Erl, EVENTLOG_ERROR_TYPE, Err.Number)
     Resume Next
     '</EhFooter>
End Sub

Private Sub Lane_Click()

End Sub

