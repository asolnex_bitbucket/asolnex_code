VERSION 5.00
Begin VB.Form frmTicket 
   Caption         =   "Ticket Printer"
   ClientHeight    =   6345
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7695
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   222
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmTicket.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   6345
   ScaleWidth      =   7695
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtCID 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "TH SarabunPSK"
         Size            =   15.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2445
      TabIndex        =   15
      Top             =   1095
      Visible         =   0   'False
      Width           =   600
   End
   Begin VB.ListBox lstCID 
      Height          =   5715
      Left            =   1860
      TabIndex        =   14
      Top             =   960
      Visible         =   0   'False
      Width           =   420
   End
   Begin VB.CheckBox chkManual 
      Caption         =   "��͡����ö�ͧ"
      Height          =   495
      Left            =   1380
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   465
      Width           =   1560
   End
   Begin VB.ListBox lstCarLicense 
      Height          =   5715
      Left            =   1350
      TabIndex        =   6
      Top             =   990
      Visible         =   0   'False
      Width           =   420
   End
   Begin VB.ListBox lstCarNo 
      Height          =   5715
      Left            =   75
      TabIndex        =   8
      Top             =   465
      Width           =   1185
   End
   Begin VB.TextBox txtCarLicense 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "TH SarabunPSK"
         Size            =   15.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4710
      TabIndex        =   5
      Top             =   465
      Width           =   1605
   End
   Begin VB.CheckBox chkOnTop 
      Caption         =   "On Top"
      Height          =   765
      Left            =   3075
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   5445
      Visible         =   0   'False
      Width           =   4500
   End
   Begin VB.CheckBox chkBottle 
      Caption         =   "�բǴ"
      Height          =   495
      Left            =   6345
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   465
      Width           =   1215
   End
   Begin VB.TextBox txtCarNo 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "TH SarabunPSK"
         Size            =   15.75
         Charset         =   222
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3075
      MaxLength       =   5
      TabIndex        =   4
      Top             =   465
      Width           =   1605
   End
   Begin VB.CommandButton cmdType 
      Caption         =   "cmdType"
      Height          =   765
      Index           =   3
      Left            =   3075
      TabIndex        =   3
      Top             =   3885
      Width           =   4500
   End
   Begin VB.CommandButton cmdType 
      Caption         =   "cmdType"
      Height          =   765
      Index           =   2
      Left            =   3075
      TabIndex        =   2
      Top             =   3015
      Width           =   4500
   End
   Begin VB.CommandButton cmdType 
      Caption         =   "cmdType"
      Height          =   765
      Index           =   1
      Left            =   3075
      TabIndex        =   1
      Top             =   2145
      Width           =   4500
   End
   Begin VB.CommandButton cmdType 
      Caption         =   "cmdType"
      Height          =   765
      Index           =   0
      Left            =   3075
      TabIndex        =   0
      Top             =   1275
      Width           =   4500
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "����¹ö"
      Height          =   195
      Left            =   4710
      TabIndex        =   13
      Top             =   135
      Width           =   1605
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "����ö"
      Height          =   195
      Left            =   3075
      TabIndex        =   12
      Top             =   135
      Width           =   1605
   End
   Begin VB.Label lblCarNo 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "����ö"
      Height          =   195
      Left            =   75
      TabIndex        =   11
      Top             =   165
      Width           =   1185
   End
End
Attribute VB_Name = "frmTicket"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub chkBottle_Click()
     '<EhHeader>
     On Error GoTo chkBottle_Click_Err
     '</EhHeader>


        Dim i%, chk As Boolean
100     chk = chkBottle.Value

105     For i = 2 To UBound(m_Q.QTypeName)
110         cmdType(i - 1).Enabled = Not chk
        Next

     '<EhFooter>
     Exit Sub

chkBottle_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmTicket.chkBottle_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub chkManual_Click()
     '<EhHeader>
     On Error GoTo chkManual_Click_Err
     '</EhHeader>

        Dim i%
100     If chkManual.Value Then
105         txtCarNo.Enabled = True
110         txtCarLicense.Enabled = True
115         lstCarNo.Enabled = False
120         txtCarNo.Text = ""
125         txtCarLicense.Text = ""
130         txtCID.Text = "0"
135         txtCarNo.SetFocus
        Else
140         txtCarNo.Enabled = False
145         txtCarLicense.Enabled = False
150         lstCarNo.Enabled = True
155         txtCarNo.Text = ""
160         txtCarLicense.Text = ""
165         txtCID.Text = "0"
170         lstCarNo.SetFocus

175         For i = 0 To lstCarNo.ListCount - 1
180             lstCarNo.Selected(i) = False
185         Next i

        End If

     '<EhFooter>
     Exit Sub

chkManual_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmTicket.chkManual_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub chkOnTop_Click()
     '<EhHeader>
     On Error GoTo chkOnTop_Click_Err
     '</EhHeader>


100     If chkOnTop.Value Then
105         Call m_Ontop.FormOnTop(Me)
        End If

     '<EhFooter>
     Exit Sub

chkOnTop_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmTicket.chkOnTop_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub cmdType_Click(index As Integer)
     '<EhHeader>
     On Error GoTo cmdType_Click_Err
     '</EhHeader>

100     If Len(txtCarNo.Text) = 0 Then
105         MsgBox "��س����͡ ���� ��͡������ ����ö !!!", vbCritical

            Exit Sub

        End If
110     Call m_Q.getQ(index + 1, chkBottle.Value, txtCarNo.Text, txtCarLicense.Text, Val(txtCID.Text))
115     Call m_Q.setCarNo
120     Call frmMain.ReadData   'g_ForceRead = True '�����ҹ�������������
125     txtCarNo.Text = ""
130     txtCarLicense.Text = ""
135     txtCID.Text = "0"
     '<EhFooter>
     Exit Sub

cmdType_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmTicket.cmdType_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub InitialTicket()
     '<EhHeader>
     On Error GoTo InitialTicket_Err
     '</EhHeader>


        Dim i%

100     For i = 1 To UBound(m_Q.QTypeName)
105         cmdType(i - 1).Caption = Replace(m_Q.QTypeName(i), "&", "&&")
110     Next i

     '<EhFooter>
     Exit Sub

InitialTicket_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmTicket.InitialTicket " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub Form_Load()
     '<EhHeader>
     On Error GoTo Form_Load_Err
     '</EhHeader>

100     Call m_Ontop.FormOnTop(Me)
105     Call InitialTicket '��Ŵ�����Ż�������������

     '<EhFooter>
     Exit Sub

Form_Load_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmTicket.Form_Load " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub lstCarNo_Click()

        '<EhHeader>
        On Error GoTo lstCarNo_Click_Err

        '</EhHeader>

        Dim index As Integer
100     index = lstCarNo.ListIndex
105     txtCarNo.Text = lstCarNo.List(index)
110     txtCarLicense.Text = lstCarLicense.List(index)
115     txtCID.Text = lstCID.List(index)

        '<EhFooter>
        Exit Sub

lstCarNo_Click_Err:
        Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & "in Queue.frmTicket.lstCarNo_Click " & "at line " & Erl, EVENTLOG_ERROR_TYPE, Err.Number)

        Resume Next

        '</EhFooter>
End Sub

Private Sub txtCarNo_KeyPress(KeyAscii As Integer)
    If (KeyAscii >= 48 And KeyAscii <= 57) Or (KeyAscii >= 97 And KeyAscii <= 122) Or (KeyAscii = 8) Or (KeyAscii >= 65 And KeyAscii <= 90) Then
        KeyAscii = Asc(UCase$(Chr$(KeyAscii)))
    Else
        KeyAscii = 0
    End If
End Sub
