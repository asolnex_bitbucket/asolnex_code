Attribute VB_Name = "m_year"
Option Explicit

Public Function gYear() As String
     '<EhHeader>
     On Error GoTo gYear_Err
     '</EhHeader>


100     If Val(Format$(Now, "yyyy")) > 2100 Then ' �.�.
105         gYear = CStr(Val(Format$(Now, "yyyy")) - 543)
        Else
110         gYear = Format$(Now, "yyyy")
        End If

     '<EhFooter>
     Exit Function

gYear_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_year.gYear " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function
