Attribute VB_Name = "m_Ontop"
Option Explicit

'//FormOnTop/FormNotOnTop API Declaration
Public Declare Function SetWindowPos _
               Lib "USER32" (ByVal hWnd As Long, _
                             ByVal hWndInsertAfter As Long, _
                             ByVal X As Long, _
                             ByVal Y As Long, _
                             ByVal cx As Long, _
                             ByVal cy As Long, _
                             ByVal wFlags As Long) As Long

'//FormOnTop/FormNotOnTop Constants
Public Const HWND_TOPMOST = -1

Public Const HWND_NOTOPMOST = -2

Public Const SWP_NOSIZE = &H1

Public Const SWP_NOMOVE = &H2

Public Const FLAGS = SWP_NOSIZE Or SWP_NOMOVE

'//FormOnTop Function
Public Function FormOnTop(frm As Form)
     '<EhHeader>
     On Error GoTo FormOnTop_Err
     '</EhHeader>


        Dim SetFrmOnTop As Long
100     SetFrmOnTop = SetWindowPos(frm.hWnd, HWND_TOPMOST, 0, 0, 0, 0, FLAGS)

     '<EhFooter>
     Exit Function

FormOnTop_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Ontop.FormOnTop " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function

'//FormNotOnTop Function
Public Function FormNotOnTop(frm As Form)
     '<EhHeader>
     On Error GoTo FormNotOnTop_Err
     '</EhHeader>


        Dim SetFrmNotOnTop As Long
100     SetFrmNotOnTop = SetWindowPos(frm.hWnd, HWND_NOTOPMOST, 0, 0, 0, 0, FLAGS)

     '<EhFooter>
     Exit Function

FormNotOnTop_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Ontop.FormNotOnTop " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function
