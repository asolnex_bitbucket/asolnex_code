VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cDB"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private cn As ADODB.Connection
Private cnnString As String

Public Enum databaseType
        d_MySQL = 0
        d_SQLSERVER = 1
        d_ASSCESS = 2
End Enum
Public dType As databaseType

Public Function Connect() As Boolean
        '<EhHeader>
        On Error GoTo Connect_Err
        '</EhHeader>

        On Error GoTo e

100     Set cn = New ADODB.Connection

102     Select Case dType
    
            Case 0
104             cnnString = "DRIVER={MySQL ODBC 5.1 Driver};charset=TIS620;Server=" & m_Parking.pkHost & ";Database=" & m_Parking.pkName & ";UID=" & m_Parking.pkUser & ";PWD=" & m_Parking.pkPass & ";Option=3"

106         Case 1
108             cnnString = "Provider=SQLOLEDB.1;Persist Security Info=False;Data Source=" & m_Parking.pkHost & ";User ID=" & m_Parking.pkUser & ";Password=" & m_Parking.pkPass & ";Initial Catalog=" & m_Parking.pkName

110         Case Else
112             cnnString = "DRIVER={MySQL ODBC 5.1 Driver};charset=TIS620;Server=" & m_Parking.pkHost & ";Database=" & m_Parking.pkName & ";UID=" & m_Parking.pkUser & ";PWD=" & m_Parking.pkPass & ";Option=3"
    
        End Select
    
114     cn.ConnectionString = cnnString
116     cn.Open
118     Connect = True
        Exit Function

e:
120     Connect = False

        '<EhFooter>
        Exit Function

Connect_Err:
        Err.Raise vbObjectError + 100, _
                  "Queue.cDB.Connect", _
                  "cDB component failure"
        '</EhFooter>
End Function
Public Function Query(sql As String) As Recordset
        '<EhHeader>
        On Error GoTo Query_Err
        '</EhHeader>
    Dim rs As Recordset
100 Set rs = New Recordset
102 Call Connect
104 rs.CursorLocation = adUseClient
106 rs.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
108 Set Query = rs
110 Set rs = Nothing
        '<EhFooter>
        Exit Function

Query_Err:
        Err.Raise vbObjectError + 100, _
                  "Queue.cDB.Query", _
                  "cDB component failure"
        '</EhFooter>
End Function
Public Sub Disconnect()
        '<EhHeader>
        On Error GoTo Disconnect_Err
        '</EhHeader>
100 cn.Close
        '<EhFooter>
        Exit Sub

Disconnect_Err:
        Err.Raise vbObjectError + 100, _
                  "Queue.cDB.Disconnect", _
                  "cDB component failure"
        '</EhFooter>
End Sub

