Attribute VB_Name = "m_KillProcess"
Option Explicit

Public Sub TerminateProcess(app_exe As String)
     '<EhHeader>
     On Error GoTo TerminateProcess_Err
     '</EhHeader>


        On Error Resume Next

        Dim Process As Object

100     For Each Process In GetObject("winmgmts:").ExecQuery("Select Name from Win32_Process Where Name = '" & app_exe & "'")

105         Process.Terminate
        Next

     '<EhFooter>
     Exit Sub

TerminateProcess_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_KillProcess.TerminateProcess " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

