Attribute VB_Name = "m_printer"
Option Explicit

Public PRT_IP   As String

Public PRT_PORT As Integer

Public Sub Printing(Addr As Integer, _
                    PicNum As Integer, _
                    QNum As String, _
                    iWait As String, _
                    iWaitTime As String, _
                    iCopy As Integer)
     '<EhHeader>
     On Error GoTo Printing_Err
     '</EhHeader>


        Dim Dat(30) As Byte  ' 31 Byte
        Dim i       As Integer
        Dim chkSum  As Byte
        Dim iDate$, iTime$

100     QNum = m_LED.FillStr(QNum, 4)
105     Dat(0) = 2 ' STX
110     Dat(1) = Asc("P")  ' Device 1
115     Dat(2) = Asc("R") ' Device 2
120     Dat(3) = Asc("T") ' Device 3
125     Dat(4) = Addr + 48 ' Address
130     Dat(5) = Asc("1") ' Command
135     Dat(6) = Asc(PicNum)    ' Pic
140     Dat(7) = Asc(Left$(QNum, 1))   ' Q Number
145     Dat(8) = Asc(Mid(QNum, 2, 1))
150     Dat(9) = Asc(Mid(QNum, 3, 1))
155     Dat(10) = Asc(Right$(QNum, 1))
160     Dat(11) = Asc(Left$(iWait, 1)) ' Wait
165     Dat(12) = Asc(Mid$(iWait, 2, 1))
170     Dat(13) = Asc(Right$(iWait, 1))
175     Dat(14) = Asc(Left$(iWaitTime, 1)) ' Wait Time
180     Dat(15) = Asc(Mid$(iWaitTime, 2, 1))
185     Dat(16) = Asc(Mid$(iWaitTime, 3, 1))
190     Dat(17) = Asc(Right$(iWaitTime, 1))
195     If iCopy < 1 Or iCopy > 9 Then iCopy = 1
200     Dat(18) = Asc(iCopy)  ' Copy
    
        'iDate ddMMyy
205     iDate = Format$(Now, "ddMMyy")
210     Dat(19) = Asc(Left$(iDate, 1))  ' Date
215     Dat(20) = Asc(Mid$(iDate, 2, 1))
220     Dat(21) = Asc(Mid$(iDate, 3, 1))
225     Dat(22) = Asc(Mid$(iDate, 4, 1))
230     Dat(23) = Asc(Mid$(iDate, 5, 1))
235     Dat(24) = Asc(Right$(iDate, 1))
    
        'iTime hhmm
240     iTime = Format$(Now, "HHmm")
245     Dat(25) = Asc(Left$(iTime, 1)) ' Time
250     Dat(26) = Asc(Mid$(iTime, 2, 1))
255     Dat(27) = Asc(Mid$(iTime, 3, 1))
260     Dat(28) = Asc(Right$(iTime, 1))
265     Dat(29) = 3 ' ETX (03H)

270     With frmMain.sck
275         .RemoteHost = PRT_IP
280         .RemotePort = PRT_PORT
285         chkSum = Dat(0)

290         For i = 1 To 29
295             chkSum = chkSum Xor Dat(i)
300         Next i

305         Dat(30) = chkSum
        End With
       'frmMain.sck.SendData ("$$$")
       'DoEvents: Sleep 1000
310     frmMain.sck.SendData (Dat)
   'Call LogNTEvent(vbCrLf & vbCrLf & "TEST PINTING", EVENTLOG_ERROR_TYPE, 0) ' For Debug
     '<EhFooter>
     Exit Sub

Printing_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_printer.Printing " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

