VERSION 5.00
Begin VB.Form frmDBConfig 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Database Setting"
   ClientHeight    =   4920
   ClientLeft      =   2850
   ClientTop       =   1755
   ClientWidth     =   4470
   ControlBox      =   0   'False
   Icon            =   "frmDBConfig.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4920
   ScaleWidth      =   4470
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkPkSystem 
      Caption         =   "Parking System"
      Height          =   270
      Left            =   120
      TabIndex        =   20
      Top             =   1980
      Width           =   1500
   End
   Begin VB.Frame fPk 
      Caption         =   "Connection Parking System"
      Height          =   1710
      Left            =   120
      TabIndex        =   11
      Top             =   2340
      Width           =   4230
      Begin VB.TextBox txtpkHost 
         Height          =   330
         Left            =   1125
         TabIndex        =   15
         Top             =   240
         Width           =   3015
      End
      Begin VB.TextBox txtpkDB 
         Height          =   300
         Left            =   1125
         TabIndex        =   14
         Top             =   1260
         Width           =   3015
      End
      Begin VB.TextBox txtpkPass 
         Height          =   300
         IMEMode         =   3  'DISABLE
         Left            =   1125
         PasswordChar    =   "�"
         TabIndex        =   13
         Top             =   930
         Width           =   3015
      End
      Begin VB.TextBox txtpkUser 
         Height          =   300
         Left            =   1125
         TabIndex        =   12
         Top             =   585
         Width           =   3015
      End
      Begin VB.Label lblStep3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "&Server:"
         Height          =   195
         Index           =   7
         Left            =   135
         TabIndex        =   19
         Top             =   315
         Width           =   510
      End
      Begin VB.Label lblStep3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Data&base:"
         Height          =   195
         Index           =   5
         Left            =   135
         TabIndex        =   18
         Top             =   1320
         Width           =   735
      End
      Begin VB.Label lblStep3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "&Password:"
         Height          =   195
         Index           =   1
         Left            =   135
         TabIndex        =   17
         Top             =   975
         Width           =   735
      End
      Begin VB.Label lblStep3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "&UID:"
         Height          =   195
         Index           =   0
         Left            =   135
         TabIndex        =   16
         Top             =   630
         Width           =   330
      End
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Close"
      Height          =   450
      Left            =   2295
      MaskColor       =   &H00FF0000&
      TabIndex        =   9
      Top             =   4185
      Width           =   1440
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&OK"
      Height          =   450
      Left            =   690
      TabIndex        =   8
      Top             =   4185
      Width           =   1440
   End
   Begin VB.Frame fraStep3 
      Caption         =   "Connection Values"
      Height          =   1710
      Index           =   0
      Left            =   120
      TabIndex        =   10
      Top             =   120
      Width           =   4230
      Begin VB.TextBox txtUID 
         Height          =   300
         Left            =   1125
         TabIndex        =   1
         Top             =   600
         Width           =   3015
      End
      Begin VB.TextBox txtPWD 
         Height          =   300
         IMEMode         =   3  'DISABLE
         Left            =   1125
         PasswordChar    =   "�"
         TabIndex        =   3
         Top             =   930
         Width           =   3015
      End
      Begin VB.TextBox txtDatabase 
         Height          =   300
         Left            =   1125
         TabIndex        =   5
         Top             =   1260
         Width           =   3015
      End
      Begin VB.TextBox txtServer 
         Height          =   330
         Left            =   1125
         TabIndex        =   7
         Top             =   240
         Width           =   3015
      End
      Begin VB.Label lblStep3 
         AutoSize        =   -1  'True
         Caption         =   "&UID:"
         Height          =   195
         Index           =   2
         Left            =   135
         TabIndex        =   0
         Top             =   630
         Width           =   330
      End
      Begin VB.Label lblStep3 
         AutoSize        =   -1  'True
         Caption         =   "&Password:"
         Height          =   195
         Index           =   3
         Left            =   135
         TabIndex        =   2
         Top             =   975
         Width           =   735
      End
      Begin VB.Label lblStep3 
         AutoSize        =   -1  'True
         Caption         =   "Data&base:"
         Height          =   195
         Index           =   4
         Left            =   135
         TabIndex        =   4
         Top             =   1320
         Width           =   735
      End
      Begin VB.Label lblStep3 
         AutoSize        =   -1  'True
         Caption         =   "&Server:"
         Height          =   195
         Index           =   6
         Left            =   135
         TabIndex        =   6
         Top             =   315
         Width           =   510
      End
   End
End
Attribute VB_Name = "frmDBConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub chkPkSystem_Click()
     '<EhHeader>
     On Error GoTo chkPkSystem_Click_Err
     '</EhHeader>
100 fPk.Visible = chkPkSystem.Value
     '<EhFooter>
     Exit Sub

chkPkSystem_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmDBConfig.chkPkSystem_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub cmdCancel_Click()
     '<EhHeader>
     On Error GoTo cmdCancel_Click_Err
     '</EhHeader>


100     g_forceClose = True
105     Unload Me

     '<EhFooter>
     Exit Sub

cmdCancel_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmDBConfig.cmdCancel_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub cmdOK_Click()
     '<EhHeader>
     On Error GoTo cmdOK_Click_Err
     '</EhHeader>
100     MousePointer = 11
105     Call m_DB.Disconnect
        '<Save and Read config parameter MySql>
110     dbHost = txtServer.Text
115     dbUser = txtUID.Text
120     dbDBName = txtDatabase.Text
125     dbPass = txtPWD.Text
130     Call m_Ini.WriteIni("Config", "dbHost", dbHost, IniFile)
135     Call m_Ini.WriteIni("Config", "dbUser", dbUser, IniFile)
140     Call m_Ini.WriteIni("Config", "dbDBName", dbDBName, IniFile)
145     Call m_Ini.WriteIni("Config", "dbPass", m_Encode.Base64EncodeString(dbPass), IniFile)
        'Debug.Print m_Encode.Base64EncodeString("351286")
        'Debug.Print m_Encode.Base64DecodeString("MzUxMjg2")
        '</Save config parameter MySql>
150     m_Parking.pkSystem = chkPkSystem.Value
155     Call m_Ini.WriteIni("Config", "pkSystem", chkPkSystem.Value, IniFile)
160     If m_Parking.pkSystem Then
165         Call m_Ini.WriteIni("Config", "pkHost", txtpkHost.Text, IniFile)
170         Call m_Ini.WriteIni("Config", "pkUser", txtpkUser.Text, IniFile)
175         Call m_Ini.WriteIni("Config", "pkDB", txtpkDB.Text, IniFile)
180         Call m_Ini.WriteIni("Config", "pkPass", m_Encode.Base64EncodeString(txtpkPass.Text), IniFile)
    
185         Call m_Parking.initialParking(m_Ini.ReadINI("Config", "pkHost", IniFile), m_Ini.ReadINI("Config", "pkDB", IniFile), m_Ini.ReadINI("Config", "pkUser", IniFile), m_Encode.Base64DecodeString(m_Ini.ReadINI("Config", "pkPass", IniFile)))
190         If Not m_Parking.Connection Then
195             MsgBox "Can't connecting to ParkingSystem Database !!!", vbCritical + vbQuestion, "Warring"
            End If
        End If
    
        '<Connect Mysql>
200     m_DB.Connect
        '</Connect Mysql>
    
205     If g_Connected Then
210         MsgBox "Database config successful"
215         Call frmMain.ReadData
220         Unload Me
        End If
225     MousePointer = 0
     '<EhFooter>
     Exit Sub

cmdOK_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmDBConfig.cmdOK_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub Form_Load()
     '<EhHeader>
     On Error GoTo Form_Load_Err
     '</EhHeader>

100     txtServer.Text = m_global.dbHost
105     txtUID.Text = m_global.dbUser
110     txtPWD.Text = m_global.dbPass
115     txtDatabase.Text = m_global.dbDBName
    
120     chkPkSystem.Value = IIf(m_Parking.pkSystem, 1, 0)
125     fPk.Visible = chkPkSystem.Value

130     txtpkHost.Text = m_Ini.ReadINI("Config", "pkHost", IniFile)
135     txtpkUser.Text = m_Ini.ReadINI("Config", "pkUser", IniFile)
140     txtpkPass.Text = m_Encode.Base64DecodeString(m_Ini.ReadINI("Config", "pkPass", IniFile))
145     txtpkDB.Text = m_Ini.ReadINI("Config", "pkDB", IniFile)

     '<EhFooter>
     Exit Sub

Form_Load_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmDBConfig.Form_Load " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub
