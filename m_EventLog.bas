Attribute VB_Name = "m_EventLog"
Declare Function RegisterEventSource _
        Lib "advapi32.dll" _
        Alias "RegisterEventSourceA" (ByVal lpUNCServerName As String, _
                                      ByVal lpSourceName As String) As Long
Declare Function DeregisterEventSource _
        Lib "advapi32.dll" (ByVal hEventLog As Long) As Long
Declare Function ReportEvent _
        Lib "advapi32.dll" _
        Alias "ReportEventA" (ByVal hEventLog As Long, _
                              ByVal wType As Integer, _
                              ByVal wCategory As Integer, _
                              ByVal dwEventID As Long, _
                              ByVal lpUserSid As Any, _
                              ByVal wNumStrings As Integer, _
                              ByVal dwDataSize As Long, _
                              plpStrings As Long, _
                              lpRawData As Any) As Boolean
Declare Function GetLastError Lib "kernel32" () As Long
Declare Sub CopyMemory _
        Lib "kernel32" _
        Alias "RtlMoveMemory" (hpvDest As Any, _
                               hpvSource As Any, _
                               ByVal cbCopy As Long)
Declare Function GlobalAlloc _
        Lib "kernel32" (ByVal wFlags As Long, _
                        ByVal dwBytes As Long) As Long
Declare Function GlobalFree Lib "kernel32" (ByVal hMem As Long) As Long

Public Const EVENTLOG_SUCCESS = 0

Public Const EVENTLOG_ERROR_TYPE = 1

Public Const EVENTLOG_WARNING_TYPE = 2

Public Const EVENTLOG_INFORMATION_TYPE = 4

Public Const EVENTLOG_AUDIT_SUCCESS = 8

Public Const EVENTLOG_AUDIT_FAILURE = 10

Public Sub LogNTEvent(sString As String, iLogType As Integer, iEventID As Long)
     '<EhHeader>
     On Error GoTo LogNTEvent_Err
     '</EhHeader>


        Dim bRC          As Boolean
        Dim iNumStrings  As Integer
        Dim hEventLog    As Long
        Dim hMsgs        As Long
        Dim cbStringSize As Long
100     hEventLog = RegisterEventSource("", App.Title)
105     cbStringSize = Len(sString) + 1
110     hMsgs = GlobalAlloc(&H40, cbStringSize)
115     CopyMemory ByVal hMsgs, ByVal sString, cbStringSize
120     iNumStrings = 1
125     If ReportEvent(hEventLog, iLogType, 0, iEventID, 0&, iNumStrings, cbStringSize, hMsgs, hMsgs) = 0 Then
            'MsgBox GetLastError()
        End If
130     Call GlobalFree(hMsgs)
135     DeregisterEventSource (hEventLog)

     '<EhFooter>
     Exit Sub

LogNTEvent_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_EventLog.LogNTEvent " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Sub Main()
     '<EhHeader>
     On Error GoTo Main_Err
     '</EhHeader>


100     Call LogNTEvent("Information from " & App.EXEName, EVENTLOG_INFORMATION_TYPE, 1001)
105     Call LogNTEvent("Warning from " & App.EXEName, EVENTLOG_WARNING_TYPE, 1002)
110     Call LogNTEvent("Error from " & App.EXEName, EVENTLOG_ERROR_TYPE, 1003)
115     MsgBox "Done"

     '<EhFooter>
     Exit Sub

Main_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_EventLog.Main " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub
