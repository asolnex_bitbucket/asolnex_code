VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmReport 
   BackColor       =   &H00E0E0E0&
   Caption         =   "Report"
   ClientHeight    =   4755
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6885
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   222
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmReport.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   4755
   ScaleWidth      =   6885
   StartUpPosition =   2  'CenterScreen
   Begin MSComCtl2.MonthView mvEnd 
      Height          =   2370
      Left            =   3960
      TabIndex        =   4
      Top             =   750
      Width           =   2490
      _ExtentX        =   4392
      _ExtentY        =   4180
      _Version        =   393216
      ForeColor       =   -2147483630
      BackColor       =   14737632
      Appearance      =   1
      StartOfWeek     =   170065922
      CurrentDate     =   42383
   End
   Begin MSComCtl2.MonthView mvBegin 
      Height          =   2370
      Left            =   540
      TabIndex        =   3
      Top             =   750
      Width           =   2490
      _ExtentX        =   4392
      _ExtentY        =   4180
      _Version        =   393216
      ForeColor       =   -2147483630
      BackColor       =   14737632
      Appearance      =   1
      StartOfWeek     =   170065922
      CurrentDate     =   42383
   End
   Begin VB.CommandButton cmdExport 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Height          =   1290
      Left            =   225
      MaskColor       =   &H00E0E0E0&
      Picture         =   "frmReport.frx":058A
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   3255
      Width           =   6390
   End
   Begin VB.Label lblDateBegin 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Date End"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   15.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   4455
      TabIndex        =   1
      Top             =   315
      Width           =   1710
   End
   Begin VB.Label lblDateBegin 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Date Begin"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   15.75
         Charset         =   222
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   855
      TabIndex        =   0
      Top             =   315
      Width           =   1710
   End
End
Attribute VB_Name = "frmReport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function MakeSureDirectoryPathExists _
                Lib "imagehlp.dll" (ByVal lpPath As String) As Long
Dim dateBegin As String, dateEnd As String

Private Sub cmdExport_Click()
     '<EhHeader>
     On Error GoTo cmdExport_Click_Err
     '</EhHeader>


100     dateBegin = mvBegin.Year & "-" & Format(mvBegin.Month, "00") & "-" & Format(mvBegin.Day, "00")
105     dateEnd = mvEnd.Year & "-" & Format(mvEnd.Month, "00") & "-" & Format(mvEnd.Day, "00")
110     MousePointer = 11
115     dbBusy = True
120     Call rtnExport
125     dbBusy = False
130     MousePointer = 0

     '<EhFooter>
     Exit Sub

cmdExport_Click_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmReport.cmdExport_Click " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub rtnExport()
     '<EhHeader>
     On Error GoTo rtnExport_Err
     '</EhHeader>


        Dim sql        As String
        Dim rs1        As ADODB.Recordset
        Dim r%, fName$
        Dim excelApp   As Excel.Application
        Dim excelWB    As Excel.Workbook
        Dim excelWS    As Excel.Worksheet
        Dim myDocuPath As String
100     myDocuPath = Environ$("USERPROFILE") & "\My Documents\QueueReport\"
105     Set excelApp = CreateObject("Excel.Application")
110     excelApp.Visible = False
115     Call CreateFolder(myDocuPath)
120     fName = myDocuPath & Replace(dateBegin, "-", "") & "-" & Replace(dateEnd, "-", "") & ".xlsx"
125     Set excelWB = excelApp.Workbooks.Open(App.Path & "\Template\REPORT.xlsx", ReadOnly:=True)
130     Set excelWS = excelWB.Worksheets(1)
135     excelWS.Cells(1, "I") = dateBegin & ""
140     excelWS.Cells(1, "M") = dateEnd & ""
        'sql = "SELECT *,TIMESTAMPDIFF(MINUTE,QTime2,QTime4) as totalTime,TIMESTAMPDIFF(MINUTE,QTime3,QTime4) as LoadTime,TIMESTAMPDIFF(MINUTE,QTime21,QTime22) as BottleTime FROM tblQueue WHERE (QDate>='" & dateBegin & "' AND QDate<='" & dateEnd & "' AND QTime5>0) OR (QDate>='" & dateBegin & "' AND QDate<='" & dateEnd & "' AND QDelete=1)"
145     sql = "SELECT *,TIMESTAMPDIFF(MINUTE,QTime2,QTime4) as totalTime,TIMESTAMPDIFF(MINUTE,QTime3,QTime4) as LoadTime,TIMESTAMPDIFF(MINUTE,QTime21,QTime22) as BottleTime FROM tblQueue WHERE QDate>='" & dateBegin & "' AND QDate<='" & dateEnd & "'"
150     Set rs1 = New ADODB.Recordset
155     rs1.CursorLocation = adUseClient
160     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
165     r = 4

170     Do While Not rs1.EOF

175         With excelWS
180             .Cells(r, "A") = rs1!QDate & ""
185             .Cells(r, "B") = rs1!QNum & ""
190             .Cells(r, "C") = rs1!QCarNo & ""
195             .Cells(r, "D") = rs1!QCarLicense & ""
200             .Cells(r, "E") = m_Q.QTypeName(Val(rs1!QType))
205             .Cells(r, "F") = rs1!QLane & ""
210             .Cells(r, "G") = Format(rs1!QTime1, "dd/MM/yyyy hh:mm:ss")
215             .Cells(r, "H") = Format(rs1!QTime2, "dd/MM/yyyy hh:mm:ss")
            
220             .Cells(r, "I") = rs1!QStep1Call_1 & ""
225             .Cells(r, "J") = rs1!QStep1Call_2 & ""
230             .Cells(r, "K") = rs1!QStep1Call_3 & ""
            
235             .Cells(r, "L") = Format(rs1!QTime21, "dd/MM/yyyy hh:mm:ss")
240             .Cells(r, "M") = Format(rs1!QTime22, "dd/MM/yyyy hh:mm:ss")
245             .Cells(r, "N") = rs1!BottleTime & ""
            
250             .Cells(r, "O") = rs1!QCall_1 & ""
255             .Cells(r, "P") = rs1!QCall_2 & ""
260             .Cells(r, "Q") = rs1!QCall_3 & ""
            
265             .Cells(r, "R") = Format(rs1!QTime3, "dd/MM/yyyy hh:mm:ss")
270             .Cells(r, "S") = Format(rs1!QTime4, "dd/MM/yyyy hh:mm:ss")
275             .Cells(r, "T") = rs1!LoadTime & ""
280             .Cells(r, "U") = rs1!totalTime & ""
285             .Cells(r, "V") = Format(rs1!QTime5, "dd/MM/yyyy hh:mm:ss")
            
290             If rs1!QDelete Then
295                 .Cells(r, "W") = "YES"
                Else
300                 .Cells(r, "W") = ""
                End If
            
305             .Range("A" & r, "W" & r).Borders.Color = vbBlack
310             .Range("A" & r, "W" & r).Borders.LineStyle = xlContinuous
315             .Range("A" & r, "W" & r).Borders.Weight = xlThin
            End With

320         DoEvents: Sleep 1
325         r = r + 1
330         rs1.MoveNext
        Loop

335     Set rs1 = Nothing
     
340     excelApp.DisplayAlerts = False
345     excelWB.SaveAs fName
350     excelApp.Quit
355     Set excelApp = Nothing
360     Set excelWB = Nothing
365     Call m_KillProcess.TerminateProcess("excel.exe")
370     Call m_OpenFile.Start(fName)
 
     '<EhFooter>
     Exit Sub

rtnExport_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmReport.rtnExport " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub CreateFolder(ByVal pstrFolder As String)
     '<EhHeader>
     On Error GoTo CreateFolder_Err
     '</EhHeader>


100     If Right$(pstrFolder, 1) <> "\" Then pstrFolder = pstrFolder & "\"
105     MakeSureDirectoryPathExists pstrFolder

     '<EhFooter>
     Exit Sub

CreateFolder_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmReport.CreateFolder " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub Form_Load()
     '<EhHeader>
     On Error GoTo Form_Load_Err
     '</EhHeader>

100     Call m_Ontop.FormOnTop(Me)
105     mvBegin.Year = m_year.gYear
110     mvBegin.Month = Format(Now, "m")
115     mvBegin.Day = Format(Now, "d")
120     mvEnd.Year = mvBegin.Year
125     mvEnd.Month = mvBegin.Month
130     mvEnd.Day = mvBegin.Day

     '<EhFooter>
     Exit Sub

Form_Load_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.frmReport.Form_Load " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

