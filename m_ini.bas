Attribute VB_Name = "m_Ini"
Option Explicit
#If Win16 Then
    Declare Function WritePrivateProfileString _
            Lib "Kernel" (ByVal appName As String, _
                          ByVal KeyName As String, _
                          ByVal NewString As String, _
                          ByVal filename As String) As Integer
    Declare Function GetPrivateProfileString _
            Lib "Kernel" _
            Alias "GetPrivateProfilestring" (ByVal appName As String, _
                                             ByVal KeyName As Any, _
                                             ByVal Default As String, _
                                             ByVal ReturnedString As String, _
                                             ByVal MAXSIZE As Integer, _
                                             ByVal filename As String) As Integer
#Else
    Declare Function GetPrivateProfileString _
            Lib "kernel32" _
            Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, _
                                              ByVal lpKeyName As String, _
                                              ByVal lpDefault As String, _
                                              ByVal lpReturnedString As String, _
                                              ByVal nSize As Long, _
                                              ByVal lpFileName As String) As Long
    Declare Function WritePrivateProfileString _
            Lib "kernel32" _
            Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, _
                                                ByVal lpKeyName As String, _
                                                ByVal lpString As Any, _
                                                ByVal lpFileName As String) As Long
#End If

'***************************************
'     ****************
'* Procedure Name: sReadINI*
'*======================================
'     ===============*
'*Returns a string from an INI file. To
'     use, call the *
'*functions and pass it the Section, Key
'     Name and INI*
'*File Name, [sRet=sReadINI(Section,Key1
'     ,INIFile)].*
'*val command. *
'***************************************
'     ****************
Function ReadINI(Section, KeyName, filename As String) As String
     '<EhHeader>
     On Error GoTo ReadINI_Err
     '</EhHeader>


        Dim sRet As String
100     sRet = String(255, Chr(0))
105     ReadINI = Left(sRet, GetPrivateProfileString(Section, ByVal KeyName, "", sRet, Len(sRet), filename))

        'Debug.Print ReadINI
     '<EhFooter>
     Exit Function

ReadINI_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Ini.ReadINI " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function
'***************************************
'     ****************
'* Procedure Name: WriteINI*
'*======================================
'     ===============*
'*Writes a string to an INI file. To use
'     , call the *
'*function and pass it the sSection, sKe
'     yName, the New *
'*String and the INI File Name,*
'*[Ret=WriteINI(Section,Key,String,INIFi
'     le)]. *
'*Returns a 1 if there were no errors an
'     d *
'*a 0 if there were errors.*
'***************************************
'     ****************

Function WriteIni(sSection As String, _
                  sKeyName As String, _
                  sNewString As String, _
                  sFileName) As Integer
     '<EhHeader>
     On Error GoTo WriteIni_Err
     '</EhHeader>


        Dim r
100     r = WritePrivateProfileString(sSection, sKeyName, sNewString, sFileName)

     '<EhFooter>
     Exit Function

WriteIni_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Ini.WriteIni " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function
