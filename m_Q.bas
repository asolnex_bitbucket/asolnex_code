Attribute VB_Name = "m_Q"
Option Explicit

Public QTypeName(1 To 4)  As String

Public QPic(1 To 4)       As Integer

Public QCopy(1 To 4)      As Integer

Public QLaneType(1 To 16) As Integer

Private QMin(1 To 4)      As Integer

Private LaneBusy(1 To 16) As Boolean

Public Sub QTypeLoad()
     '<EhHeader>
     On Error GoTo QTypeLoad_Err
     '</EhHeader>


        Dim sql As String
        Dim rs1 As ADODB.Recordset
        Dim i%
100     sql = "SELECT * FROM tblQType ORDER BY QType"
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
120     i = 1

125     Do While Not rs1.EOF
130         QTypeName(i) = rs1!QDesc
135         QPic(i) = Val(rs1!QPic)
140         QCopy(i) = Val(rs1!QCopy)
145         QMin(i) = Val(rs1!QMin)
150         i = i + 1
155         rs1.MoveNext
        Loop
   
160     Set rs1 = Nothing

     '<EhFooter>
     Exit Sub

QTypeLoad_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.QTypeLoad " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub LaneTypeLoad()
     '<EhHeader>
     On Error GoTo LaneTypeLoad_Err
     '</EhHeader>


        Dim sql As String
        Dim rs1 As ADODB.Recordset
        Dim i%
100     sql = "SELECT * FROM tblKeyPad ORDER BY Lane"
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
120     i = 1

125     Do While Not rs1.EOF
130         QLaneType(i) = Val(rs1!QType)
135         i = i + 1
140         rs1.MoveNext
        Loop
   
145     Set rs1 = Nothing

     '<EhFooter>
     Exit Sub

LaneTypeLoad_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.LaneTypeLoad " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub getQ(QType As Integer, _
                QBottle As Integer, _
                QCarNo As String, _
                QCarLicense As String, _
                QCID As Long)
     '<EhHeader>
     On Error GoTo getQ_Err
     '</EhHeader>

        Dim rs1     As ADODB.Recordset
        Dim sql$
        Dim tmpQNum As Integer
        Dim tmpQW   As Integer
100     Call m_DB.Disconnect
105     Call m_DB.Connect
110     sql = "SELECT a.QNum,b.QMin FROM tblQueue a INNER JOIN tblQType b ON a.QType=b.QType WHERE a.QDate = CURDATE() AND a.QType=" & QType & " AND a.QNum>=b.QMin AND a.QNum<b.QMax ORDER BY a.QNum DESC" '���� < max ������Ҥ���ش���µ�ͧ� + 1 ������ 99 �ʹ�
115     Set rs1 = New ADODB.Recordset
120     rs1.CursorLocation = adUseClient
125     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
    
130     If rs1.EOF Then  '����ѧ������Ţ��� �����Թ��ǧ������������������ ��� min
135         tmpQNum = QMin(QType)
140         tmpQW = 0
        Else
145         tmpQNum = Val(rs1!QNum) + 1
150         tmpQW = getQW
        End If
        'm_Printer.Printing(QType,tmpQNum,NumberFormat(tmpQW,3,0),"1")
155     Call InsertQ(tmpQNum, QType, QBottle, QCarNo, QCarLicense, QCID)
160     If m_Parking.pkSystem Then
165         Call UpdateTimeIn(QCID)
        End If
170     Call m_printer.Printing(1, QPic(QType), Format$(tmpQNum, "000"), Format$(tmpQW, "000"), "0000", QCopy(QType))
    
        'Call UpdateCar(QCarNo)
175     Set rs1 = Nothing

     '<EhFooter>
     Exit Sub

getQ_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.getQ " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub
Private Sub UpdateTimeIn(CID As Long)
     '<EhHeader>
     On Error GoTo UpdateTimeIn_Err
     '</EhHeader>
     Dim sql As String
        Dim rs1 As ADODB.Recordset
100     sql = "UPDATE tblQueue SET QTime1='" & getQTimeIN(CID) & "' WHERE QCarID=" & CID
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
120     Set rs1 = Nothing
     '<EhFooter>
     Exit Sub

UpdateTimeIn_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.UpdateTimeIn " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Function getQTimeIN(CID As Long)
     '<EhHeader>
     On Error GoTo getQTimeIN_Err
     '</EhHeader>
        Dim sql As String
        Dim rs1 As ADODB.Recordset
100     sql = "SELECT ParkingTime FROM tblParking WHERE CID=" & CID
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
120     If Not rs1.EOF Then
125         If Val(Format$(Now, "yyyy")) > 2100 Then ' �.�.
130             getQTimeIN = Format$(rs1!ParkingTime, "yyyy") - 543 & Format$(rs1!ParkingTime, "-MM-dd hh:mm:ss")
            Else
135             getQTimeIN = Format$(rs1!ParkingTime, "yyyy") & Format$(rs1!ParkingTime, "-MM-dd hh:mm:ss")
            End If
        Else
140         getQTimeIN = ""
        End If
145     Set rs1 = Nothing
     '<EhFooter>
     Exit Function

getQTimeIN_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.getQTimeIN " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function
Private Sub UpdateCar(carNo As String)
     '<EhHeader>
     On Error GoTo UpdateCar_Err
     '</EhHeader>


        Dim sql As String
        Dim rs1 As ADODB.Recordset
100     sql = "UPDATE tblCar SET Printed=1 WHERE CarNo='" & carNo & "' AND CarDate='" & m_year.gYear & Format(Now, "-MM-dd") & "'"
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
120     Set rs1 = Nothing

     '<EhFooter>
     Exit Sub

UpdateCar_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.UpdateCar " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Function getQW() As Integer
     '<EhHeader>
     On Error GoTo getQW_Err
     '</EhHeader>


        Dim sql As String
        Dim rs1 As ADODB.Recordset
100     sql = "SELECT COUNT(*) as QW FROM tblqueue WHERE  QNum>0 AND QCall=0 AND QDelete=0"
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
120     getQW = Val(rs1!QW)
125     Set rs1 = Nothing

     '<EhFooter>
     Exit Function

getQW_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.getQW " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function

Sub InsertQ(QNum As Integer, _
            QType As Integer, _
            QBottle As Integer, _
            QCarNo As String, _
            QCarLicense As String, _
            QCID As Long)
     '<EhHeader>
     On Error GoTo InsertQ_Err
     '</EhHeader>


        Dim tmpStep1Call As Integer
        Dim rs1          As ADODB.Recordset
        Dim sql          As String

100     If QBottle = 1 Then
105         tmpStep1Call = 0
        Else
110         tmpStep1Call = 1
        End If
        '=============�ѧ���������д���Ẻ����͹======================
        '������� QTime 1 仡�͹��
115     sql = "INSERT INTO tblQueue (QNum,QCarID,QType,QDate,QStep1Call,QBottle,QCarNo,QCarLicense,QTime1,QTime2) VALUES (" & QNum & "," & QCID & "," & QType & ",CURDATE()," & tmpStep1Call & "," & QBottle & ",'" & QCarNo & "','" & QCarLicense & "',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP)"
        '======================================================
        'sql = "INSERT INTO tblQueue (QNum,QType,QDate,QStep1Call,QBottle,QCarNo,QTime2) VALUES (" & QNum & "," & QType & ",CURDATE()," & tmpStep1Call & "," & QBottle & ",'" & QCarNo & "',CURRENT_TIMESTAMP)"
120     Set rs1 = New ADODB.Recordset
125     rs1.CursorLocation = adUseClient
130     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
135     Set rs1 = Nothing
    
     '<EhFooter>
     Exit Sub

InsertQ_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.InsertQ " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub QWait(fg As VSFlexGrid) 'ö�����¡
     '<EhHeader>
     On Error GoTo QWait_Err
     '</EhHeader>

        Dim rs1   As ADODB.Recordset
        Dim sql   As String
        Dim row   As Integer
        Dim fgDSP As VSFlexGrid
        'SELECT *,TIMESTAMPDIFF(MINUTE,QTime22,CURRENT_TIMESTAMP) as WaitTimeBottle
        '    sql = "SELECT *,TIMESTAMPDIFF(MINUTE,QTime2,CURRENT_TIMESTAMP) as WaitTime,TIMESTAMPDIFF(MINUTE,QTime22,CURRENT_TIMESTAMP) as WaitTimeBottle FROM tblQueue WHERE QCall=0 OR QStep1Call=0 ORDER BY QTime22,QTime2 ASC"
100     sql = "SELECT *,TIMESTAMPDIFF(MINUTE,QTime2,CURRENT_TIMESTAMP) as WaitTime,TIMESTAMPDIFF(MINUTE,QTime22,CURRENT_TIMESTAMP) as WaitTimeBottle FROM tblQueue WHERE ((ISNULL(QTime21) AND ISNULL(QTime3) AND QCall<=3)   OR (ISNULL(QTime3) AND NOT(ISNULL(QStep1Call_1)) AND QStep1Call<=3))  AND QDelete=0 ORDER BY QTime2 ASC"
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
120     fg.Rows = fg.FixedRows
125     row = fg.FixedRows
130     Set fgDSP = frmDSP32.fgQWait
135     fgDSP.Rows = fgDSP.FixedRows

        'RowDSP = fgDSP.FixedRows
140     Do While Not rs1.EOF
        
145         With fg
           
150             If rs1!QBottle Then
155                 If Val(rs1!QStep1Call) = 0 Or IsNull(rs1!QTime21) Then  'öŧ�Ǵ����ѧ������¡
160                     .AddItem "", .Rows
165                     .TextMatrix(row, .Cols - 1) = rs1!QID & ""
170                     .TextMatrix(row, 0) = rs1!QNum
175                     .TextMatrix(row, 1) = rs1!QCarNo
180                     .TextMatrix(row, 2) = rs1!QCarLicense
185                     .TextMatrix(row, 3) = m_Q.QTypeName(Val(rs1!QType)) & " (Bottle)"
190                     .TextMatrix(row, 4) = rs1!WaitTime & ""
195                     .TextMatrix(row, 5) = Format$(Right$(rs1!QTime2, 8), "hh:MM:ss") & "" '�����Ѻ�ѵä��
200                     If Val(rs1!QStep1Call) = 0 Then
205                         .TextMatrix(row, 6) = "Wait" 'ʶҹ� Wait or Call
                        Else
210                         .TextMatrix(row, 6) = "Call (" & rs1!QStep1Call & ")" 'ʶҹ� Wait or Call
                        End If
                    
215                     fgDSP.AddItem "", fgDSP.Rows
220                     fgDSP.TextMatrix(row + 1, 0) = rs1!QCarNo
225                     fgDSP.TextMatrix(row + 1, 1) = m_Q.QTypeName(Val(rs1!QType)) & " (Bottle)"
230                     fgDSP.TextMatrix(row + 1, 2) = rs1!QNum
235                     fgDSP.TextMatrix(row + 1, 3) = rs1!WaitTime & ""
                      
240                     row = row + 1
                    Else

                        '                    If Not IsNull(rs1!QTime21) And Not IsNull(rs1!QTime22) Then 'ŧ�Ǵ���� �����¡�����Ŵ
                        '
                        '                     .AddItem "", .Rows
                        '                    .TextMatrix(row, 0) = rs1!QNum
                        '                    .TextMatrix(row, 1) = rs1!QCarNo
                        '                    .TextMatrix(row, 2) = rs1!QCarLicense
                        '                    .TextMatrix(row, 3) = m_Q.QTypeName(Val(rs1!QType))
                        '                    .TextMatrix(row, 4) = rs1!WaitTimeBottle & ""
                        '                    row = row + 1
                        '
                        '                    End If
                    End If

                Else
245                 .AddItem "", .Rows
250                 fgDSP.AddItem "", fgDSP.Rows
255                 .TextMatrix(row, .Cols - 1) = rs1!QID & ""
260                 .TextMatrix(row, 0) = rs1!QNum
265                 .TextMatrix(row, 1) = rs1!QCarNo
270                 .TextMatrix(row, 2) = rs1!QCarLicense
275                 .TextMatrix(row, 3) = m_Q.QTypeName(Val(rs1!QType))
280                 fgDSP.TextMatrix(row + 1, 0) = rs1!QCarNo
285                 fgDSP.TextMatrix(row + 1, 1) = m_Q.QTypeName(Val(rs1!QType))
290                 fgDSP.TextMatrix(row + 1, 2) = rs1!QNum

295                 If Not IsNull(rs1!QTime21) And Not IsNull(rs1!QTime22) Then 'ŧ�Ǵ���� �����¡�����Ŵ
300                     .TextMatrix(row, 4) = rs1!WaitTimeBottle & ""
305                     fgDSP.TextMatrix(row + 1, 3) = rs1!WaitTimeBottle & ""
                    Else
310                     .TextMatrix(row, 4) = rs1!WaitTime & ""
315                     fgDSP.TextMatrix(row + 1, 3) = rs1!WaitTime & ""
                    End If
                     
320                 .TextMatrix(row, 5) = Format$(Right$(rs1!QTime2, 8), "hh:MM:ss") & "" '�����Ѻ�ѵä��
325                 If Val(rs1!QCall) = 0 Then
330                     .TextMatrix(row, 6) = "Wait" 'ʶҹ� Wait or Call
                    Else
335                     .TextMatrix(row, 6) = "Call (" & rs1!QCall & ")" 'ʶҹ� Wait or Call
                    End If
                     
340                 row = row + 1
                End If

            End With

345         rs1.MoveNext
        Loop
    
350     Set rs1 = Nothing

        '==============SUM TOTAL Queue ===============
355     If fg.Rows > fg.FixedRows Then
        
360         If m_Parking.pkSystem Then
365             fg.AddItem "���" & vbTab & fg.Rows - fg.FixedRows & vbTab & "ö��ç�ҹ" & vbTab & CalCarInFactory, fg.Rows
370             fg.Cell(flexcpFontBold, fg.Rows - 1, 0, fg.Rows - 1, 3) = True
375             fg.Cell(flexcpBackColor, fg.Rows - 1, 0, fg.Rows - 1, 3) = vbGreen
380             fg.Cell(flexcpAlignment, fg.Rows - 1, 3, fg.Rows - 1, 3) = 4
            Else
385             fg.AddItem "���" & vbTab & fg.Rows - fg.FixedRows, fg.Rows
390             fg.Cell(flexcpFontBold, fg.Rows - 1, 0, fg.Rows - 1, 1) = True
395             fg.Cell(flexcpBackColor, fg.Rows - 1, 0, fg.Rows - 1, 1) = vbGreen
            End If
            'fg.Cell(flexcpFontSize, fg.Rows - 1, 0, fg.Rows - 1, fg.Cols - 1) = 12
400         fg.RowHeight(fg.Rows - 1) = 500
        End If

        '==========================================
     '<EhFooter>
     Exit Sub

QWait_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.QWait " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub
Public Function CalCarInFactory()
     '<EhHeader>
     On Error GoTo CalCarInFactory_Err
     '</EhHeader>

        Dim rs1 As ADODB.Recordset
        Dim sql As String
        Dim row As Integer

100     sql = "SELECT car_in.CID,car_in.LicenseDigit,car_no.CarNo FROM tblParking as car_in LEFT JOIN tblParking as car_out ON car_out.ReferenceId = car_in.id LEFT JOIN tblCarNo car_no ON car_no.CarLicense=car_in.LicenseDigit WHERE car_out.id IS NULL AND car_in.Flag = 0"
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
120     CalCarInFactory = rs1.RecordCount
125     Set rs1 = Nothing
     '<EhFooter>
     Exit Function

CalCarInFactory_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.CalCarInFactory " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function
Public Sub QReCall(QID As Double)
     '<EhHeader>
     On Error GoTo QReCall_Err
     '</EhHeader>


        Dim rs1 As ADODB.Recordset
        Dim sql As String
        Dim row As Integer

100     sql = "SELECT * FROM tblQueue WHERE QID=" & QID
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
    
120     Do While Not rs1.EOF
    
125         If rs1!QBottle Then
                '   If IsNull(rs1!QTime21) And Not IsNull(rs1!QTime22) Then

130             Call Q2Lane(Val(rs1!QLane), Val(rs1!QID))
135             rs1!QStep1Call = Val(rs1!QStep1Call) + 1

                '=======�� Time Stamp ���¡��Ǥú 3 ����=======
140             Select Case Val(rs1!QStep1Call)
                
                    Case 2: rs1!QStep1Call_2 = Now
                    
145                 Case 3: rs1!QStep1Call_3 = Now
                
                End Select
                '====================================
                
150             If Val(rs1!QStep1Call) > 3 Then
155                 Call RemoveQ2Lane(Val(rs1!QID)) '���¡�ú 3 �ͺ ��Ҥ���͡�ҡ �Ź ��͹
160                 Call QCall(Val(rs1!QType), Val(rs1!QLane)) '����¡��ǶѴ�����
165                 rs1!QLane = 0
170                 rs1!QReCall = 1
                Else
175                 frmMain.lsSpeak.AddItem rs1!QNum & Space$(1) & rs1!QCarNo & Space$(1) & Format(Val(rs1!QLane), "00") 'Speaker
                End If
            
180             rs1.Update
185             g_ForceRead = True

                Exit Do

                '   End If
            
            Else

190             Call Q2Lane(Val(rs1!QLane), Val(rs1!QID))
195             rs1!QCall = Val(rs1!QCall) + 1
             
                '=======�� Time Stamp ���¡��Ǥú 3 ����=======
200             Select Case Val(rs1!QCall)
                
                    Case 2: rs1!QCall_2 = Now
                    
205                 Case 3: rs1!QCall_3 = Now
                
                End Select
                '====================================
           
210             If Val(rs1!QCall) > 3 Then
215                 Call RemoveQ2Lane(Val(rs1!QID)) '���¡�ú 3 �ͺ ��Ҥ���͡�ҡ �Ź ��͹
220                 Call QCall(Val(rs1!QType), Val(rs1!QLane)) '����¡��ǶѴ�����
225                 rs1!QLane = 0
230                 rs1!QReCall = 1
                Else
235                 frmMain.lsSpeak.AddItem rs1!QNum & Space$(1) & rs1!QCarNo & Space$(1) & Format(Val(rs1!QLane), "00") 'Speaker
                End If
            
240             rs1.Update
245             g_ForceRead = True

                Exit Do

            End If
250         rs1.MoveNext
        Loop

255     Set rs1 = Nothing
    
     '<EhFooter>
     Exit Sub

QReCall_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.QReCall " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub QCall(QType As Integer, Lane As Integer)
     '<EhHeader>
     On Error GoTo QCall_Err
     '</EhHeader>

        Dim rs1 As ADODB.Recordset
        Dim sql As String
        Dim row As Integer
    
        'If m_global.maxLane = 11 Then
100         sql = "SELECT * FROM tblQueue WHERE QBottle=0 AND QCall=0 AND QDelete=0 AND QType=" & QType
        'Else
105         If Lane = b1 Or Lane = b2 Then
110             sql = "SELECT * FROM tblQueue WHERE QBottle=1 AND QStep1Call=0 AND QDelete=0 AND QType=" & QType
            Else
115             sql = "SELECT * FROM tblQueue WHERE QBottle=0 AND QCall=0 AND QDelete=0 AND QType=" & QType
            End If
        'End If
    
120     Set rs1 = New ADODB.Recordset
125     rs1.CursorLocation = adUseClient
130     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText

135     Do While Not rs1.EOF
    
140         If rs1!QBottle Then
145             If IsNull(rs1!QStep1Call_1) Or rs1!QReCall Then  'And IsNull(rs1!QTime22)
150                 If Not LaneReady(Lane, Val(rs1!QID)) Then '�Ź�����ҧ�͡���������
                        '�Ҩ������ͤ��������� keypad ����Ź�ѧ�����ҧ
155                     Set rs1 = Nothing

                        Exit Sub

                    End If
160                 Call Q2Lane(Lane, Val(rs1!QID))
165                 rs1!QStep1Call_1 = Now
170                 rs1!QStep1Call = Val(rs1!QStep1Call) + 1
175                 rs1!QLane = Lane
180                 frmMain.lsSpeak.AddItem rs1!QNum & Space$(1) & rs1!QCarNo & Space$(1) & Format(Lane, "00") 'Speaker
185                 rs1.Update
190                 g_ForceRead = True
                 
                    Exit Do

                End If
            
            Else
195             If Not LaneReady(Lane, Val(rs1!QID)) Then '�Ź�����ҧ�͡���������
                    '�Ҩ������ͤ��������� keypad ����Ź�ѧ�����ҧ
200                 Set rs1 = Nothing

                    Exit Sub

                End If
205             Call Q2Lane(Lane, Val(rs1!QID))
210             rs1!QCall_1 = Now
215             rs1!QCall = Val(rs1!QCall) + 1
220             rs1!QLane = Lane
225             frmMain.lsSpeak.AddItem rs1!QNum & Space$(1) & rs1!QCarNo & Space$(1) & Format(Lane, "00") 'Speaker
230             rs1.Update
235             g_ForceRead = True
           
                Exit Do

            End If
240         rs1.MoveNext
        Loop

245     Set rs1 = Nothing

     '<EhFooter>
     Exit Sub

QCall_Err:

    Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.QCall " & _
               "at line " & Erl, EVENTLOG_ERROR_TYPE, Err.Number)
     Resume Next
     '</EhFooter>
End Sub

Public Sub QStartLoad(QID As Double) ' TIMESTAMP Step 3 Loading
     '<EhHeader>
     On Error GoTo QStartLoad_Err
     '</EhHeader>


        Dim rs1 As ADODB.Recordset
        Dim sql As String
        Dim row As Integer
100     sql = "SELECT * FROM tblQueue a INNER JOIN tblLane b ON a.QID=b.QID WHERE a.QID=" & QID
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
120     If Not rs1.EOF Then
125         rs1!Start = 1
130         If rs1!QBottle Then
135             rs1!QTime21 = Now 'ŧ�Ǵ Stamp Step 2.1
            Else
140             rs1!QTime3 = Now '���ŧ�Ǵ Stamp Step 3
            End If
145         rs1.Update
        End If
150     Set rs1 = Nothing

     '<EhFooter>
     Exit Sub

QStartLoad_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.QStartLoad " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub QFinish(QID As Double)
     '<EhHeader>
     On Error GoTo QFinish_Err
     '</EhHeader>

        Dim rs1 As ADODB.Recordset
        Dim sql As String
        Dim row As Integer
100     sql = "SELECT * FROM tblQueue a INNER JOIN tblLane b ON a.QID=b.QID WHERE a.QID=" & QID
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText

120     If Not rs1.EOF Then
125         rs1!Start = 1

130         If rs1!QBottle Then
135             rs1!QBottle = 0 '��� status ŧ�Ǵ�͡ �͹�͡��§ҹ�����Ҩҡ QTime21 �Ѻ QTime22 ������ͧ��ǹ���ʴ������öŧ�Ǵ
140             rs1!QTime22 = Now 'ŧ�Ǵ Stamp Step 2.1
145             rs1!QLane = 0 ' ����Ź �͡����
            Else
150             rs1!QTime4 = Now '���ŧ�Ǵ Stamp Step 3

                'ŧ     QTime5 ���������������д��ѧ�������
155             If mPark Then
            
160                 rs1!QTime5 = Now
                End If
            End If

165         rs1.Update
        End If

170     Set rs1 = Nothing

     '<EhFooter>
     Exit Sub

QFinish_Err:

    Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.QFinish " & _
               "at line " & Erl, EVENTLOG_ERROR_TYPE, Err.Number)
     Resume Next
     '</EhFooter>
End Sub

Private Function LaneReady(Lane As Integer, QID As Double) As Boolean
     '<EhHeader>
     On Error GoTo LaneReady_Err
     '</EhHeader>

        ' True = Ready

        Dim rs1 As ADODB.Recordset
        Dim sql As String
        Dim row As Integer
100     sql = "SELECT * FROM tblLane WHERE QID=" & QID & "  AND Start=1"
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
120     LaneReady = rs1.EOF
125     Set rs1 = Nothing

     '<EhFooter>
     Exit Function

LaneReady_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.LaneReady " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function

Public Sub RemoveQ2Lane(QID As Double)
     '<EhHeader>
     On Error GoTo RemoveQ2Lane_Err
     '</EhHeader>


        Dim rs1 As ADODB.Recordset
        Dim sql As String
        Dim row As Integer
        ' sql = "INSERT INTO tblLane (Lane,QID) VALUES (" & Lane & "," & QID & ")"
100     sql = "DELETE FROM tblLane WHERE QID=" & QID
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
120     Set rs1 = Nothing
    
     '<EhFooter>
     Exit Sub

RemoveQ2Lane_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.RemoveQ2Lane " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub Q2Lane(Lane As Integer, QID As Double)
     '<EhHeader>
     On Error GoTo Q2Lane_Err
     '</EhHeader>


        Dim rs1 As ADODB.Recordset
        Dim sql As String
        Dim row As Integer
        ' sql = "INSERT INTO tblLane (Lane,QID) VALUES (" & Lane & "," & QID & ")"
100     sql = "INSERT INTO tblLane (Lane, QID) VALUES (" & Lane & "," & QID & ") ON DUPLICATE KEY UPDATE  Lane=" & Lane & ",curTimeStamp=CURRENT_TIMESTAMP"
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
120     Set rs1 = Nothing

     '<EhFooter>
     Exit Sub

Q2Lane_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.Q2Lane " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub CurQ(fg As VSFlexGrid)
     '<EhHeader>
     On Error GoTo CurQ_Err
     '</EhHeader>


        Dim rs1 As ADODB.Recordset
        Dim sql As String
        Dim row As Integer
        'SELECT *,TIMESTAMPDIFF(MINUTE,a.QTime21,CURRENT_TIMESTAMP) as workTimeBottle
100     sql = "SELECT *,TIMESTAMPDIFF(MINUTE,a.QTime3,CURRENT_TIMESTAMP) as workTime,TIMESTAMPDIFF(MINUTE,a.QTime21,CURRENT_TIMESTAMP) as workTimeBottle FROM tblQueue a INNER JOIN tblLane b ON a.QID=b.QID WHERE b.Start=1 ORDER BY b.Lane"
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
120     fg.Rows = fg.FixedRows
125     fg.Rows = m_global.maxLane + fg.FixedRows
    
130     For row = fg.FixedRows To fg.Rows - 1
135         fg.TextMatrix(row, 0) = row
140     Next row
    
145     Do While Not rs1.EOF

150         With fg
155             .TextMatrix(Val(rs1!QLane), 0) = rs1!QLane
160             .TextMatrix(Val(rs1!QLane), 1) = m_Q.QTypeName(Val(rs1!QType))
165             .TextMatrix(Val(rs1!QLane), 2) = rs1!QNum
170             .TextMatrix(Val(rs1!QLane), 3) = rs1!QCarNo
175             .TextMatrix(Val(rs1!QLane), 4) = rs1!QCarLicense
180             If rs1!QBottle Then
185                 .TextMatrix(Val(rs1!QLane), 5) = rs1!WorkTimeBottle & ""
                Else
190                 .TextMatrix(Val(rs1!QLane), 5) = rs1!WorkTime & ""
                End If

195             .TextMatrix(Val(rs1!QLane), 6) = Format$(Right$(rs1!QTime2, 8), "hh:MM:ss") '�����Ѻ�ѵä��
200             .TextMatrix(Val(rs1!QLane), .Cols - 1) = rs1!QID & "" 'QID
            End With

205         rs1.MoveNext
        Loop

210     Set rs1 = Nothing

     '<EhFooter>
     Exit Sub

CurQ_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.CurQ " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub CurQonLane()
     '<EhHeader>
     On Error GoTo CurQonLane_Err
     '</EhHeader>

        Dim rs1 As ADODB.Recordset
        Dim sql As String
        Dim i%, index%, tmpWT%
100     sql = "SELECT *,TIMESTAMPDIFF(MINUTE,a.QTime3,CURRENT_TIMESTAMP) as workTime,TIMESTAMPDIFF(MINUTE,a.QTime21,CURRENT_TIMESTAMP) as workTimeBottle FROM tblLane b INNER JOIN tblQueue a ON a.QID=b.QID ORDER BY b.Lane"
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText

120     For i = 0 To 15
125         frmKeyPad.txtQNum(i).Text = ""
130         frmKeyPad.txtStatus(i).Text = ""
135         frmKeyPad.txtQID(i).Text = ""
140         frmKeyPad.cmdStart(i).Enabled = False
145         frmKeyPad.cmdFinish(i).Enabled = False
150         frmKeyPad.cmdCall(i).Enabled = True
        Next

155     Do While Not rs1.EOF
160         index = Val(rs1!Lane) - 1
165         frmKeyPad.txtQNum(index).Text = rs1!QNum
170         frmKeyPad.txtQID(index).Text = rs1!QID
        
175         If Val(rs1!Start) = 1 Then
180             If rs1!QBottle Then
185                 If IsNull(rs1!WorkTimeBottle) Then
190                     tmpWT = 0
                    Else
195                     tmpWT = Val(rs1!WorkTimeBottle)
                    End If
                
                    'tmpWT = IIf(IsNull(rs1!WorkTimeBottle), 0, Val(rs1!WorkTimeBottle)) 'Val(rs1!WorkTimeBottle)
200                 frmKeyPad.txtStatus(index).Text = "Bottle(" & tmpWT & ")"
                Else
205                 If IsNull(rs1!WorkTime) Then
210                     tmpWT = 0
                    Else
215                     tmpWT = Val(rs1!WorkTime)
                    End If
                    'tmpWT = IIf(IsNull(rs1!WorkTime), 0, Val(rs1!WorkTime))
220                 frmKeyPad.txtStatus(index).Text = "Loading(" & tmpWT & ")"
                End If
225             frmKeyPad.cmdCall(index).Enabled = False
230             frmKeyPad.cmdStart(index).Enabled = False
235             frmKeyPad.cmdFinish(index).Enabled = True
            Else
240             frmKeyPad.cmdCall(index).Enabled = True
245             frmKeyPad.cmdStart(index).Enabled = True
250             frmKeyPad.cmdFinish(index).Enabled = False
255             If rs1!QBottle And IsNull(rs1!QTime22) Then
260                 frmKeyPad.txtStatus(index).Text = "Call(" & rs1!QStep1Call & ")"
                Else
265                 frmKeyPad.txtStatus(index).Text = "Call(" & rs1!QCall & ")"
                End If
            End If

270         rs1.MoveNext
        Loop

275     Set rs1 = Nothing

        '========UNLOCK LANE==============
280     For i = 0 To 15
285         If Len(frmKeyPad.txtQNum(i).Text) > 0 Then
290             frmKeyPad.txtQNum(i).Locked = True
            Else
295             frmKeyPad.txtQNum(i).Locked = False
            End If
300     Next i

        '=================================
     '<EhFooter>
     Exit Sub

CurQonLane_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.CurQonLane " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub QPass(fg As VSFlexGrid)
     '<EhHeader>
     On Error GoTo QPass_Err
     '</EhHeader>


        Dim rs1 As ADODB.Recordset
        Dim sql As String
        Dim row%, i
        Dim tmpLane$, tmpLaneBottle$, tmpQNum$
    
100     Call getLaneBusy '�� ʶҹ��Ź��͹�����ҧ�������
    
105     sql = "SELECT * FROM tblQueue WHERE QStep1Call>3 OR QCall>3 AND QDelete=0 ORDER BY QTime2"
110     Set rs1 = New ADODB.Recordset
115     rs1.CursorLocation = adUseClient
120     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
125     fg.Rows = fg.FixedRows
130     row = fg.FixedRows

135     For i = 1 To maxLane

140         If Not LaneBusy(i) Then
145             If i = 11 Or i = 12 Then
150                 tmpLane = tmpLane & "|" & Format(i, "00") & " " & m_Q.QTypeName(m_Q.QLaneType(i)) & " (Bottle) [��ҧ]"
                Else
155                 tmpLane = tmpLane & "|" & Format(i, "00") & " " & m_Q.QTypeName(m_Q.QLaneType(i)) & " [��ҧ]"
                End If

            Else

    '            If i = 11 Or i = 12 Then
    '                tmpLane = tmpLane & "|" & Format(i, "00") & " " & m_Q.QTypeName(m_Q.QLaneType(i)) & " (Bottle) [�����ҧ]"
    '            Else
    '                tmpLane = tmpLane & "|" & Format(i, "00") & " " & m_Q.QTypeName(m_Q.QLaneType(i)) & " [�����ҧ]"
    '            End If
            End If

160     Next i

        'tmpLaneBottle = "11|12"
165     tmpQNum = ""

170     Do While Not rs1.EOF
        
175         With fg
180             .AddItem "", .Rows
185             .TextMatrix(row, 0) = rs1!QID 'QID
190             .TextMatrix(row, 1) = rs1!QNum 'QNum
195             tmpQNum = tmpQNum & Space$(1) & rs1!QNum
200             .TextMatrix(row, 2) = rs1!QCarNo '����ö

205             If rs1!QBottle Then
                    ' .ColComboList(4) = tmpLaneBottle '��������Ţ�Ź���������͡
210                 .TextMatrix(row, 3) = m_Q.QTypeName(Val(rs1!QType)) & " (Bottle)" '������
                Else
                    '.ColComboList(4) = tmpLane '��������Ţ�Ź���������͡
215                 .TextMatrix(row, 3) = m_Q.QTypeName(Val(rs1!QType)) '������
                End If
            
220             .ColComboList(4) = tmpLane '��������Ţ�Ź���������͡
225             .TextMatrix(row, 5) = "RECALL"
                '.Cell(flexcpBackColor, Row, 5, Row, 5) = vbGreen
230             .Cell(flexcpFontBold, row, 5, row, 5) = True
            End With

235         row = row + 1
240         rs1.MoveNext
        Loop

245     frmDSP32.txtQSkip.Text = tmpQNum
250     Set rs1 = Nothing

     '<EhFooter>
     Exit Sub

QPass_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.QPass " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub getLaneBusy()
     '<EhHeader>
     On Error GoTo getLaneBusy_Err
     '</EhHeader>


        Dim rs1 As ADODB.Recordset
        Dim sql As String
        Dim i%
100     sql = "SELECT * FROM tblLane"
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText

120     For i = 1 To maxLane
125         LaneBusy(i) = False ' �������ҡ�͹
130     Next i

135     Do While Not rs1.EOF
140         LaneBusy(Val(rs1!Lane)) = True
145         rs1.MoveNext
        Loop

150     Set rs1 = Nothing

     '<EhFooter>
     Exit Sub

getLaneBusy_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.getLaneBusy " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub QPassCall(QID As Double, QBottle As Boolean, Lane As Integer)
     '<EhHeader>
     On Error GoTo QPassCall_Err
     '</EhHeader>


        Dim rs1 As ADODB.Recordset
        Dim sql As String
   
100     sql = "SELECT * FROM tblQueue WHERE QID=" & QID
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText

120     Do While Not rs1.EOF
    
125         If QBottle Then
130             If IsNull(rs1!QStep1Call_1) Or Val(rs1!QReCall) Then  'And IsNull(rs1!QTime22)
135                 If Not LaneReady(Lane, Val(rs1!QID)) Then '�Ź�����ҧ�͡���������
                        '�Ҩ������ͤ��������� keypad ����Ź�ѧ�����ҧ
140                     Set rs1 = Nothing

                        Exit Sub

                    End If
145                 Call Q2Lane(Lane, Val(rs1!QID))
150                 rs1!QStep1Call_1 = Now
155                 'rs1!QStep1Call_2 = Null
160                 'rs1!QStep1Call_3 = Null
165                 rs1!QStep1Call = 1
170                 rs1!QLane = Lane
175                 frmMain.lsSpeak.AddItem rs1!QNum & Space$(1) & rs1!QCarNo & Space$(1) & Format(Lane, "00") 'Speaker
180                 rs1.Update
185                 g_ForceRead = True
                 
                    Exit Do

                End If
            
            Else
190             If Not LaneReady(Lane, Val(rs1!QID)) Then '�Ź�����ҧ�͡���������
                    '�Ҩ������ͤ��������� keypad ����Ź�ѧ�����ҧ
195                 Set rs1 = Nothing

                    Exit Sub

                End If
200             Call Q2Lane(Lane, Val(rs1!QID))
205             rs1!QCall_1 = Now
210             'rs1!QCall_2 = Null
215             'rs1!QCall_3 = Null
220             rs1!QCall = 1
225             rs1!QLane = Lane
230             frmMain.lsSpeak.AddItem rs1!QNum & Space$(1) & rs1!QCarNo & Space$(1) & Format(Lane, "00") 'Speaker
235             rs1.Update
240             g_ForceRead = True
           
                Exit Do

            End If
245         rs1.MoveNext
        Loop

250     Set rs1 = Nothing

     '<EhFooter>
     Exit Sub

QPassCall_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.QPassCall " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub DirectQ(Lane As Integer, QNum As Integer)
     '<EhHeader>
     On Error GoTo DirectQ_Err
     '</EhHeader>


        Dim rs1 As ADODB.Recordset
        Dim sql As String
   
        'sql = "SELECT * FROM tblQueue WHERE QNum=" & QNum & " AND QDelete=0 AND QDate='" & m_year.gYear & Format$(Now, "-MM-dd") & "'"
100     sql = "SELECT * FROM tblQueue WHERE QNum=" & QNum & " AND ISNULL(QTime4) AND QDelete=0 ORDER BY QDate"
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText

120     If Not rs1.EOF Then
    
125         If rs1!QBottle Then
130             If IsNull(rs1!QStep1Call_1) Or rs1!QReCall Then  'And IsNull(rs1!QTime22)
135                 If Not LaneReady(Lane, Val(rs1!QID)) Then '�Ź�����ҧ�͡���������
                        '�Ҩ������ͤ��������� keypad ����Ź�ѧ�����ҧ
140                     Set rs1 = Nothing

                        Exit Sub

                    End If

145                 Call Q2Lane(Lane, Val(rs1!QID))
150                 rs1!QStep1Call_1 = Now
155                 'rs1!QStep1Call_2 = Null
160                 'rs1!QStep1Call_3 = Null
165                 rs1!QStep1Call = 1
170                 rs1!QTime21 = Null
175                 rs1!QTime22 = Null
180                 rs1!QTime3 = Null
185                 rs1!QTime4 = Null
190                 rs1!QTime5 = Null
195                 rs1!QLane = Lane
200                 frmMain.lsSpeak.AddItem rs1!QNum & Space$(1) & rs1!QCarNo & Space$(1) & Format(Lane, "00") 'Speaker
205                 rs1.Update
210                 g_ForceRead = True
215                 Set rs1 = Nothing

                    Exit Sub

                End If
            
            Else

220             If Not LaneReady(Lane, Val(rs1!QID)) Then '�Ź�����ҧ�͡���������
                    '�Ҩ������ͤ��������� keypad ����Ź�ѧ�����ҧ
225                 Set rs1 = Nothing

                    Exit Sub

                End If

230             Call Q2Lane(Lane, Val(rs1!QID))
235             rs1!QCall_1 = Now
240             'rs1!QCall_2 = Null
245             'rs1!QCall_3 = Null
250             rs1!QTime3 = Null
255             rs1!QTime4 = Null
260             rs1!QTime5 = Null
265             rs1!QCall = 1
270             rs1!QLane = Lane
275             frmMain.lsSpeak.AddItem rs1!QNum & Space$(1) & rs1!QCarNo & Space$(1) & Format(Lane, "00") 'Speaker
280             rs1.Update
285             g_ForceRead = True
           
290             Set rs1 = Nothing

                Exit Sub

            End If

        Else
            '����������Ţ��Ƿ���ҹ���¡
        End If

295     Set rs1 = Nothing

     '<EhFooter>
     Exit Sub

DirectQ_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.DirectQ " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub setCarNo()
     '<EhHeader>
     On Error GoTo setCarNo_Err
     '</EhHeader>

        Dim rs1 As ADODB.Recordset
        Dim sql As String
   
100     sql = "SELECT car_in.CID,car_in.LicenseDigit,car_no.CarNo FROM tblParking as car_in LEFT JOIN tblParking as car_out ON car_out.ReferenceId = car_in.id INNER JOIN tblCarNo car_no ON car_no.CarLicense=car_in.LicenseDigit WHERE car_out.id IS NULL AND car_in.Flag = 0 AND car_in.CID NOT IN (SELECT QCarID FROM tblQueue)" 'Select �Ţö��Ш��ѹ����ѧ����դ�����ʴ� 'sql = "SELECT * FROM tblCar WHERE Printed=0"
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
120     frmTicket.lstCarNo.Clear
125     frmTicket.lstCarLicense.Clear
130     frmTicket.lstCID.Clear

135     Do While Not rs1.EOF
140         frmTicket.lstCarNo.AddItem rs1!carNo & ""
145         frmTicket.lstCarLicense.AddItem rs1!LicenseDigit & ""
150         frmTicket.lstCID.AddItem rs1!CID & ""
155         rs1.MoveNext
        Loop
    
160     Set rs1 = Nothing

     '<EhFooter>
     Exit Sub

setCarNo_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.setCarNo " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub UpdateCarTimeOUT()
     '<EhHeader>
     On Error GoTo UpdateCarTimeOUT_Err
     '</EhHeader>
        Dim rs1 As ADODB.Recordset
        Dim sql As String
100     sql = "UPDATE tblQueue tq INNER JOIN tblParking tpk ON tq.QCarID=tpk.ReferenceId SET tq.QTime5=tpk.ParkingTime"
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
120     Set rs1 = Nothing
     '<EhFooter>
     Exit Sub

UpdateCarTimeOUT_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.UpdateCarTimeOUT " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub
Public Sub initialCall()
     '<EhHeader>
     On Error GoTo initialCall_Err
     '</EhHeader>


        Dim rs1 As ADODB.Recordset
        Dim sql As String
100     sql = "SELECT * FROM tblQueue a INNER JOIN tblLane b ON a.QID=b.QID ORDER BY `b`.`curTimeStamp` ASC"
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
120     frmMain.lstDSP.Clear
    
125     Do While Not rs1.EOF
130         frmMain.lstDSP.AddItem rs1!QCarNo & "|" & rs1!QNum & "|" & rs1!QLane, 0
135         rs1.MoveNext
        Loop

140     Set rs1 = Nothing

     '<EhFooter>
     Exit Sub

initialCall_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.initialCall " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

'
Public Sub curCall()
     '<EhHeader>
     On Error GoTo curCall_Err
     '</EhHeader>

        '    Dim rs1   As ADODB.Recordset
        '    Dim sql   As String

        Dim i     As Integer
        Dim exp() As String
        '    sql = "SELECT * FROM tblQueue a INNER JOIN tblLane b ON a.QID=b.QID ORDER BY `b`.`curTimeStamp` ASC"
        '    Set rs1 = New ADODB.Recordset
        '    rs1.CursorLocation = adUseClient
        '    rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText

100     For i = 0 To 3

105         If Len(frmMain.lstDSP.List(i)) > 0 Then
110             exp = Split(frmMain.lstDSP.List(i), "|")
                'exp(0) => CarNo
                'exp(1) => QNum
                'exp(2) =>  Lane
115             frmDSP32.lblCarNo(i).Caption = exp(0)
120             frmDSP32.lblQ(i).Caption = exp(1)
125             frmDSP32.lblLane(i).Caption = exp(2)
            Else
130             frmDSP32.lblCarNo(i).Caption = ""
135             frmDSP32.lblQ(i).Caption = ""
140             frmDSP32.lblLane(i).Caption = ""
            End If

145     Next i

150     For i = 4 To frmMain.lstDSP.ListCount

            On Error Resume Next

155         frmMain.lstDSP.RemoveItem (i)
160     Next i

165     Call m_LED.DSP(DSP_BingOn)

        'Set rs1 = Nothing
     '<EhFooter>
     Exit Sub

curCall_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.curCall " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub ClearQ()
     '<EhHeader>
     On Error GoTo ClearQ_Err
     '</EhHeader>


        Dim rs1 As ADODB.Recordset
        Dim sql As String
100     sql = "DELETE FROM tblQueue;"
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
120     Set rs1 = Nothing
    
125     sql = "DELETE FROM tblLane"
130     Set rs1 = New ADODB.Recordset
135     rs1.CursorLocation = adUseClient
140     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
145     Set rs1 = Nothing

     '<EhFooter>
     Exit Sub

ClearQ_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.ClearQ " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Private Sub SUMCar(ByRef arr() As Integer, QTime As String)
     '<EhHeader>
     On Error GoTo SUMCar_Err
     '</EhHeader>


        Dim rs1 As ADODB.Recordset
        Dim sql As String
        Dim c%

100     sql = "SELECT"
105     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','00:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE(),' ','01:00:00') THEN 1 ELSE 0 END) AS count1,"
110     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','01:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE(),' ','02:00:00') THEN 1 ELSE 0 END) AS count2,"
115     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','02:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE(),' ','03:00:00') THEN 1 ELSE 0 END) AS count3,"
120     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','03:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE(),' ','04:00:00') THEN 1 ELSE 0 END) AS count4,"
125     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','04:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE(),' ','05:00:00') THEN 1 ELSE 0 END) AS count5,"
130     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','05:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE(),' ','06:00:00') THEN 1 ELSE 0 END) AS count6,"
135     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','06:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE(),' ','07:00:00') THEN 1 ELSE 0 END) AS count7,"
140     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','07:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE(),' ','08:00:00') THEN 1 ELSE 0 END) AS count8,"
145     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','08:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE(),' ','09:00:00') THEN 1 ELSE 0 END) AS count9,"
150     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','09:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE(),' ','10:00:00') THEN 1 ELSE 0 END) AS count10,"
155     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','10:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE(),' ','11:00:00') THEN 1 ELSE 0 END) AS count11,"
160     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','11:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE(),' ','12:00:00') THEN 1 ELSE 0 END) AS count12,"
165     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','12:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE(),' ','13:00:00') THEN 1 ELSE 0 END) AS count13,"
170     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','13:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE(),' ','14:00:00') THEN 1 ELSE 0 END) AS count14,"
175     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','14:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE(),' ','15:00:00') THEN 1 ELSE 0 END) AS count15,"
180     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','15:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE(),' ','16:00:00') THEN 1 ELSE 0 END) AS count16,"
185     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','16:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE(),' ','17:00:00') THEN 1 ELSE 0 END) AS count17,"
190     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','17:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE(),' ','18:00:00') THEN 1 ELSE 0 END) AS count18,"
195     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','18:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE(),' ','19:00:00') THEN 1 ELSE 0 END) AS count19,"
200     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','19:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE(),' ','20:00:00') THEN 1 ELSE 0 END) AS count20,"
205     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','20:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE(),' ','21:00:00') THEN 1 ELSE 0 END) AS count21,"
210     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','21:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE(),' ','22:00:00') THEN 1 ELSE 0 END) AS count22,"
215     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','22:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE(),' ','23:00:00') THEN 1 ELSE 0 END) AS count23,"
220     sql = sql & " SUM(CASE WHEN " & QTime & " >= CONCAT(CURRENT_DATE(),' ','23:00:00') AND " & QTime & " <= CONCAT(CURRENT_DATE() + INTERVAL 1 DAY,' ','00:00:00') THEN 1 ELSE 0 END) AS count24"
225     sql = sql & " FROM tblQueue WHERE QDelete=0"
230     Set rs1 = New ADODB.Recordset
235     rs1.CursorLocation = adUseClient
240     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
245     If Not rs1.EOF Then
            
250         For c = 1 To frmMain.chart.RowCount
                '            frmMain.chart.row = c
                '            frmMain.chart.Data = Val(rs1.Fields(c - 1) & "")
                '            frmMain.chart.RowLabel = c - 1 & "-" & c
255             arr(c) = Val(rs1.Fields(c - 1) & "")
260         Next c

        End If
265     Set rs1 = Nothing

     '<EhFooter>
     Exit Sub

SUMCar_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.SUMCar " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub CompleteInHr()
     '<EhHeader>
     On Error GoTo CompleteInHr_Err
     '</EhHeader>


        Dim c%
        Dim cCarIn(1 To 24)       As Integer
        Dim cCarComplete(1 To 24) As Integer
100     Call SUMCar(cCarComplete, "QTime4")
105     Call SUMCar(cCarIn, "QTime2")

        'frmMain.chart.ColumnCount = 2
110     For c = 1 To frmMain.chart.RowCount
115         frmMain.chart.row = c
120         frmMain.chart.Column = 1
125         frmMain.chart.Data = cCarIn(c)
130         frmMain.chart.Column = 2
135         frmMain.chart.Data = cCarComplete(c)
140         frmMain.chart.RowLabel = c - 1 & "-" & c
145     Next c

     '<EhFooter>
     Exit Sub

CompleteInHr_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.CompleteInHr " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub QDel(QID As Double)
     '<EhHeader>
     On Error GoTo QDel_Err
     '</EhHeader>
        Dim rs1 As ADODB.Recordset
        Dim sql As String
        Dim row As Integer
100     sql = "UPDATE tblQueue SET QDelete=1 WHERE QID=" & QID
105     Set rs1 = New ADODB.Recordset
110     rs1.CursorLocation = adUseClient
115     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
120     Set rs1 = Nothing
     '<EhFooter>
     Exit Sub

QDel_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.QDel " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub

Public Sub QCarInOut(fg As VSFlexGrid, CarIn As Boolean)
     '<EhHeader>
     On Error GoTo QCarInOut_Err
     '</EhHeader>

        Dim rs1   As ADODB.Recordset
        Dim sql   As String
        Dim row   As Integer
        Dim fgDSP As VSFlexGrid
        'SELECT *,TIMESTAMPDIFF(MINUTE,QTime22,CURRENT_TIMESTAMP) as WaitTimeBottle
        '    sql = "SELECT *,TIMESTAMPDIFF(MINUTE,QTime2,CURRENT_TIMESTAMP) as WaitTime,TIMESTAMPDIFF(MINUTE,QTime22,CURRENT_TIMESTAMP) as WaitTimeBottle FROM tblQueue WHERE QCall=0 OR QStep1Call=0 ORDER BY QTime22,QTime2 ASC"
100     If CarIn Then
105     sql = "SELECT car_in.*,car_no.* FROM tblParking as car_in LEFT JOIN tblParking as car_out ON car_out.ReferenceId = car_in.id LEFT JOIN tblCarNo car_no ON car_no.CarLicense=car_in.LicenseDigit WHERE car_out.id IS NULL AND car_in.Flag = 0 AND car_in.CID NOT IN (SELECT QCarID FROM tblQueue)"
        Else
110     sql = "SELECT car_out.*,car_no.* FROM tblParking car_out LEFT JOIN tblCarNo car_no ON car_out.LicenseDigit=car_no.CarLicense WHERE DATE(car_out.ParkingTime)=CURDATE() AND car_out.Flag=1"
        End If
115     Set rs1 = New ADODB.Recordset
120     rs1.CursorLocation = adUseClient
125     rs1.Open sql, cn, adOpenStatic, adLockPessimistic, adCmdText
130     fg.Rows = fg.FixedRows
135     row = fg.FixedRows

        'RowDSP = fgDSP.FixedRows
140     Do While Not rs1.EOF
145         fg.AddItem rs1!carNo & vbTab & rs1!CarLicense & vbTab & rs1!CarType & vbTab & Format$(rs1!ParkingTime, "dd/MM/yyyy  hh:mm:ss")
150         rs1.MoveNext
        Loop
155   Set rs1 = Nothing
     '<EhFooter>
     Exit Sub

QCarInOut_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_Q.QCarInOut " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Sub
