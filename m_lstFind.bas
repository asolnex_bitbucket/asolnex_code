Attribute VB_Name = "m_lstFind"
Option Explicit

Private Declare Function SendMessage _
                Lib "USER32" _
                Alias "SendMessageA" (ByVal hWnd As Long, _
                                      ByVal wMsg As Long, _
                                      ByVal wParam As Integer, _
                                      ByVal lParam As Any) As Long

'constants for searching the ListBox
Private Const LB_FINDSTRINGEXACT = &H1A2

Private Const LB_FINDSTRING = &H18F

'How to use => MsgBox GetListBoxIndex(Me.lst.hWnd, Me.msk.Text, True)
Public Function GetListBoxIndex(hWnd As Long, _
                                SearchKey As String, _
                                Optional FindExactMatch As Boolean = True) As Long
     '<EhHeader>
     On Error GoTo GetListBoxIndex_Err
     '</EhHeader>


100     If FindExactMatch Then
105         GetListBoxIndex = SendMessage(hWnd, LB_FINDSTRINGEXACT, -1, ByVal SearchKey)
        Else
110         GetListBoxIndex = SendMessage(hWnd, LB_FINDSTRING, -1, ByVal SearchKey)
        End If

     '<EhFooter>
     Exit Function

GetListBoxIndex_Err:
            Call LogNTEvent(vbCrLf & vbCrLf & Err.Description & vbCrLf & _
               "in Queue.m_lstFind.GetListBoxIndex " & _
               "at line " & Erl, _
              EVENTLOG_ERROR_TYPE, Err.Number)
    Resume Next
     '</EhFooter>
End Function

